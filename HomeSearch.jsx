import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Link } from "react-router-dom";
import { Button, Row, Col, Container } from "react-bootstrap";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import "../css/HomeSearch.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import { geolocated } from "react-geolocated";
import locationButton from "./locationButton";
import PropTypes from "prop-types";
import PropertyList from "../components/Search/Search";

class HomeSearch extends React.Component {
  constructor(props) {
    super(props);

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  static propTypes = {
    options: PropTypes.instanceOf(Array).isRequired,
  };
  state = {
    activeOption: 0,
    filteredOptions: [],
    showOptions: false,
    userInput: "",
  };

  handleButtonClick = (event) => {
    this.setState(
      {
        propertystatus: event.target.attributes.getNamedItem("data-key").value,
      },
      () => console.log("propertyType", this.state.propertystatus)
    );
  };
  handleClearForm(event) {
    event.preventDefault();
    this.setState({
      propertyStatus: "",
      userInput: "",
    });
  }

  onChange = (e) => {
    console.log("onChanges");

    const { options } = this.props;
    const userInput = e.currentTarget.value;

    const filteredOptions = options.filter(
      (optionName) =>
        optionName.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    this.setState({
      activeOption: 0,
      filteredOptions,
      showOptions: true,
      userInput: e.currentTarget.value,
    });
    console.log("userInput1", e.currentTarget.value);
  };

  onClick = (e) => {
    console.log("onClick");
    this.setState({
      activeOption: 0,
      filteredOptions: [],
      showOptions: false,
      userInput: e.currentTarget.innerText,
    });
    console.log("userInput2", e.currentTarget.innerText);
  };
  onKeyDown = (e) => {
    const { activeOption, filteredOptions } = this.state;
    console.log("on key down");
    if (e.keyCode === 13) {
      this.setState({
        activeOption: 0,
        showOptions: false,
        userInput: filteredOptions[activeOption],
      });
    } else if (e.keyCode === 38) {
      if (activeOption === 0) {
        return;
      }
      this.setState({ activeOption: activeOption - 1 });
    } else if (e.keyCode === 40) {
      if (activeOption === filteredOptions.length - 1) {
        console.log(activeOption);
        return;
      }
      this.setState({ activeOption: activeOption + 1 });
    }
  };

  handleFormSubmit(e) {
    e.preventDefault();
    if (e.currentTarget.innerText !== undefined) {
      alert("Your input value is: " + e.currentTarget.value);
    }
    const formPayload = {
      propertyStatus: this.state.propertystatus,
      userInput: e.currentTarget.value,
    };

    console.log("Send this in a POST request:", formPayload);
    this.handleClearForm(e);
  }

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,

      state: { activeOption, filteredOptions, showOptions, userInput },
    } = this;
    let optionList;
    if (showOptions && userInput) {
      if (filteredOptions.length) {
        optionList = (
          <ul className="options">
            {filteredOptions.map((optionName, index) => {
              let className;
              if (index === activeOption) {
                className = "option-active";
              }
              return (
                <li className={className} key={optionName} onClick={onClick}>
                  {optionName}
                </li>
              );
            })}
          </ul>
        );
      } else {
        optionList = (
          <div className="no-options">
            <h2>
              <em>No Option!</em>
            </h2>
          </div>
        );
      }
    }
    return (
      <Router>
        <React.Fragment>
          {/* <SearchFilter /> */}
          <form onSubmit={this.handleFormSubmit}>
            <div className="Homefrantpage">
              <Row className="buttonContainer nomargin">
                <Col>
                  {" "}
                  <ButtonGroup
                    className="HomepageButtons "
                    onClick={this.handleButtonClick}
                  >
                    <button
                      className="BuyButton buttonHomepage"
                      data-key="buy"
                      active
                    >
                      Buy
                    </button>
                    {/* <div className="divider"></div> */}
                    <button
                      className="RentButton buttonHomepage"
                      data-key="rent"
                    >
                      Rent
                    </button>
                    <button
                      className="LeaseButton buttonHomepage"
                      data-key="lease"
                    >
                      Lease
                    </button>
                    <button
                      className="InvestButton buttonHomepage"
                      data-key="invest"
                    >
                      Invest
                    </button>
                    <button
                      className="InvestButton buttonHomepage"
                      data-key="join venture"
                    >
                      Joint Venture
                    </button>
                  </ButtonGroup>
                </Col>
              </Row>

              <Container>
                <InputGroup size="lg" className="mb-3">
                  <FormControl
                    type="search"
                    name="search"
                    placeholder="Enter Place,Builder, Project..."
                    aria-label="Enter Place,Builder, Project..."
                    aria-describedby="basic-addon2"
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                    value={userInput}
                  />

                  <InputGroup.Append>
                    <InputGroup.Text>
                      <i
                        className="fa fa-crosshairs"
                        onClick={locationButton}
                      ></i>
                    </InputGroup.Text>
                    
                      <Button
                        variant="danger"
                        value="search"
                        className="searchButton1"
                        type="submit"
                      >
                        search
                      </Button>
                   
                  </InputGroup.Append>
                </InputGroup>
                <div>{optionList}</div>
              </Container>
            </div>
          </form>
        </React.Fragment>
      </Router>
    );
  }
}
export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(HomeSearch);

// function HomepageButtons() {
//   return (
//     <ButtonGroup className="HomepageButtons " onClick={this.handleButtonClick}>
//       <button className="BuyButton buttonHomepage" active>
//         Buy
//       </button>
//       {/* <div className="divider"></div> */}
//       <button className="RentButton buttonHomepage">Rent</button>
//       <button className="LeaseButton buttonHomepage">Lease</button>
//       <button className="InvestButton buttonHomepage">Invest</button>
//       <button className="InvestButton buttonHomepage">Joint Venture</button>
//     </ButtonGroup>
//   );
// }
