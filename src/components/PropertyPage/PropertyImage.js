import React, { Component } from 'react';
import {Container}  from 'react-bootstrap';
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.css";

export default class PropertyImage extends Component {
  constructor(props) {
      super(props);
     this.state = {
       propertyType : this.props.apiResponse.propertyType,
       propertyStatus : this.props.apiResponse.propertyStatus,
       numberOfImages : this.props.apiResponse.noOfImages,
       productId : this.props.apiResponse.propertyId
     }

    
    }

    loadImageData() {
          var imageName = '';
            const srcName = "https://aceimagingandvideo.com/assets/beegru/".concat(this.state.propertyType).concat("_").concat(this.state.propertyStatus).concat("/").concat(this.state.productId).concat("/");
            if(this.state.numberOfImages == 1){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 2){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 3){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 4){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 5){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div><div><img src = {srcName.concat("image5.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 6){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div><div><img src = {srcName.concat("image5.png")} /></div><div><img src = {srcName.concat("image6.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 7){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div><div><img src = {srcName.concat("image5.png")} /></div><div><img src = {srcName.concat("image6.png")} /></div><div><img src = {srcName.concat("image7.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 8){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div><div><img src = {srcName.concat("image5.png")} /></div><div><img src = {srcName.concat("image6.png")} /></div><div><img src = {srcName.concat("image7.png")} /></div><div><img src = {srcName.concat("image8.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 9){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div><div><img src = {srcName.concat("image5.png")} /></div><div><img src = {srcName.concat("image6.png")} /></div><div><img src = {srcName.concat("image7.png")} /></div><div><img src = {srcName.concat("image8.png")} /></div><div><img src = {srcName.concat("image9.png")} /></div></Carousel>
            }else if(this.state.numberOfImages == 10){
              imageName = <Carousel autoPlay><div><img src = {srcName.concat("image1.png")} /></div><div><img src = {srcName.concat("image2.png")} /></div><div><img src = {srcName.concat("image3.png")} /></div><div><img src = {srcName.concat("image4.png")} /></div><div><img src = {srcName.concat("image5.png")} /></div><div><img src = {srcName.concat("image6.png")} /></div><div><img src = {srcName.concat("image7.png")} /></div><div><img src = {srcName.concat("image8.png")} /></div><div><img src = {srcName.concat("image9.png")} /></div><div><img src = {srcName.concat("image10.png")} /></div></Carousel>
            }
          return imageName;

  }



    render() { 
      let imageList = this.loadImageData();
        return ( 
                <div>
                    <Container>
                    
                      {imageList}
                    
                    </Container>
                </div>
              );
            }
          }
