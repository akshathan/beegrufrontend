import React from 'react';
import { Row, Col, Container, Button, Form } from 'react-bootstrap';
import SearchFilterIcon from '../Search/searchFilterIcon';
import CollectionIcon from '../Search/CollectionIcon';
import "../../css/propertyListPage.css";
import NewProperties from "../Home/NewProperties";
import ContactUs from "../../components/Home/ContactUs";
// import PropertyPagination from "./pagination"
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'

// import DropdownButton from 'react-bootstrap/DropdownButton';


export default class PropertyListPage extends React.Component {
        constructor(props) {
                super(props);
                this.state = {
                        totalamount: '',
                };
        }
        myTotalAmountChangeHandler1 = (event) => {
                this.setState({ totalamount1: event.target.value });
        }
        myTotalAmountChangeHandler2 = (event) => {
                this.setState({ totalamount2: event.target.value });
        }
        render() {
                return (
                        <Container className="propertyListPageWrapper">
                                <Container className="searchAndSortContainer">
                                        
                                        <Row>
                                                <Col xs={12} sm={4} md={6} lg={6}>
                                                        <div className="buttonoptions d-flex flex-column">
                                                                <ButtonGroup size="md">
                                                                        <Button className="buttongrp active">SALE</Button>
                                                                        <Button className="buttongrp">RENT</Button>
                                                                        <Button className="buttongrp">Join venture</Button>
                                                                        <Button className="buttongrp">Invest</Button>

                                                                </ButtonGroup>
                                                        </div>
                                                </Col>
                                                <Col xs={12} sm={4} md={2} lg={1}>

                                                </Col>
                                                <Col xs={12} sm={4} md={4} lg={5}>
                                                        <ButtonGroup size="md">
                                                                <Button className="button buy active">Low - High</Button>
                                                                <Button className="button rent">High - Low</Button>
                                                                <Button className="button rent">Relevent</Button>
                                                        </ButtonGroup>
                                                </Col>
                                        </Row>
                                </Container>
                                {/* <h5 className="h5">Buy</h5>
                                                        <div className="buyAndRentWrapper">
                                                                <div className="button buy active">
                                                                        To Buy
                                                                </div>
                                                                <div className="button rent">
                                                                        To Rent
                                                                </div>
                                                        </div> 
                                                        <div className="sortByDropdown">
                                                                <Dropdown>
                                                                        <Dropdown.Toggle variant="" id="dropdown-basic">
                                                                                Sort By: Relevance
                                                                        </Dropdown.Toggle>

                                                                        <Dropdown.Menu>
                                                                                <Dropdown.Item href="">Relevance</Dropdown.Item>
                                                                                <Dropdown.Item href="">Price(Inc)</Dropdown.Item>
                                                                                <Dropdown.Item href="">Price(Dec)</Dropdown.Item>
                                                                                <Dropdown.Item href="">Area(Inc)</Dropdown.Item>
                                                                                <Dropdown.Item href="">Area(Dec)</Dropdown.Item>
                                                                                <Dropdown.Item href="">Date Added</Dropdown.Item>
                                                                        </Dropdown.Menu>
                                                                </Dropdown>
                                                        </div> */}
                                <Container>

                                        <Row >
                                                <Col xs={12} sm={9} md={5} lg={5} >

                                                        <div className="budget">
                                                                <h5 className="h5">Budget</h5>

                                                                <div data-role="rangeslider" className="rangeSlider">
                                                                        <Form.Group as={Row} >
                                                                                <Form.Label column sm="2">
                                                                                        Min
                                                                                </Form.Label>
                                                                                <Col sm="10">
                                                                                        <input className="form-control-range inputrange" type="range"
                                                                                                onChange={this.myTotalAmountChangeHandler1} min="10000" max="2000000" step="1000" id="formControlRange1" />
                                                                                </Col>
                                                                        </Form.Group>
                                                                        <Form.Group as={Row} >
                                                                                <Form.Label column sm="2">
                                                                                        Max
                                                                                </Form.Label>
                                                                                <Col sm="10">
                                                                                        <input className="form-control-range inputrange" type="range"
                                                                                                onChange={this.myTotalAmountChangeHandler2} min="10000" max="2000000" step="1000" id="formControlRange1" />
                                                                                </Col>
                                                                        </Form.Group>
                                                                        {/* <input type="range" name="price-min" id="price-min" value="200" min="0" max="1000" />
                                                                    <input type="range" name="price-max" id="price-max" value="800" min="0" max="1000" /> */}
                                                                </div>
                                                                <div className="textAndDropDownWrapper">



                                                                        <InputGroup>
                                                                                <InputGroup.Prepend>
                                                                                        <InputGroup.Text id="basic-addon1">Rs.</InputGroup.Text>
                                                                                </InputGroup.Prepend>
                                                                                <FormControl
                                                                                        placeholder="Min"
                                                                                        defaultValue={this.state.totalamount1} />
                                                                                {/* <DropdownButton
                                                                                        as={InputGroup.Append}
                                                                                        variant="outline-secondary"
                                                                                        title="Rs"
                                                                                        id="input-group-dropdown-2"
                                                                                >
                                                                                        <Dropdown.Item href="#">Hunds</Dropdown.Item>
                                                                                        <Dropdown.Item href="#">Lac</Dropdown.Item>
                                                                                        <Dropdown.Item href="#">Thd</Dropdown.Item>
                                                                                        <Dropdown.Divider />
                                                                                        <Dropdown.Item href="#">$</Dropdown.Item>
                                                                                </DropdownButton> */}

                                                                        </InputGroup>
                                                                        <span className="to">to</span>
                                                                        <InputGroup>
                                                                                <InputGroup.Prepend>
                                                                                        <InputGroup.Text id="basic-addon1">Rs.</InputGroup.Text>
                                                                                </InputGroup.Prepend>
                                                                                <FormControl
                                                                                        placeholder="Max"
                                                                                        defaultValue={this.state.totalamount2}
                                                                                        aria-describedby="basic-addon2"
                                                                                />


                                                                                {/* <DropdownButton
                                                                                        as={InputGroup.Append}
                                                                                        variant="outline-secondary"
                                                                                        title="Rs"
                                                                                        id="input-group-dropdown-2"
                                                                                >
                                                                                        <Dropdown.Item href="#">Hunds</Dropdown.Item>
                                                                                        <Dropdown.Item href="#">Lac</Dropdown.Item>
                                                                                        <Dropdown.Item href="#">Thd</Dropdown.Item>
                                                                                        <Dropdown.Divider />
                                                                                        <Dropdown.Item href="#">$</Dropdown.Item>
                                                                                </DropdownButton> */}
                                                                        </InputGroup>
                                                                        {/* <div className="textAndDropDown">
                                                                        <input type="text" ></input>
                                                                        <select>
                                                                                <option>Hunds</option>
                                                                                <option>Thds</option>
                                                                                <option>Lacs</option>
                                                                                
                                                                        </select>
                                                                </div>
                                                                <span className="to">to</span>

                                                                <div className="textAndDropDown">
                                                                        <input type="text" ></input>
                                                                        <select>
                                                                                <option>Hunds</option>
                                                                                <option>Thds</option>
                                                                                <option>Lacs</option>
                                                                        </select>
                                                                </div> */}

                                                                </div>
                                                        </div>
                                                </Col>
                                                <Col xs={12} sm={9} md={7} lg={7} >
                                                        <div className="propertyType">
                                                                <h5 className="h5">Property type</h5>
                                                                <div className="searchFilterIcons">

                                                                        <SearchFilterIcon></SearchFilterIcon>
                                                                </div>
                                                        </div>

                                                </Col>

                                        </Row>
                                </Container>
                                <Container >

                                        <Row>
                                                <Col xs={12} sm={9} md={5} lg={5} >

                                                        <Container className="placeSearch">
                                                                <h5 className="h5">Location</h5>
                                                                <InputGroup className="mb-3">
                                                                        <InputGroup.Prepend>
                                                                                <Button variant="outline-secondary"><i className="fa fa-search" aria-hidden="true"></i></Button>
                                                                        </InputGroup.Prepend>
                                                                        <FormControl aria-describedby="basic-addon1" placeholder="Sornam nagar" />
                                                                        <InputGroup.Append>
                                                                                <Button variant="outline-secondary"><i className="fa fa-crosshairs"></i></Button>
                                                                        </InputGroup.Append>
                                                                </InputGroup>
                                                                {/* <InputGroup className="mb-3 placeSearchBar">
                                                                        <InputGroup.Prepend>
                                                                                <Button variant="outline"><i className="fa fa-search" aria-hidden="true"></i></Button>
                                                                        </InputGroup.Prepend>
                                                                        <FormControl placeholder="Sornam nagar" />
                                                                        <InputGroup.Append>
                                                                                <i className="fa fa-crosshairs searchIcon1"></i>
                                                                        </InputGroup.Append>
                                                                </InputGroup> */}
                                                                <div className="searchButtonss">
                                                                        <div className="button">
                                                                                <span><i className="fa fa-times" aria-hidden="true"></i></span>
                                                                                <span className="buttonText"> Villa Nagar</span>
                                                                        </div>
                                                                        <div className="button">
                                                                                <span><i className="fa fa-times" aria-hidden="true"></i></span>
                                                                                <span className="buttonText" >Bilal Street</span>
                                                                        </div>

                                                                </div>
                                                        </Container>
                                                </Col>
                                                <Col xs={12} sm={9} md={7} lg={7} >

                                                        <div className="propertyType">
                                                                <h5 className="h5">Collection</h5>
                                                                <div className="searchFilterIcons">
                                                                        <CollectionIcon />
                                                                </div>
                                                        </div>
                                                </Col>
                                        </Row>
                                        <Row >
                                                <Col xs={12} sm={9} md={9} lg={12}>
                                                        <InputGroup className=" searchTextPropertyListPage mb-3" size="lg">
                                                                <InputGroup.Prepend>
                                                                        <Button variant="outline"><i className="fa fa-search" aria-hidden="true"></i></Button>
                                                                </InputGroup.Prepend>
                                                                <FormControl aria-describedby="basic-addon1" placeholder="Bangalore" />
                                                        </InputGroup>
                                                        {/* <div className="searchTextPropertyListPage">
                                                                <i className="fa fa-search" aria-hidden="true"></i>
                                                                <input type="text" className="searchText" placeholder="Bangalore"></input>
                                                        </div> */}
                                                </Col>

                                                
                                        </Row>
                                </Container>
                                <inputrangeapp />
                                <Container>
                                        <h3>Search Results</h3>
                                <NewProperties />

                                </Container>
                                



                                {/* <PropertyPagination /> */}

                                <div className="containnerWrapper contactUs">
                                        <ContactUs />
                                </div>


                        </Container>
                )
        }
}
