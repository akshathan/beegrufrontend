import React from "react";
import { Container, Row, Col, Button } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import '../../css/emi.css';
// import Gallery from "react-photo-gallery";
// import Carousel, { Modal, ModalGateway } from "react-images";
// import { photos } from "./photos";
// import "bootstrap/dist/css/bootstrap.css";

class Emicalc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalamount: '0',
      intrest: '1',
      downpayment: '0',
      term: '1',
      emi: '0'

    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
 
  calculateEMI =  () => {
    
    let actualLoanAmount = this.state.totalamount - this.state.downpayment;
    let interest = (this.state.intrest / (12 * 100));
    let timeInMonths = this.state.term;
    let calculatedEMI = (actualLoanAmount * interest * Math.pow(1 + interest, timeInMonths))  
                / (Math.pow(1 + interest, timeInMonths) - 1);
    this.state.emi = Math.round(calculatedEMI);
    
  }
  myTotalAmountChangeHandler = (event) => {
    this.state.totalamount = event.target.value;
    this.setState({ totalamount: event.target.value });
    this.calculateEMI();
    
  }
  myIntrestChangeHandler = (event) => {
    this.state.intrest = event.target.value;
    this.setState({ intrest: event.target.value });
    this.calculateEMI();
  }
  myDownPaymentChangeHandler = (event) => {
    this.setState({ downpayment: event.target.value });
    this.state.downpayment = event.target.value;
    this.calculateEMI();
  }
  myTermChangeHandler = (event) => {
    this.setState({ term: event.target.value });
    this.state.term = event.target.value;
    this.calculateEMI();
  }
  handleSubmit(e) {
   // alert(document.enquiry.name.value);
    e.preventDefault();
    fetch('https://aceimagingandvideo.com/beegru/website/savePropertyEnquiry', {
            method: 'POST',
            async: false,
            headers: {
                    //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                    "name": document.enquiry.name.value,
                    "mobileNumber": document.enquiry.mobileNumber.value,
                    "emailId": document.enquiry.emailId.value,
                    "message": document.enquiry.message.value,
                    "propertyId": localStorage.getItem("propertyId")

            })
    }).then((response) => response.json())
            .then((responseJson) => {
                    console.log(responseJson);
                    if(responseJson.httpStatus == 200){
                      alert("Property Enquiry Added Successfully");
                      document.getElementById("enquiryForm").reset();
                    }else{
                      alert("Error Occured, Try Again Later");
                    }
               //     this.handleClearForm(e);
            })
            .catch((error) => {
                    console.error(error);
            });
           
}
  render() {
    return (
      <div className="container">
        <div className="emiheading">
          <h1 className="emiText">EMI (Pay in Monthly installments)</h1>
        </div>
        <Form className="formborder">
          <Row className="form-group">
            <Col md="3">
              <label>Total Amount </label>
            </Col>
            <Col md="6">
              <div className="inputshow">Rs. {this.state.totalamount}</div>
              <input className="form-control-range inputrange" type="range" onChange={this.myTotalAmountChangeHandler} min="10000" max="2000000" step="1000" id="formControlRange1" />
            </Col>
          </Row>

          <Row className="form-group">
            <Col md="3">
              <label >Interest</label>
            </Col>
            <Col md="6">
              <div className="inputshow">{this.state.intrest}%</div>
              <input type="range" onChange={this.myIntrestChangeHandler} className="form-control-range inputrange" min="0%" max="50%" step="0.05" id="formControlRange2"></input>
            </Col>
          </Row>
          <Row className="form-group">
            <Col md="3">
              <label >DownPayment </label>
            </Col>
            <Col md="6">
              <div className="inputshow">Rs. {this.state.downpayment}</div>
              <input type="range" onChange={this.myDownPaymentChangeHandler} className="form-control-range inputrange" min="5000" max="100000" step="" id="formControlRange3" />
            </Col>
          </Row>
          <Row className="form-group">
            <Col md="3">
              <label>Term</label>
            </Col>
            <Col md="6">
              <div className="inputshow"> {this.state.term} Months</div>
              <input type="range" onChange={this.myTermChangeHandler} className="form-control-range inputrange" min="1" max="480" step="1" id="formControlRange4"></input>
            </Col>
          </Row>
          <Row className="form-group">
            <Col md="3">
              <label>EMI</label>
            </Col>
            <Col md="6">

            <div className="inputshow">Rs. {this.state.emi}</div>

            </Col>
          </Row>
        </Form>

        {/* Enquiry form  start*/}
        <div className="container">
      <h1 className="enquireheading">Enquire</h1>
      <Form className="enquirecontainer" onSubmit={this.handleSubmit} name="enquiry" id="enquiryForm">

        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Name </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control type="text" placeholder=" Name" className="" id = "name" />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Phone </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control type="text" placeholder="phone number" className="" id = "mobileNumber"/>
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Email </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control type="text" placeholder="name@example.com" className="" id = "emailId"/>
            </Col>
          </Row>
        </Form.Group>
        <Form.Group className="form-group">
          <Row>
            <Col md="3">
              <Form.Label>Message </Form.Label>
            </Col>
            <Col md="6">
              <Form.Control as="textarea" placeholder="type here" rows="3" className="" id = "message" />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group as={Row}>
          <Col sm={{ span: 10, offset: 4 }}>
          <Button type="submit" className="btn btn-danger center" size="md" >
            Submit
          </Button>
          </Col>
        </Form.Group>
      
      </Form>
    </div >
        {/* Enquiry form end */}


      </div>



    );
  }
}


export default Emicalc;

