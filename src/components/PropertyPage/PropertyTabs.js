import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import '../../css/propertyTabs.css';
import ProgressBar from 'react-bootstrap/ProgressBar'


export default class PropertyTabs extends Component {
  constructor(props) {
    super(props);
    this.beegruScore = this.beegruScore.bind(this);
    //this.aboutProject = this.aboutProject.bind(this);        
    this.aboutLocation = this.aboutLocation.bind(this);
    this.createUlLiListSttucture = this.createUlLiListSttucture.bind(this);
    this.loadData = this.loadData.bind(this);
    this.aboutProjectData = undefined;
    this.state = [{
      aboutProjectData: undefined,
      navBar: undefined
    }]
   this.data = this.props.data;
   this.list = this.props.apiResponse;
   
  }

  loadData() {
    let that = this;
    let data = this.data.propertyTabs;
    let propTabs = this.data.propertyTabsKey;
    this.state.navBar = propTabs.map((elem) => {
      return <li className="nav-item">
        <a className="nav-link" href="#" tabindex="-1">{elem}</a>
      </li>
    });
    this.setState({
      loadData: data.map((elem) => {

        switch (elem.id) {
          case 'aboutProject':
            console.log("Gaurav:::about Proj", elem);
            let aboutProject = this.list.aboutProperty.split("\n");
            
            const aboutProjectListItems = aboutProject.map((value) => {
              console.log("Gaurav:::aboutProject Value", value);
              return <li>{value}</li>
            });
            return (
              <div className="aboutProjectUlWrapper">
                <h5 className="headingLine">{elem.text}</h5>
                <ul className="aboutProjectUl">
                  {aboutProjectListItems}
                </ul>
              </div>
            );
            break;
          case 'aboutLocation':
            console.log("Gaurav:::data Abt Loc", elem);
            let aboutLocation = this.list.aboutLocation.split("\n");
            
            let aboutLocationListItems = aboutLocation.map((value) => {
              console.log("Gaurav:::aboutLocation Value", value);
              return <li>{value}</li>
            });
            return (
              <div className="aboutLocationUlWrapper">
                <h5>{elem.text}</h5>
                <ul className="aboutLocationUl">
                  {aboutLocationListItems}
                </ul>
              </div>
            );
            break;
          case 'beegruScore':
            console.log("Gaurav:::data Beegru Scr", elem);
            let beegruScoreListItems =<ul className="beegruScoreUl">
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                            Legal and Bank Support
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.legalAndBanksSupport} />
                                          </div>
                                        </div>
                                      </li>
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                          Approval stage
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.approvalStage} />
                                          </div>
                                        </div>
                                      </li>
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                          Accesibility
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.accesibility} />
                                          </div>
                                        </div>
                                      </li>
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                          Vaastu & Location
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.vaastuAndLocation} />
                                          </div>
                                        </div>
                                      </li>
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                          Price Value
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.priceValue} />
                                          </div>
                                        </div>
                                      </li>
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                          Neighbourhood
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.neighbourhood} />
                                          </div>
                                        </div>
                                      </li>
                                      <li className="beegruScore2Col">
                                        <div className="liWrapper">
                                          <div className="progressBarKey">
                                          Appreciation Potential
                                          </div>
                                          <div className="progressBarUpdate">
                                            <ProgressBar now={this.list.beegruScore.appreciationPotential} />
                                          </div>
                                        </div>
                                      </li>
                                      </ul>
              
            return (
              <div className="beegruScoreUlWrapper">
                <h5>{elem.text}</h5>
                  {beegruScoreListItems}
              </div>
            );
            break;
          case 'propertAmenities':
            console.log("Gaurav:::data Prop Ame", elem);
            let amenites = this.list.amenities.split(",");
            amenites.pop();
            let propertAmenitiesListItems = amenites.map((value) => {
              // let unicode = value.unicode;
              // let classNameVal = value.className;
              console.log("Gaurav:::propertAmenities Value", value);
              return <li>
                <div className="liWrapper">
                  { value == 'ac' &&
                    <div className="unicode" style={{ fontFamily: "FontAwesome", "content": "f863" }}>
                    <i className="fas fa-dumbbell">
                    </i>
                  </div>

                  }
                  { value == 'gym' &&
                    <div className="unicode" style={{ fontFamily: "FontAwesome", "content": "f44b" }}>
                    <i className="fas fa-dumbbell">
                    </i>
                  </div>

                  }
                  { value == 'laundry' &&
                    <div className="unicode" style={{ fontFamily: "FontAwesome", "content": "f898" }}>
                    <i className="fas fa-washer">
                    </i>
                  </div>

                  }
                  
                  <div className="unicodeText">
                    {value}
                  </div>
                </div>
              </li>
            });
            return (
              <div className="propertAmenitiesWrapper">
                <h5>{elem.text}</h5>
                <ul className="propertAmenities">
                  {propertAmenitiesListItems}
                </ul>
              </div>
            );
            break;
          case 'propertLocation':
            console.log("Gaurav:::data Prop Loc", elem);
            return <div className="propLocation">
              <div className="locationTextWrapper">
                <div>Property location</div>
            <div>{this.list.propertyLocation}</div>
              </div>
              <div className="buttonPropLocation">
                View in Map
                          </div>
            </div>
            break;
          case 'propertVicinity':
            console.log("Gaurav:::data Proj Vic", elem);
            let propertVicinityListItems = this.list.propertyVicinity.map((propertyVicinity) => {
            let id = propertyVicinity.propertyVicinityType;
            let aboutProject = propertyVicinity.propertyVicinityDescription.split("\n");
            let nearByVal = aboutProject.map((value) => {
                return <div className="vicinityRowWrapper">
                  {value}
                </div>
            
              
            });
              return <li>
                <div className="liWrapper">
                  <div className="propertVicinityId"  >
                    {id}
                  </div>
                  <div className="vicinityWrapper">
                    {nearByVal}
                  </div>
                </div>
              </li>
            });
            return (
              <div className="propertVicinityWrapper">
                <h5>{elem.text}</h5>
                <ul className="propertVicinity">
                  {propertVicinityListItems}
                </ul>
              </div>
            );

            break;
        }
      })
    });
  }
  createUlLiListSttucture(elem) {
    if (elem.length) {
      const listItems = elem.map((el) =>
        <li>{el.value}</li>
      );
      return (
        <ul>{listItems}</ul>
      );
    }
  }
  aboutLocation() {

  }
  beegruScore() {

  }
  componentDidMount() {
    let that = this;
    this.loadData();
  }
  render() {
    console.log("Gaurav:::render");
    return (
      <div id="propertyTabs">
        <Container>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">

            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav propertyNavTabs">
                {this.state.navBar}
              </ul>
            </div>
          </nav>

          {this.state.loadData}
        </Container>
      </div>
    );
  }
}

