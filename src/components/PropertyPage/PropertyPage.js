import React from 'react';
import PropertyTabs from "./PropertyTabs";
import EmiCalculator from "./EmiCalculator";
import '../../css/emi.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from "../models/Login.js";
// import ImageGallery from "../Gallery/ImageGallery";
//import { Container } from 'react-bootstrap';
import NewProperties from "../Home/NewProperties";
import PropertyImage from "../PropertyPage/PropertyImage";
import { createGlobalStyle } from "styled-components";
import { createPdfFromHtml } from "./logic";
import { UncontrolledPopover, PopoverBody } from 'reactstrap';

const Global = createGlobalStyle`
 
  
  /** Paper sizes **/
  #print.A3               .sheet { width: 297mm; height: 419mm }
  #print.A3.landscape     .sheet { width: 420mm; height: 296mm }
  #print.A4               .sheet { width: 210mm; height: 296mm }
  #print.A4.landscape     .sheet { width: 297mm; height: 209mm }
  #print.A5               .sheet { width: 148mm; height: 209mm }
  #print.A5.landscape     .sheet { width: 210mm; height: 147mm }
  #print.letter           .sheet { width: 216mm; height: 279mm }
  #print.letter.landscape .sheet { width: 280mm; height: 215mm }
  #print.legal            .sheet { width: 216mm; height: 356mm }
  #print.legal.landscape  .sheet { width: 357mm; height: 215mm }
  
  /** Padding area **/
  .sheet.padding-10mm { padding: 10mm }
  .sheet.padding-15mm { padding: 15mm }
  .sheet.padding-20mm { padding: 20mm }
  .sheet.padding-25mm { padding: 25mm }
  
  
  /** Fix for Chrome issue #273306 **/
  @media print {
    #print.A3.landscape            { width: 420mm }
    #print.A3, #print.A4.landscape { width: 297mm }
    #print.A4, #print.A5.landscape { width: 210mm }
    #print.A5                      { width: 148mm }
    #print.letter, #print.legal    { width: 216mm }
    #print.letter.landscape        { width: 280mm }
    #print.legal.landscape         { width: 357mm }
  }
`;

export default class PropertyPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: {}
    }

    this.data = {
      "id": "property1001",
      "propertyData": {
 
      },
      "images": [
        {
          "mobile": "/static/media/mobile.jpg",
          "tablet": "../images/homewebsite.png",
          "laptop": "/static/media/mobile.jpg",
          "hd": "/static/media/mobile.jpg"
        }
      ],
      "propertyTabsKey": ["About", "Amenities", "Map", "Vicinity", "Calculate Emi"],
      "propertyTabs": [
        {
          "text": "ABOUT Project",
          "id": "aboutProject",
          "aboutProject": [
            {
              "value": "BBP Limits"
            }, {
              "value": "OC and CC obtained"
            }, {
              "value": "Budget friendly ( Starting 47L ) "
            }, {
              "value": "14 flats sold and lively resident community"
            }, {
              "value": "24/7 security and surveillance"
            }, {
              "value": "24/7 water supply"
            }
          ]
        },
        {
          "text": "About Location",
          "id": "aboutLocation",
          "aboutLocation": [
            {
              "value": "0.5 km from Wipro Main gate Sarjapur Road"
            }, {
              "value": "Near all softeare tech parks"
            }, {
              "value": "Building viewable from an entire neighbourhood"
            }, {
              "value": "Walkable distance to supermarket, schools, fine dining restaurant, breweries, boutiques, and malls"
            }, {
              "value": "Away from bypass road so less traffic and less pollution"
            }, {
              "value": "Walkable distance to the heart of evergrowing sarjapur road"
            }
          ]
        },
        {
          "text": "Beegru Score",
          "id": "beegruScore",
          "beegruScore": [
            {
              "key": "Legal nd Bank Support",
              "value": "80"
            },
            {
              "key": "Approval stage",
              "value": "60"
            },
            {
              "key": "Accesibility",
              "value": "80"
            },
            {
              "key": "Vaastu & Location",
              "value": "80"
            },
            {
              "key": "Price Value",
              "value": "50"
            },
            {
              "key": "Neighbourhood",
              "value": "90"
            },
            {
              "key": "Appreciation Potential",
              "value": "80"
            }
          ]
        },
        {
          "text": "Amenities",
          "id": "propertAmenities",
          "propertAmenities": [
            {
              "key": "ac",
              "unicode": "\\f863;",
              "value": "AC",
              "className": "fas fa-fan"
            }, {
              "key": "gym",
              "unicode": "\\f44b;",
              "value": "GYM",
              "className": "fas fa-dumbbell"
            }, {
              "key": "laundry",
              "unicode": "f898;",
              "value": "Laundry",
              "className": "fas fa-washer"
            }, {
              "key": "cctv",
              "unicode": "f030;",
              "value": "CCTV",
              "className": "fas fa-camera"
            }, {
              "key": "security",
              "unicode": "f2f7;",
              "value": "SECURITY",
              "className": "fas fa-shield-check"
            }, {
              "key": "wifi",
              "unicode": "f1eb",
              "value": "WIFI",
              "className": "fas fa-wifi"
            }, {
              "key": "pool",
              "unicode": "f5c4",
              "value": "POOL",
              "className": "fas fa-swimmer"
            }

          ]
        },
        {
          "text": "MAP",
          "id": "propertLocation",
          "propertLocation": [
            {
              "address": "Kesav Nagar, Bangalore",
              "latitude": "",
              "longitude": ""
            }
          ]
        },
        {
          "text": "VICINITY",
          "id": "propertVicinity",
          "propertVicinity": [
            {
              "id": "Schools",
              "nearBy": [
                {
                  "name": "OldField Primary School",
                  "distanceFrom": "0.6 miles"
                },
                {
                  "name": "Guilden Sutton Cofe primary schools",
                  "distanceFrom": "0.7 miles"
                },
                {
                  "name": "The Hammold School",
                  "distanceFrom": "0.8 miles"
                },
                {
                  "name": "The bishop Blue Coat High School",
                  "distanceFrom": "1.6 miles"
                }
              ]
            },
            {
              "id": "Restaurant",
              "nearBy": [
                {
                  "name": "OldField Primary School",
                  "distanceFrom": "0.6 miles"
                },
                {
                  "name": "Guilden Sutton Cofe primary schools",
                  "distanceFrom": "0.7 miles"
                },
                {
                  "name": "The Hammold School",
                  "distanceFrom": "0.8 miles"
                },
                {
                  "name": "The bishop Blue Coat High School",
                  "distanceFrom": "1.6 miles"
                }
              ]
            },
            {
              "id": "Malls",
              "nearBy": [
                {
                  "name": "OldField Primary School",
                  "distanceFrom": "0.6 miles"
                },
                {
                  "name": "Guilden Sutton Cofe primary schools",
                  "distanceFrom": "0.7 miles"
                },
                {
                  "name": "The Hammold School",
                  "distanceFrom": "0.8 miles"
                },
                {
                  "name": "The bishop Blue Coat High School",
                  "distanceFrom": "1.6 miles"
                }
              ]
            },
            {
              "id": "Hospitals",
              "nearBy": [
                {
                  "name": "OldField Primary School",
                  "distanceFrom": "0.6 miles"
                },
                {
                  "name": "Guilden Sutton Cofe primary schools",
                  "distanceFrom": "0.7 miles"
                },
                {
                  "name": "The Hammold School",
                  "distanceFrom": "0.8 miles"
                },
                {
                  "name": "The bishop Blue Coat High School",
                  "distanceFrom": "1.6 miles"
                }
              ]
            }
          ]
        },
        {
          "text": "CALCULATE EMI",
          "id": "calculateEmi"
        }
      ],
      "propertyDetails": [
        {
          "propertyName": "Jaz Aquaviva Hurghada",
          "propertyAddess": "Bangalore",
          "propertyType": "House Tolet/For Sale",
          "propertyPrice": "53,88,678",
          "propertyPerSqFt": "5757\/sqft",
          "propertyPriceIcons": [{
            "icon": "upload",
            "key": "upload",
            "value": "upload"
          },
          {
            "icon": "floppy",
            "key": "floppy",
            "value": "floppy"
          }],
          "propertyIcons": [{
            "icon": "room",
            "key": "roomPSqFt",
            "value": "1600 sq Ft"
          },
          {
            "icon": "beds",
            "key": "beds",
            "value": "2 beds"
          },
          {
            "icon": "bath",
            "key": "bath",
            "value": "1 bath"
          },
          {
            "icon": "garage",
            "key": "garage",
            "value": "1 garage"
          }],
          "propertyAddress": "Pole star city barajod Toll Plaza, Kanpur",
          "name": "property 1",
          "heading": "1",
          "synopsis": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic ",
          "priceRange": "120000-320000",
          "minPrice": "120000",
          "maxPrice": "320000",
          "currency": "rupee",
          "type": "independent House",
          "location": "anekalgooglemaplink",
          "locality": "Anekal",
          "state": "Karnataka",
          "propertyAction": "For Sale",
          "pricePerSqft": "2400 Sq ft",
          "roomDetails": [
            {
              "bedRoom": "2",
              "bedRoomType": "onoe master/one normal",
              "bathroom": "2",
              "bathroomType": "one attached/one separate",
              "flooring": "marble",
              "studyRoom": "yes",
              "beds": "3"
            }],
          "roomShortMetadata": {
            "icons": {
              "bedRoom": "2",
              "bathRoom": "2",
              "beds": "2"
            }
          }
        }]
    };
  }
  printContent;
  display = (data) => {
    console.log(data);
    this.setState({ list: data });
  }
  handleClick = () => {
    createPdfFromHtml(this.printContent);
  };
  componentDidMount() {
    fetch('https://aceimagingandvideo.com/beegru/website/fetchSingleProperties', {
      method: 'POST',
      async: false,
      headers: {
       // "Authorization": "Basic " + btoa("8970685540" + ":" + "776447"),
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "propertyId": localStorage.getItem('propertyId'),
        "mobileNumber":"4324"
      })
    }).then(response => response.json())
      .then(this.display)
      .catch()
  }

  addOrDeleteWishList(propertyId) {
    let element = document.getElementById('productId' + propertyId);


    fetch('https://aceimagingandvideo.com/beegru/login/saveWishList', {
      method: 'POST',
      async: false,
      crossDomain: false,
      headers: {
        "Authorization": "Basic " + btoa("9902376090" + ":" + "swa123thi"),
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "propertyId": propertyId
      })
    }).then(response => {
      return response.json();
    }).then(result => {
      console.log(result.resultStatus);
      if (result.resultStatus == 1) {
        //    alert("dsdsd");
        element.style.color = 'red';
        element.style.fontWeight = '900';
      } else if (result.resultStatus == 3) {
        element.style.color = 'black';
        element.style.fontWeight = '100';
      }
    });

  }

  Sheet() {
    return (
      <div id="print" className="A4">
        <div id="propertyPage">


          <div className="gallary container">

            <div className="propGallaryWrapperData">
              <div className="propGallaryData">
                <div className="propName">{this.state.list.propertyName}</div>
                <div className="propAddress">{this.state.list.propertyLocation}</div>
                <div className="propType">{this.state.list.propertyType}-{this.state.list.propertyStatus}</div>
                {/*   <div className="propIcons">{this.state.list.propertyIcons.icon}</div>*/}
              </div>
              <div className="propGallaryDataPrice">
                <div className="propPrice">{this.state.list.displayPrice}</div>
                {this.state.list.propertyType === 'Resedential' &&
                  <div className="propPerSqFt">{this.state.list.length * this.state.list.breadth} Sq Ft</div>
                }
              </div>
            </div>
            <div className="propertyMetadata">
              {/* propertyMetadata componenet */}
              {Object.keys(this.state.list).length > 0 &&
                <PropertyTabs data={this.data} apiResponse={this.state.list} />
              }

            </div>
          </div>
        </div>
      </div>
    );
  };


  render() {
    let sheet = this.Sheet();
    let wishList;
    let modalClose = () =>
      this.setState({
        modalShow: false
      });
    if (this.state.list.propertyLiked) {
      var style = {
        color: 'red',
        fontWeight: '900',
      };
      this.wishListIcon = 'fa fa-heart';
    } else {
      var style = {
        fontWeight: 'normal',
        color: 'black',
      };
    }

    if (!this.state.isloaded) {
      wishList = (
        <Link onClick={() => this.setState({ modalShow: true })}><i className="fa fa-heart"></i></Link>

      );
    } else {
      wishList = (
        <i className="fa fa-heart" onClick={() => this.addOrDeleteWishList(this.state.list.propertyId)} style={style} id={"productId" + this.state.list.propertyId}></i>
      );
    }
    return (
      <div id="propertyPage">

            {Object.keys(this.state.list).length > 0 &&
              <PropertyImage apiResponse={this.state.list}/>
            }

        <div className="gallary container">
          {/* Gallary componenet */}
          <div className="propGallaryWrapperData">
            <div className="propGallaryData">
              <div className="propName">{this.state.list.propertyName}</div>
              <div className="propAddress">{this.state.list.propertyLocation}</div>
              <div className="propType">{this.state.list.propertyType}-{this.state.list.propertyStatus}</div>
              {/*   <div className="propIcons">{this.state.list.propertyIcons.icon}</div>*/}
            </div>
            <div className="propGallaryDataPrice">
              <div className="propPrice">{this.state.list.displayPrice}</div>
              {this.state.list.propertyType === 'Resedential' &&
                <div className="propPerSqFt">{this.state.list.length * this.state.list.breadth} Sq Ft</div>
              }
              <div className="propIcons">

                {wishList}
                <Login
                  show={this.state.modalShow}
                  onHide={modalClose}
                />
                {/* <i className="far fa-heart"></i> */}
                {/*<i className="fas fa-share-alt"></i>*/}
                <div id="share">
                  <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like to Share"></i>

                </div>
                <UncontrolledPopover trigger="legacy" placement="bottom" target="share">
                  {/* <PopoverHeader>Legacy Trigger</PopoverHeader> */}
                  <PopoverBody>
                    <div className="popicon">
                      <Link><i className='fab fa-facebook-f'></i></Link>

                      <Link > <i className='fab fa-twitter' ></i></Link>

                      <Link ><i className='fab fa-instagram' ></i></Link>

                      <Link ><i className='fab fa-whatsapp' ></i></Link>
                    </div>
                  </PopoverBody>
                </UncontrolledPopover>

                <i className="fas fa-download" onClick={this.handleClick}></i>
                {/* {this.data.propertyDetails[0].propertyPriceIcons.icon} */}

              </div>



            </div>
          </div>


          <div className="propertyMetadata">
            {/* propertyMetadata componenet */}
            {Object.keys(this.state.list).length > 0 &&
              <PropertyTabs data={this.data} apiResponse={this.state.list} />
            }

          </div>
        </div>
        <Global />
        {/* <button onClick={this.handleClick}>print</button> */}
        <div id="print" >

          <div style={{ position: "fixed", top: "350vh" }}>
            <div
              ref={el => {
                this.printContent = el;
              }}
            >
              {sheet}
            </div>
          </div>
        </div>
        <EmiCalculator />
        <NewProperties />


      </div>
    )
  }
}




