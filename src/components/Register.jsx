import React, { Component } from "react";
import {
  Form,
  Row,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup,
  Container,
  Image,
  Card,
  CardDeck
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import { Link } from "react-router-dom";
import "../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import icon from "./../images/logoIcon.png";
import HomeIcon from "./../images/HomeIcon.png";
import avtar1 from "./../images/img_avatar.png";
import avtar2 from "./../images/img_avatar2.png";
import avtar3 from "./../images/img_avatar3.png";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Register extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
    this.state = { modalShow: false };
  }
 
  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
    
  }

  handleGameClik() {
    this.setState( {disabled: !this.state.disabled} )
  } 

  render() {
    const { validated } = this.state;
    let modalClose = () =>
      this.setState({
        modalShow1: false
      });

      let otpModal = () =>
      this.setState({
        modalShow1: true
      });

      let closeRegistrationModal = () =>
      this.setState({
        modalShow: false
      });

      function onClick() {
        if(document.registration.regPhoneNumber.value ==""){
          alert("Please enter your Mobile Number")
        }else if(document.registration.regPhoneNumber.value.length <= 9){
          alert("Please enter valid mobile number")
        }else{          
          sendOTP();
        }
      }


      function sendOTP(){
        // alert(document.registration.regPhoneNumber.value);
        fetch('https://aceimagingandvideo.com/beegru/website/sendOtp', {
          method: 'POST',
          async: false,
          headers: {
                  //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({                  
                  "mobileNumber": document.registration.regPhoneNumber.value                 
          })
      }).then((response) => response.json())
          .then((responseJson) => {
                  console.log(responseJson);
                  if(responseJson.resultStatus == 2){
                    alert("Mobile Number Already Registered");
                    // closeOtpModal();
                  }else if(responseJson.resultStatus == 1){
                    otpModal();
                  } else{
                    alert("Server Error");
                  }               
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
      }

      function verifyRegistration(){
        console.log(document.registration.regLocation.value);
        console.log(document.registration.regMessage.value);
        if(statusVariable == 1){
        fetch('https://aceimagingandvideo.com/beegru/website/signup', {
                            method: 'POST',
                            async: false,
                            headers: {
                                    //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                    "name": document.registration.regName.value,
                                    "mobileNumber": document.registration.regPhoneNumber.value,
                                    "emailId": document.registration.regEmail.value,
                                    "location": document.registration.regLocation.value,
                                    "password": document.registration.regPassword.value,
                                    "address": document.registration.regAddress.value,
                                    "aboutme": document.registration.regAboutMe.value,
                                    "message": document.registration.regMessage.value
                            })
                    }).then((response) => response.json())
                            .then((responseJson) => {
                                    console.log(responseJson);
                                    console.log("Response",responseJson.resultStatus);
                                    if(responseJson.resultStatus == 2){
                                      alert("Email ID already Registered");
                                      //document.getElementById("registrationForm").reset();
                                    }else if(responseJson.resultStatus == 1){
                                        alert("Registration Successfull");
                                        document.getElementById("registrationForm").reset();
                                        window.location.reload();
                                        closeRegistrationModal();
                                    }else{
                                      alert("Error");
                                    }
                                    return responseJson.movies;
                            })
                            .catch((error) => {
                                    console.error(error);
                            });
                          }else{
                          alert("Please verify your mobile number");
                          }
      }

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
            name="registration"
            id="registrationForm"
          >
            <Form.Row>
              <Form.Group as={Col} md="6" sm="6">
                <Button type="submit" className="loginfacebook" size="md" block>
                  <i className="fab fa-facebook-f"></i> Facebook
                </Button>
              </Form.Group>
              <Form.Group as={Col} md="6" sm="6">
                <Button
                  variant="white gray border"
                  type="submit"
                  size="md"
                  block
                >
                  <i className="fab fa-google"></i> Google
                </Button>
              </Form.Group>
            </Form.Row>
            <hr className="hr-text" data-content=" or "></hr>
            <Form.Group as={Col} md="12" controlId="validationCustom01">
              <Form.Label>NAME * </Form.Label>
              <Form.Control
                className="controll"
                required
                type="text"
                placeholder="Name"
                id="regName"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
           
            <Form.Group as={Col} md="12" controlId="validationCustom03">
              <Form.Label>PHONE *</Form.Label>
              <InputGroup className="mb-3">
                <Form.Control
                  className="controll"                  
                  type="text"
                  placeholder="Phone"
                  id="regPhoneNumber"
                  disabled = {(this.state.disabled)? "disabled" : ""}                  
                />
                <Form.Control.Feedback type="invalid">
                  must be 10 numbers. Please provide a valid Phone number.
                </Form.Control.Feedback>
                <InputGroup.Append>
                  <Button variant="outline-secondary" className="btn btn-danger"
                  onClick={onClick}>Sent OTP</Button>
                </InputGroup.Append>
              </InputGroup>
              <OtpModal show={this.state.modalShow1} onHide={modalClose} />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom04">
              <Form.Label>EMAIL *</Form.Label>
              <Form.Control
                className="controll"
                type="email"
                placeholder="Email"
                id="regEmail"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Email id.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>PASSWORD *</Form.Label>
              <Form.Control
                className="controll"
                type="password"
                placeholder="Password"
                id="regPassword"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
              <Form.Text className="red">
                Must be more than 8 Characters.
              </Form.Text>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>LOCATION *</Form.Label>

              <Form.Control
                className="controll"
                type="Location"
                as="select"
                placeholder="location"
                id="regLocation"
                required
              >
                <option value="Bangalore">Bangalore</option>
                <option value="Mysore">Mysore</option>
                <option value="Chitradurga">Chitradurga</option>
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom06">
              <Form.Label>ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Address"
                id="regAddress"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Address.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom07">
              <Form.Label>About me *</Form.Label>
              <Form.Control
                className="controll"
                type="textarea"
                as="textarea"
                placeholder="About me "
                id="regAboutMe"
                required
              />
              <Form.Control.Feedback type="invalid">
                write About me.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom08">
              <Form.Label>
                How can Beegru help with respect to your property needs?
              </Form.Label>
              <Form.Control
                className="controll"
                type="textarea"
                as="textarea"
                placeholder="Write a message "
                id="regMessage"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid message, atleast 10 words.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group>
              <Form.Check
              className="bordernone"
                required
                label="I have read and agree to the Privacy Policy and 
                 Terms of use*"
                feedback="You must agree before submitting."
              />
            </Form.Group>

            <Form.Group>
              <Button type="submit"  onClick={verifyRegistration} className="btn btn-danger" size="md" block>
                Register
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export  class  Login extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
    this.state = { modalShow: false };
  }

   handleSubmit(event) {
    const form = event.currentTarget;
     if (form.checkValidity() === false) {
       event.preventDefault();       
        event.stopPropagation();
     }
     this.setState({ validated: true });
     
   }

  render() {
    const { validated } = this.state;
    let modalClose = () =>
      this.setState({
        modalShow2: false
      });

      let loginmodalClose = () =>
      this.setState({
        modalShow1: false
      });

      let registrationModalOpen = () =>
      this.setState({
        modalShow2: true
      });

      
      function registrationModal(){
        loginmodalClose();
        registrationModalOpen();
      }

      function verifyLogin(){        
        fetch('https://aceimagingandvideo.com/beegru/website/login', {
          method: 'POST',
          async: false,
          headers: {                               
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                  "mobileNumber": document.login.loginEmailId.value,
                  "password": document.login.loginPassword.value                                
          })
  }).then((response) => response.json())
          .then((responseJson) => {                  
                  console.log(responseJson);
                  if(responseJson.resultStatus == 2){
                    alert("Invalid Credentials");                    
                  }else if(responseJson.resultStatus == 1){                                           
                      localStorage.setItem('loginStatus', 'loginStatus');
                      localStorage.setItem('mobileNumber', document.login.loginPassword.value);
                      alert('Login Successful');
                      document.getElementById("loginForm").reset();
                      window.location.reload();
                      //onHide={modalClose}                 
                  }else if(responseJson.resultStatus == 3){
                    alert("Not approved by admin");                    
                 }else{
                    alert("Error");
                  }
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
      }

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              <Col className="Loginmodaldiv1">
                <Image src={icon} fluid />
                <Image src={HomeIcon} fluid />

                <p>
                  Find the best matches for you Make the most of high seller
                  scores Experience a joyful journey
                </p>
              </Col>
              <Col className="Loginmodaldiv2">
                <Form
                  noValidate
                  validated={validated}
                  onSubmit={e => this.handleSubmit(e)}
                  name="login"
                  id="loginForm"
                >
                  <Form.Group as={Col} md="12" controlId="validationCustom11">
                    <Form.Label>EMAIL or MOBILE </Form.Label>
                    <Form.Control
                      className="controll"
                      type="email"
                      placeholder="Email or Mobile"
                      id="loginEmailId"
                      required
                    />
                    <Form.Control.Feedback type="invalid">
                      must be 8 characters strong. Please provide a valid email.
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group as={Col} md="12" controlId="validationCustom12">
                    <Form.Label>PASSWORD</Form.Label>
                    <Form.Control
                      className="controll"
                      type="password"
                      placeholder="Password"
                      id="loginPassword"
                      required
                    />
                    <Form.Control.Feedback type="invalid">
                      must be 8 characters strong. Please provide a valid
                      Password.
                    </Form.Control.Feedback>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>
                      <Link href="#">Login with OTP</Link>
                    </Form.Label>
                  </Form.Group>
                  <Form.Group as={Col} md="11">
                    <Button
                      type="submit"                      
                      onClick={e => {e.preventDefault();this.setState({modalShow5: false}); {verifyLogin()}}}
                      className="btnFullwidth loginButton"
                      size="md"
                      block
                    >
                      Login
                    </Button>

                    <hr className="hr-text" data-content="or login via"></hr>

                    <Button
                      className="btnFullwidth loginfacebook"
                      size="md"
                      block
                    >
                      <i className="fab fa-facebook-f"></i> Facebook
                    </Button>

                    <Button
                      variant="white gray border"
                      className="btnFullwidth"
                      size="md"
                      block
                    >
                      <i className="fab fa-google"></i> Google
                    </Button>
                  </Form.Group>
                  <Form.Group className="login">
                    <p>
                      {" "}
                      Don't have an account? Please
                      {/* <Link href="#"> Register</Link> */}
                      <Link onClick={registrationModal}>
                        Register
                      </Link>
                      <Register
                        show={this.state.modalShow2}
                        onHide={modalClose}
                      />
                    </p>

                    <p>
                     
                      <Link onClick={() => this.setState({ modalShow19: true })}>
                      Forgot your password?
                      </Link>
                      <ForgotPassword
                        show={this.state.modalShow19}
                        onHide={modalClose}
                      />
                    </p>

                  </Form.Group>
                </Form>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
      </Modal>
    );
  }
}

export class RegisterasAgent extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
    this.state = { modalShow: false };
  }
  

  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
  }

  render() {
    const { validated } = this.state;

    let modalClose = () =>
    this.setState({
      modalShow1: false
    });

    let otpModal = () =>
    this.setState({
      modalShow1: true
    });

    let closeRegistrationModal = () =>
    this.setState({
      modalShow: false
    });


    function verifyAgentRegistration(){
      alert(document.agentregistration.regagentName.value);
      alert(document.agentregistration.regagentPhoneNumber.value);
      alert(document.agentregistration.regagentEmail.value);
      alert(document.agentregistration.regagentPassword.value);
      alert(document.agentregistration.regagentAddress.value);
      alert(document.agentregistration.regagentLocation.value);
     // if(statusVariable == 1){
      fetch('https://aceimagingandvideo.com/beegru/website/saveAgent', {
                          method: 'POST',
                          async: false,
                          headers: {
                                  //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                  Accept: 'application/json',
                                  'Content-Type': 'application/json',
                          },
                          body: JSON.stringify({
                                  "name": document.agentregistration.regagentName.value,
                                  "mobileNumber": document.agentregistration.regagentPhoneNumber.value,
                                  "emailId": document.agentregistration.regagentEmail.value,
                                  "password": document.agentregistration.regagentPassword.value,
                                  "address": document.agentregistration.regagentAddress.value,
                                  "activeStatus": 2,
                                  "roles":[
                                    {
                                    "roleId":"5"
                                    }
                                    ],
                                    "location":document.agentregistration.regagentLocation.value,
                                    "agentDetails":{

                                    }
                                   })
                  }).then((response) => response.json())
                          .then((responseJson) => {
                                  console.log(responseJson);
                                  alert("Response Shruthi Run "+responseJson.resultStatus);
                                  if(responseJson.resultStatus == 3){
                                    alert("Email ID already Registered");
                                    //document.getElementById("agentregistrationForm").reset();
                                  }else if(responseJson.resultStatus == 2){
                                    alert("mobile number already Registered");
                                    //document.getElementById("agentregistrationForm").reset();
                                  }
                                  else if(responseJson.resultStatus == 1){
                                      alert("Registration Successfull");
                                      document.getElementById("agentregistrationForm").reset();
                                      //window.location.reload();
                                      closeRegistrationModal();
                                  }else{
                                    alert("Error");
                                  }
                                 
                          })
                          .catch((error) => {
                                  console.error(error);
                          });
                        // }else{
                        // alert("Please verify your mobile number");
                        // }
    }


    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Register as agent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
            name="agentregistration"
            id="agentregistrationForm"
          >
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>NAME *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Name"
                id="regagentName"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom03">
              <Form.Label>PHONE *</Form.Label>
              {/* <InputGroup className="mb-3"> */}
                <Form.Control
                  className="controll"                  
                  type="text"
                  placeholder="Phone"
                  id="regagentPhoneNumber"
                  disabled = {(this.state.disabled)? "disabled" : ""}                  
                />
                <Form.Control.Feedback type="invalid">
                  must be 10 numbers. Please provide a valid Phone number.
                </Form.Control.Feedback>
                {/* <InputGroup.Append>
                  <Button variant="outline-secondary" className="btn btn-danger"
                  onClick={onClick}>Sent OTP</Button>
                </InputGroup.Append>
              </InputGroup>
              <OtpModal show={this.state.modalShow1} onHide={modalClose} />*/}
            </Form.Group> 

            <Form.Group as={Col} md="12" controlId="validationCustom04">
              <Form.Label>EMAIL *</Form.Label>
              <Form.Control
                className="controll"
                type="email"
                placeholder="Email"
                id="regagentEmail"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Email id.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>PASSWORD *</Form.Label>
              <Form.Control
                className="controll"
                type="password"
                placeholder="Password"
                id="regagentPassword"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
              <Form.Text className="blue">
                Must be more than 8 Characters.
              </Form.Text>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom22">
              <Form.Label>ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Address"
                id="regagentAddress"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom23">
              <Form.Label>SPECIALITY *</Form.Label>

              <Form.Control
                className="controll"
                type="speciality"
                as="select"
                placeholder="Speciality"
                id="regagentSpeciality"
                required
              >
                <option>Speciality</option>
                <option>bangalore</option>
                <option>mysore</option>
                <option>chitradurga</option>
              </Form.Control>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom24">
              <Form.Label>LOCATION I AM EXPECT IN *</Form.Label>

              <Form.Control
                className="controll"
                type="Location"
                as="select"
                placeholder="Locations I am expert in"
                id="regagentLocation"
                required
              >
                <option>Location i am expert in</option>
                <option>bangalore</option>
                <option>mysore</option>
                <option>chitradurga</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                type="button"
                onClick={verifyAgentRegistration}
                className="btn btn-danger btnFullwidth"
                size="lg"
                block
              >
                Register As Agent
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class ListyourServices1 extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

  render() {
    let modalClose = () =>
      this.setState({
        modalShow7: false,
        modalShow8: false,
        modalShow9: false,
        modalShow10: false,
        modalShow11: false,
        modalShow12: false,
        modalShow13: false,
        modalShow14: false,
        modalShow15: false,
        modalShow16: false,
        modalShow17: false
      });
    return (
      <Modal
        {...this.props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            List Your Services
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow7: true })}
                block
              >
                Residential for Sale
              </Button>
              <Residentialforsale
                show={this.state.modalShow7}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow8: true })}
                block
              >
                Residential for Rent
              </Button>
              <Residentialforrent
                show={this.state.modalShow8}
                onHide={modalClose}
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow9: true })}
                block
              >
                Commercial for Sale
              </Button>
              <Commercialforsale
                show={this.state.modalShow9}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow10: true })}
                block
              >
                Commercial for Rent
              </Button>
              <Commercialforrent
                show={this.state.modalShow10}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow11: true })}
                block
              >
                Industrial for Sale
              </Button>
              <Industrialforsale
                show={this.state.modalShow11}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow12: true })}
                block
              >
                Industrial for Rent
              </Button>
              <Industrialforrent
                show={this.state.modalShow12}
                onHide={modalClose}
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow13: true })}
                block
              >
                WareHousing for Sale
              </Button>
              <Warehousingforsale
                show={this.state.modalShow13}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow14: true })}
                block
              >
                WareHousing for Rent
              </Button>
              <Warehousingforrent
                show={this.state.modalShow14}
                onHide={modalClose}
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasic">
              {/* <Form.Label>I want to</Form.Label> */}
              <Button
                variant="white gray border"
                size="md"
                onClick={() => this.setState({ modalShow15: true })}
                block
              >
                Agricultural for Sale
              </Button>
              <Agriculturalforsale
                show={this.state.modalShow15}
                onHide={modalClose}
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicEmail">
              <Button
                variant="white gray border"
                size="md"
               // onClick={() => this.setState({ modalShow16: true })}
                block
              >
                Agricultural for Rent
              </Button>
              <Agriculturalforrent
                show={this.state.modalShow16}
                onHide={modalClose}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class ListyourProperty extends React.Component { 
  
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }
  
  handleOpenModal () {
    this.setState({ showModal: true });
  }
  
  handleCloseModal () {
    this.setState({ modalShow5: false });
  }

  getFiles(files){
    this.setState({ files: files })
  }

  render() {     
      let modalClose = () =>
      this.setState({
        modalShow7: false,
        modalShow8: false,
        modalShow9: false,
        modalShow10: false,
        modalShow11: false,
        modalShow12: false,
        modalShow13: false,
        modalShow14: false,
        modalShow15: false,
        modalShow16: false,
        modalShow17: false
      });

      let closeListYourPropertyModal = () =>
      this.setState({
        modalShow5: false
    });

    let residentialForSaleModal = () =>
      this.setState({
        modalShow7: true
    });

    let residentialForRentModal = () =>
      this.setState({
        modalShow8: true
    });

    let commercialForSaleModal = () =>
      this.setState({
        modalShow9: true
    });

    let commercialForRentModal = () =>
      this.setState({
        modalShow10: true
    });

    let industrialForSaleModal = () =>
      this.setState({
        modalShow11: true
    });

    let industrialForRentModal = () =>
      this.setState({
        modalShow12: true
    });

    let warehousingForSaleModal = () =>
      this.setState({
        modalShow13: true
    });

    let warehousingForRentModal = () =>
      this.setState({
        modalShow14: true
    });

    let agriculturalForSaleModal = () =>
      this.setState({
        modalShow15: true
    });

    let agriculturalForRentModal = () =>
      this.setState({
        modalShow16: true
    });

    
    function checkPropertyType(){
      var data = new FormData();
      var imagedata = document.querySelector('input[type="file"]').value;
      data.append("data", imagedata); 

      //alert("Property Type");
      if((document.querySelector('input[name=propertyType]:checked').value == "Residential") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
        closeListYourPropertyModal();
        residentialForSaleModal();        
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Residential") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
        alert(imagedata);
        residentialForRentModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Commercial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
        commercialForSaleModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Commercial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
        commercialForRentModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Industrial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
        industrialForSaleModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Industrial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
        industrialForRentModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Warehousing") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
        warehousingForSaleModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Warehousing") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
        warehousingForRentModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Agricultural") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
        agriculturalForSaleModal();
      }else if((document.querySelector('input[name=propertyType]:checked').value == "Agricultural") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
        agriculturalForRentModal();
      }else{
        alert("Please Select Property Type and Property Status")
      }
    }     
    return (     
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            List your Property
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="addProperty" name="uploadProperty">
            <Form.Group as={Col} md="12" controlId="formBasicName">
              <Form.Label>NAME OF THE PROPERTY *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Name of the property"
                id="propertyName"                
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
               <Form.Label>I WANT TO *</Form.Label>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Sale"
                    name="propertyStatus"
                    value="Sale"
                  />
                </Form.Group>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Rent"
                    name="propertyStatus"
                    value="Rent"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Find Joint Venture Partner-Investor"
                    name="propertyStatus"
                    value="Venture"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>TYPE OF PROPERTY *</Form.Label>
              <Form.Row>
                <Form.Group as={Col} md="6">
                <Form.Check
                  type="radio"
                  label="Residential"
                  name="propertyType"
                  value="Residential"
                />
              </Form.Group>
                <Form.Group as={Col} md="6">
                <Form.Check
                  type="radio"
                  label="Commercial"
                  name="propertyType"
                  value="Commercial"
                />
              </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6">
                <Form.Check
                  type="radio"
                  label="Warehousing"
                  name="propertyType"
                  value="Warehousing"
                />
              </Form.Group>
                <Form.Group as={Col} md="6">
                <Form.Check
                  type="radio"
                  label="Industrial"
                  name="propertyType"
                  value="Industrial"
                />
              </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} md="6">
                <Form.Check
                  type="radio"
                  label="Agricultural"
                  name="propertyType"
                  value="Agricultural"
                />
              </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>LOCATION ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Location"
                id="propertyLocation"
                
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>LANDMARK *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Landmark"
                id="propertyLandmark"
                
              />
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Landmark description"
                id="propertyDescription"
                
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD IMAGE *</Form.Label>
              <InputGroup>
                <UploadImage />
                {/* <FileBase64
                    multiple={ true }
                    onDone={ this.getFiles.bind(this) } /> */}
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>VIDEO URL *</Form.Label>
              <InputGroup>
                <Form.Control
                  placeholder="https://www.youtube.com/watch?v=sHXBWHON"
                  type="text"                  
                />
              </InputGroup>
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button      
               className="button1 btn-danger btnFullwidth"          
                type="submit"
                onClick={e => {e.preventDefault(); {checkPropertyType()}}}      
                size="lg"                          
                block
              >
                Next
              </Button>

              <Residentialforsale
                show={this.state.modalShow7}
                onHide={modalClose}
              />

              <Residentialforrent
                              show={this.state.modalShow8}
                              onHide={modalClose}
              />

              <Commercialforsale
                              show={this.state.modalShow9}
                              onHide={modalClose}
              />

              <Commercialforrent
                              show={this.state.modalShow10}
                              onHide={modalClose}
              />

              <Industrialforsale
                              show={this.state.modalShow11}
                              onHide={modalClose}
              />

              <Industrialforrent
                              show={this.state.modalShow12}
                              onHide={modalClose}
              />

              <Warehousingforsale
                              show={this.state.modalShow13}
                              onHide={modalClose}
              />

              <Warehousingforrent
                              show={this.state.modalShow14}
                              onHide={modalClose}
              />

              <Agriculturalforsale
                              show={this.state.modalShow15}
                              onHide={modalClose}
              />

              <Agriculturalforrent
                              show={this.state.modalShow16}
                              onHide={modalClose}
              />
              <ListyourProperty show={this.state.modalShow5} onHide={modalClose} />

            </Form.Group>
          </Form>
        </Modal.Body>        
      </Modal>
      
    );
    // }else{      
    //   return null;
    // }
  }
}

export class PostyourRequirement extends React.Component {
  render() {

    function postYourRequirement(){        
      fetch('https://aceimagingandvideo.com/beegru/website/postYourRequirement', {
        method: 'POST',
        async: false,
        headers: {                               
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
        body: JSON.stringify({
                "name": document.postYourRequirementForm.postYourRequirement.value,
                "comments": document.postYourRequirementForm.postYourRequirement.value,
                "wantTo": document.postYourRequirementForm.postYourRequirementWantTo.value,
                "location": document.postYourRequirementForm.postYourRequirementLocation.value,                                
        })
}).then((response) => response.json())
        .then((responseJson) => {                  
                console.log(responseJson);
                if(responseJson.resultStatus == 2){
                  alert("");                    
                }else if(responseJson.resultStatus == 1){                                                               
                    alert('');
                    //document.getElementById("postYourRequirementFormType").reset();
                    //window.location.reload();
                }else{
                  alert("Error");
                }
                return responseJson.movies;
        })
        .catch((error) => {
                console.error(error);
        });
    }

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Post your requirement
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className="postform_text">
            If you have any specific requirements with respect of property
            buying, selling, renting or join ventures trasactions, send us a
            message or get n touch with us
          </p>
          <p className="postform_p">
            Email: mail@beegru.com
            <br />
            Phone: +91 88610 39999
          </p>
          {/* <Link to="/home">
            <p className="postform_link">or upload details</p>
          </Link> */}
          <Form name="postYourRequirementForm" id="postYourRequirementFormType">
            <Form.Group as={Col} md="12" controlId="formBasicName">
              <Form.Label>I WANT TO</Form.Label>
              <ButtonToolbar>
                <Button variant="outline-secondary" className="mlr-3">SELL</Button>
                <Button variant="outline-secondary" className="mlr-3">BUY</Button>
                <Button variant="outline-secondary" className="mlr-3">RENT</Button>
                <Button variant="outline-secondary" className="mlr-3">RENT-OUT</Button>
                <Button variant="outline-secondary" className="mlr-3">JOINT-VENTURES</Button>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>PROPERTY LOCATION</Form.Label>
              <InputGroup className="mb-3 m1">
                <InputGroup.Prepend>
                  <Button variant="outline">
                    <i className="fa fa-search"></i>
                  </Button>
                </InputGroup.Prepend>
                <FormControl aria-describedby="basic-addon1" />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD IMAGE *</Form.Label>
              <InputGroup>
              <UploadImage />
                {/* <Form.Control placeholder="UPLOAD" type="file" multiple /> */}
              </InputGroup>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>COMMENTS</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="COMMENTS"
                required
              ></Form.Control>
            </Form.Group>
           
            <Form.Group>
              <Form.Check
                required
                className="bordernone"
                label="I have read and agree to the Privacy Policy and 
                Terms of use
                *"
                feedback="You must agree before submitting."
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {postYourRequirement()}}}
                type="submit"
                size="lg"
                block
              >
                Post requirement
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Residentialforrent extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function residentialForRentSubmit(){  
     // alert(document.querySelector('input[name=residentialForRentStatus]:checked').value);      
     var items=document.getElementsByName('residentialForRentAmenities');
     var residentialForRentAmenitiesList="";
     for(var i=0; i<items.length; i++){
       if(items[i].type=='checkbox' && items[i].checked==true)
       residentialForRentAmenitiesList+=items[i].value+",";
     }
     console.log("Selected Amenities : "+residentialForRentAmenitiesList);     
     fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
        method: 'POST',
        async: false,
        headers: {                               
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "propertyName":document.uploadProperty.propertyName.value, 
          "propertyLocation":document.uploadProperty.propertyLocation.value,
          "propertyLandmark":document.uploadProperty.propertyLandmark.value,
          "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
          "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
          "shortDescription":document.uploadProperty.propertyDescription.value,
          "status":document.querySelector('input[name=residentialForRentStatus]:checked').value,
          "furnishing":document.querySelector('input[name=residentialForRentFurnishing]:checked').value, 
          "bathrooms":document.residentialForRent.residentialForRentBathrooms.value,
          "carpetArea":document.residentialForRent.residentialForRentCarpetArea.value,
          "superBuiltupArea":document.residentialForRent.residentialForRentSuperBuiltupArea.value,
          "length":document.residentialForRent.residentialForRentLength.value,
          "breadth":document.residentialForRent.residentialForRentBreadth.value,
          "roadFacingWidth":document.residentialForRent.residentialForRentRoadFacingWidth.value,
          "waterSupply":document.querySelector('input[name=residentialForRentWaterSupply]:checked').value, 
          "groundWaterLevel":document.residentialForRent.residentialForRentGroundWaterLevel.value,
          "amenities":residentialForRentAmenitiesList, 
          "aboutLocation":document.residentialForRent.residentialForRentAboutLocation.value,
          "aboutProperty":document.residentialForRent.residentialForRentAboutProperty.value, 
          "other":document.residentialForRent.residentialForRentOther.value,
          "rooms":document.residentialForRent.residentialForRentRooms.value, 
          "floorNumber":document.residentialForRent.residentialForRentFloorNumber.value, 
          "balconies":document.residentialForRent.residentialForRentBalconies.value,
          "facing":document.querySelector('input[name=residentialForRentFacing]:checked').value,
          "otherTag":document.querySelector('input[name=residentialForRentOtherTags]:checked').value,
          "persons":document.residentialForRent.residentialForRentPersons.value,
          "tenentType":document.querySelector('input[name=residentialForRentTenentType]:checked').value,
          "petFriendly":document.querySelector('input[name=residentialForRentPetFriendly]:checked').value,
          "agreementPeriod":document.querySelector('input[name=residentialForRentAgreementPeriod]:checked').value, 
          "preference":document.querySelector('input[name=residentialForRentPreference]:checked').value,
          "tenentHabitPreference":document.querySelector('input[name=residentialForRentTenentHabitPreference]:checked').value,
          "minPrice":document.residentialForRent.residentialForRentMinPrice.value,
          "maxPrice":document.residentialForRent.residentialForRentMaxPrice.value,
          "coLiving":document.querySelector('input[name=residentialForRentCoLiving]:checked').value,
          "longDescription":document.residentialForRent.residentialForRentLongDescription.value,
          "propertyVicinity":document.residentialForRent.residentialForRentPropertyVicinity.value,
          "NoOfImages":document.residentialForRent.residentialForRentNoOfImages.value,                                             
        })
}).then((response) => response.json())
        .then((responseJson) => {                  
                console.log(responseJson);
                if(responseJson.resultStatus == 2){
                  alert("");                    
                }else if(responseJson.resultStatus == 1){                                                               
                    alert('');                    
                }else{
                  alert("Error");
                }
                return responseJson.movies;
        })
        .catch((error) => {
                console.error(error);
        });
    }

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Residential for rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form name="residentialForRent" id="residentialForRentForm">
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="residentialForRentLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" id="">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  value="New"
                  name="residentialForRentStatus"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  value="Resale"
                  name="residentialForRentStatus"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  value="UnderConstructions"
                  name="residentialForRentStatus"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  value="ReadyToMoveIn"
                  name="residentialForRentStatus"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  value="BuiltToSuit"
                  name="residentialForRentStatus"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  value="BareShell"
                  name="residentialForRentStatus"                  
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  value="Furnished"
                  name="residentialForRentFurnishing"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  value="SemiFurnished"
                  name="residentialForRentFurnishing"                  
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  value="UnFurnished"
                  name="residentialForRentFurnishing"                  
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentPersons"
                required
              />
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    value="PrivateCabin"
                    name="residentialForRentCoLiving"                    
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    value="SharedWorkSpace"
                    name="residentialForRentCoLiving"                    
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Length"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Breadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="residentialForRentFacing"
                    value="North"                    
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="residentialForRentFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="residentialForRentFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="residentialForRentFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="residentialForRentFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="residentialForRentFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="residentialForRentFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="residentialForRentFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check                                        
                    type="checkbox"                   
                    label="A/c"
                    name="residentialForRentAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check                    
                    type="checkbox"                    
                    label="GYM"
                    name="residentialForRentAmenities"
                    value="Gym"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check   
                    type="checkbox"                 
                    label="Laundry"
                    name="residentialForRentAmenities" 
                    value="Laundry"                 
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check 
                    type="checkbox"                   
                    label="CCTV"
                    name="residentialForRentAmenities"
                    value="CCTV"                    
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check 
                    type="checkbox"                   
                    label="WiFi"
                    name="residentialForRentAmenities"
                    value="WiFi"                    
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"                    
                    label="Security"
                    name="residentialForRentAmenities"
                    value="Security"                    
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check  
                    type="checkbox"                  
                    label="Pool"
                    name="residentialForRentAmenities"  
                    value="Pool"                  
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check    
                    type="checkbox"                
                    label="CCTV"
                    name="residentialForRentAmenities" 
                    value="CCTV"                   
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Amenities</Form.Label>
             
            
              {this.state.questions.map((question, index) => (
            <span key={index}>
              <Form.Row>
              <Form.Group as={Col} md={"9"}>
              <Form.Control
                className="controll"
                type="text"
                onChange={this.handleText(index)}
                value={question}
                
                required
              />
              </Form.Group>
              <Form.Group as={Col} md={"3"}>
              <Button className="button1 btn-danger" onClick={this.handleDelete(index)}>Delete</Button>
              </Form.Group>
              </Form.Row>
              </span>
            ))}
             <Form.Group as={Col}>
                <Button className="button1 btn-danger" onClick={this.addQuestion}>+ Add</Button>
             </Form.Group>

            </Form.Group>
           
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NRI"
                    name="residentialForRentOtherTags"
                    value="NRI"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Mansion"
                    name="residentialForRentOtherTags"
                    value="Mansion"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenant Type</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Company"
                    name="residentialForRentTenantType"
                    value="Company"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Family"
                    name="residentialForRentTenantType"
                    value="Family"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Single"
                    name="residentialForRentTenantType"
                    value="Single"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="residentialForRentTenantType"
                    value="NoPreference"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pet Friendly</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Yes"
                    name="residentialForRentPetFriendly"
                    value="Yes"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No"
                    name="residentialForRentPetFriendly"
                    value="No"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="residentialForRentAgreementPeriod"
                    value="ThreeMonths"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="residentialForRentAgreementPeriod"
                    value="SixMonths"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="residentialForRentAgreementPeriod"
                    value="TwelveMonths"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="residentialForRentAgreementPeriod"
                    value="TwoYears"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="residentialForRentPreference"
                    value="Veg"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="residentialForRentPreference"
                    value="NonVeg"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="residentialForRentPreference"
                    value="NoPreference"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="residentialForRentPreference"
                    value=""
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="residentialForRentTenentHabitsPreference"
                    value="Smoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="residentialForRentTenentHabitsPreference"
                    value="NoDrinking"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="residentialForRentTenentHabitsPreference"
                    value="NoSmoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="residentialForRentTenentHabitsPreference"
                    value="NoPreference"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="residentialForRentMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="residentialForRentMaxPrice"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentMaintainenssCharges"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForRentSecurityDeposit"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="residentialForRentAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="residentialForRentAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="residentialForRentOthers"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button                
                onClick={e => {e.preventDefault(); {residentialForRentSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Residentialforsale extends React.Component {
  render() {
    function residentialForSaleSubmit(){              
      var items=document.getElementsByName('residentialForSaleAmenities');
      var residentialForSaleAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        residentialForSaleAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+residentialForSaleAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=residentialForSaleStatus]:checked').value,
           "furnishing":document.querySelector('input[name=residentialForSaleFurnishing]:checked').value, 
           "bathrooms":document.residentialForSale.residentialForSaleBathrooms.value,
           "carpetArea":document.residentialForSale.residentialForSaleCarpetArea.value,
           "superBuiltupArea":document.residentialForSale.residentialForSaleSuperBuiltupArea.value,
           "length":document.residentialForSale.residentialForSaleLength.value,
           "breadth":document.residentialForSale.residentialForSaleBreadth.value,
           "roadFacingWidth":document.residentialForSale.residentialForSaleRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=residentialForSaleWaterSupply]:checked').value, 
           "groundWaterLevel":document.residentialForSale.residentialForSaleGroundWaterLevel.value,
           "amenities":residentialForSaleAmenitiesList, 
           "aboutLocation":document.residentialForSale.residentialForSaleAboutLocation.value,
           "aboutProperty":document.residentialForSale.residentialForSaleAboutProperty.value, 
           "other":document.residentialForSale.residentialForSaleOther.value,
           "rooms":document.residentialForSale.residentialForSaleRooms.value, 
           "floorNumber":document.residentialForSale.residentialForSaleFloorNumber.value, 
           "balconies":document.residentialForSale.residentialForSaleBalconies.value,
           "facing":document.querySelector('input[name=residentialForSaleFacing]:checked').value,
           "otherTag":document.querySelector('input[name=residentialForSaleOtherTags]:checked').value,
           "persons":document.residentialForSale.residentialForSalePersons.value,           
           "minPrice":document.residentialForSale.residentialForSaleMinPrice.value,
           "maxPrice":document.residentialForSale.residentialForSaleMaxPrice.value,           
           "longDescription":document.residentialForSale.residentialForSaleLongDescription.value,
           "propertyVicinity":document.residentialForSale.residentialForSalePropertyVicinity.value,
           "coLiving":document.querySelector('input[name=residentialForSaleCoLiving]:checked').value,
           "NoOfImages":document.residentialForSale.residentialForSaleNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Residential for sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form name="residentialForSale" id="residentialForSaleForm">
            <Form.Group as={Col} md="12">
              <Form.Label>Type</Form.Label>

              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Apartment"
                    name="residentialForSaleType"  
                    value="Apartment"                  
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Land"
                    name="residentialForSaleType"
                    value="Land"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Independent House"
                    name="residentialForSaleType"
                    value="IndependentHouse"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Plots"
                    name="residentialForSaleType"
                    value="Plots"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Farm House"
                    name="residentialForSaleType"
                    value="FarmHouse"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Co-Living"
                    name="residentialForSaleType"
                    value="CoLiving"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="other"
                    name="residentialForSaleType"
                    value="other"
                  />
                </Form.Group>
                <Form.Group as={Col}></Form.Group>
              </Form.Row>
              <Form.Control
                className="controll"
                type="text"
                placeholder="other"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="residentialForSaleLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="New"
                    name="residentialForSaleStatus"
                    value="New"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    name="residentialForSaleStatus"
                    value="Resale"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    name="residentialForSaleStatus"
                    value="UnderConstructions"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    name="residentialForSaleStatus"
                    value="ReadyToMoveIn"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    name="residentialForSaleStatus"
                    value="BuilttoSuit"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    name="residentialForSaleStatus"
                    value="BareShell"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="residentialForSaleFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="residentialForSaleFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="residentialForSaleFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSalePersons"
                required
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="residentialForSaleCoLiving"
                    value="PrivateCabin"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="residentialForSaleCoLiving"
                    value="SharedWorkSpace"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Control
                    type="text"
                    label="Length"
                    className="controll"
                    placeholder="Length"
                    id="residentialForSaleLength"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Control
                    type="text"
                    label="Breadth"
                    className="controll"
                    placeholder="Breadth"
                    id="residentialForSaleBreadth"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="residentialForSaleFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="residentialForSaleFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="residentialForSaleFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="residentialForSaleFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="residentialForSaleFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="residentialForSaleFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="residentialForSaleFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="residentialForSaleFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="residentialForSaleAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="residentialForSaleAmenities"
                    value="GYM"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="residentialForSaleAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="residentialForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="residentialForSaleAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="residentialForSaleAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="residentialForSaleAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="residentialForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="residentialForSaleOtherAmenities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Vicinity</Form.Label>
              <Form.Label>Type</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="type"
                required
              />
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="residentialForSalePropertyVicinity"
                required
              ></Form.Control>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NRI"
                    name="residentialForSaleOtherTags"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Mansion"
                    name="residentialForSaleOtherTags"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>                                             
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="residentialForSaleAboutLocation"
                required
              />             
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="residentialForSaleAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="residentialForSaleOther"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="residentialForSaleMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="residentialForSaleMaxPrice"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {residentialForSaleSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Commercialforsale extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function commercialForSaleSubmit(){              
      var items=document.getElementsByName('commercialForSaleAmenities');
      var commercialForSaleAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        commercialForSaleAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+commercialForSaleAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=commercialForSaleStatus]:checked').value,
           "furnishing":document.querySelector('input[name=commercialForSaleFurnishing]:checked').value, 
           "bathrooms":document.commercialForSale.commercialForSaleBathrooms.value,
           "carpetArea":document.commercialForSale.commercialForSaleCarpetArea.value,
           "superBuiltupArea":document.commercialForSale.commercialForSaleSuperBuiltupArea.value,
           "length":document.commercialForSale.commercialForSaleLength.value,
           "breadth":document.commercialForSale.commercialForSaleBreadth.value,
           "roadFacingWidth":document.commercialForSale.commercialForSaleRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=commercialForSaleWaterSupply]:checked').value, 
           "groundWaterLevel":document.commercialForSale.commercialForSaleGroundWaterLevel.value,
           "amenities":commercialForSaleAmenitiesList, 
           "aboutLocation":document.commercialForSale.commercialForSaleAboutLocation.value,
           "aboutProperty":document.commercialForSale.commercialForSaleAboutProperty.value, 
           "other":document.commercialForSale.commercialForSaleOther.value,
           "rooms":document.commercialForSale.commercialForSaleRooms.value, 
           "floorNumber":document.commercialForSale.commercialForSaleFloorNumber.value, 
           "balconies":document.commercialForSale.commercialForSaleBalconies.value,
           "facing":document.querySelector('input[name=commercialForSaleFacing]:checked').value,
           "otherTag":document.querySelector('input[name=commercialForSaleOtherTags]:checked').value,
           "persons":document.commercialForSale.commercialForSalePersons.value,           
           "minPrice":document.commercialForSale.commercialForSaleMinPrice.value,
           "maxPrice":document.commercialForSale.commercialForSaleMaxPrice.value,           
           "longDescription":document.commercialForSale.commercialForSaleLongDescription.value,
           "propertyVicinity":document.commercialForSale.commercialForSalePropertyVicinity.value,
           "coLiving":document.querySelector('input[name=commercialForSaleCoLiving]:checked').value,
           "NoOfImages":document.commercialForSale.commercialForSaleNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Commercial for Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="commercialForSaleLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="commercialForSaleStatus"
                  value="New"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="commercialForSaleStatus"
                  value="Resale"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="commercialForSaleStatus"
                  value="UnderConstructions"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="commercialForSaleStatus"
                  value="ReadyToMoveIn"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="commercialForSaleStatus"
                  value="BuiltToSuit"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="commercialForSaleStatus"
                  value="BareShell"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="commercialForSaleFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="commercialForSaleFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="commercialForSaleFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSalePersons"
                required
              />
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="commercialForSaleCoLiving"
                    id="PrivateCabin"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="commercialForSaleCoLiving"
                    id="SharedWorkSpace"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Length"
                  id="commercialForSaleLength"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Breadth"
                  id="commercialForSaleBreadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="commercialForSaleFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="commercialForSaleFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="commercialForSaleFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="commercialForSaleFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="commercialForSaleFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="commercialForSaleFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="commercialForSaleFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="commercialForSaleFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="commercialForSaleAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="commercialForSaleAmenities"
                    value="Gym"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="commercialForSaleAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="commercialForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="commercialForSaleAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="commercialForSaleAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="commercialForSaleAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="commercialForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForSaleOtherAminities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NRI"
                    name="commercialForSaleOtherTags"
                    value="NRI"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Mansion"
                    name="commercialForSaleOtherTags"
                    value="Mansion"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="commercialForSaleAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="commercialForSaleAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="commercialForSaleOther"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="commercialForSaleMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="commercialForSaleMaxPrice"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button 
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {commercialForSaleSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Commercialforrent extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function commercialForRentSubmit(){              
      var items=document.getElementsByName('commercialForRentAmenities');
      var commercialForRentAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        commercialForRentAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+commercialForRentAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=commercialForRentStatus]:checked').value,
           "furnishing":document.querySelector('input[name=commercialForRentFurnishing]:checked').value, 
           "bathrooms":document.commercialForRent.commercialForRentBathrooms.value,
           "carpetArea":document.commercialForRent.commercialForRentCarpetArea.value,
           "superBuiltupArea":document.commercialForRent.commercialForRentSuperBuiltupArea.value,
           "length":document.commercialForRent.commercialForRentLength.value,
           "breadth":document.commercialForRent.commercialForRentBreadth.value,
           "roadFacingWidth":document.commercialForRent.commercialForRentRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=commercialForRentWaterSupply]:checked').value, 
           "groundWaterLevel":document.commercialForRent.commercialForRentGroundWaterLevel.value,
           "amenities":commercialForRentAmenitiesList, 
           "aboutLocation":document.commercialForRent.commercialForRentAboutLocation.value,
           "aboutProperty":document.commercialForRent.commercialForRentAboutProperty.value, 
           "other":document.commercialForRent.commercialForRentOther.value,
           "rooms":document.commercialForRent.commercialForRentRooms.value, 
           "floorNumber":document.commercialForRent.commercialForRentFloorNumber.value, 
           "balconies":document.commercialForRent.commercialForRentBalconies.value,
           "facing":document.querySelector('input[name=commercialForRentFacing]:checked').value,
           "otherTag":document.commercialForRent.commercialForRentOtherTags.value,
           "persons":document.commercialForRent.commercialForRentPersons.value,           
           "minPrice":document.commercialForRent.commercialForRentMinPrice.value,
           "maxPrice":document.commercialForRent.commercialForRentMaxPrice.value,           
           "longDescription":document.commercialForRent.commercialForRentLongDescription.value,
           "propertyVicinity":document.commercialForRent.commercialForRentPropertyVicinity.value,
           "coLiving":document.querySelector('input[name=commercialForRentCoLiving]:checked').value,
           "agreementPeriod":document.querySelector('input[name=commercialForRentAgreementPeriod]:checked').value, 
           "preference":document.querySelector('input[name=commercialForRentPereference]:checked').value, 
           "tenentHabitPreference":document.querySelector('input[name=commercialForRentTenentHabitsPreference]:checked').value,
           "maintainanceCharge":document.commercialForRent.commercialForRentMaintainenssCharges.value, 
           "securityDeposit":document.commercialForRent.commercialForRentSecurityDeposit.value, 
          //  "NoOfImages":document.commercialForRent.commercialForRentNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Commercial for Rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="commercialForRentLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="commercialForRentStatus"
                  value="New"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="commercialForRentStatus"
                  value="Resale"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="commercialForRentStatus"
                  value="UnderConstructions"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="commercialForRentStatus"
                  value="ReadyToMoveIn"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="commercialForRentStatus"
                  value="BuiltToSuit"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="commercialForRentStatus"
                  value="BareShell"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="commercialForRentFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="commercialForRentFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="commercialForRentFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>if Co-Living Person</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentPersons"
                required
              />
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Private Cabin"
                    name="commercialForRentCoLiving"
                    value="PrivateCabin"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Shared Work Space"
                    name="commercialForRentCoLiving"
                    value="SharedWorkSpace"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Length"
                  id="commercialForRentLength"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="Breadth"
                  id="commercialForRentBreadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="commercialForRentFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="commercialForRentFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="commercialForRentFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="commercialForRentFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="commercialForRentFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="commercialForRentFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="commercialForRentFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="commercialForRentFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="commercialForRentAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="commercialForRentAmenities"
                    value="GYM"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="commercialForRentAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="commercialForRentAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="commercialForRentAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="commercialForRentAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="commercialForRentAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="commercialForRentAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentOtherAmenities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentOtherTags"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="commercialForRentAgreementPeriod"
                    value="3 months"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="commercialForRentAgreementPeriod"
                    value="6 months"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="commercialForRentAgreementPeriod"
                    value="12 months"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="commercialForRentAgreementPeriod"
                    value="2 years"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="commercialForRentPereference"
                    value="Veg"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="commercialForRentPereference"
                    value="NonVeg"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="commercialForRentPereference"
                    value="NoPreference"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="commercialForRentTenentHabitsPreference"
                    value="Smoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="commercialForRentTenentHabitsPreference"
                    value="NoDrinking"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="commercialForRentTenentHabitsPreference"
                    value="NoSmoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="commercialForRentTenentHabitsPreference"
                    value="NoPreference"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="commercialForRentMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="commercialForRentMaxPrice"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentMaintainenssCharges"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="commercialForRentSecurityDeposit"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="commercialForRentAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="commercialForRentAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="commercialForRentOther"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {commercialForRentSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Industrialforsale extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function industrialForSaleSubmit(){              
      var items=document.getElementsByName('industrialForSaleAmenities');
      var industrialForSaleAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        industrialForSaleAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+industrialForSaleAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=industrialForSaleStatus]:checked').value,
           "furnishing":document.querySelector('input[name=industrialForSaleFurnishing]:checked').value, 
           "bathrooms":document.industrialForSale.industrialForSaleBathrooms.value,
           "carpetArea":document.industrialForSale.industrialForSaleCarpetArea.value,
           "superBuiltupArea":document.industrialForSale.industrialForSaleSuperBuiltupArea.value,
           "length":document.industrialForSale.industrialForSaleLength.value,
           "breadth":document.industrialForSale.industrialForSaleBreadth.value,
           "roadFacingWidth":document.industrialForSale.industrialForSaleRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=industrialForSaleWaterSupply]:checked').value, 
           "groundWaterLevel":document.industrialForSale.industrialForSaleGroundWaterLevel.value,
           "amenities":industrialForSaleAmenitiesList, 
           "aboutLocation":document.industrialForSale.industrialForSaleAboutLocation.value,
           "aboutProperty":document.industrialForSale.industrialForSaleAboutProperty.value, 
           "other":document.industrialForSale.industrialForSaleOther.value,
           "rooms":document.industrialForSale.industrialForSaleRooms.value, 
           "floorNumber":document.industrialForSale.industrialForSaleFloorNumber.value, 
           "balconies":document.industrialForSale.industrialForSaleBalconies.value,
           "facing":document.querySelector('input[name=industrialForSaleFacing]:checked').value,
           "otherTag":document.industrialForSale.industrialForSaleOtherTags.value,
           "persons":document.industrialForSale.industrialForSalePersons.value,           
           "minPrice":document.industrialForSale.industrialForSaleMinPrice.value,
           "maxPrice":document.industrialForSale.industrialForSaleMaxPrice.value,           
           "longDescription":document.industrialForSale.industrialForSaleLongDescription.value,
           "propertyVicinity":document.industrialForSale.industrialForSalePropertyVicinity.value,
           "coLiving":document.querySelector('input[name=industrialForSaleCoLiving]:checked').value,
           "maintainanceCharge":document.industrialForSale.industrialForSaleMaintainenssCharges.value, 
            "securityDeposit":document.industrialForSale.industrialForSaleSecurityDeposit.value,
           //"NoOfImages":document.industrialForSale.industrialForSaleNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Industrial for Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="industrialForSaleLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="industrialForSaleStatus"
                  value="New"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="industrialForSaleStatus"
                  value="Resale"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="industrialForSaleStatus"
                  value="UnderConstructions"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="industrialForSaleStatus"
                  value="ReadyToMoveIn"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="industrialForSaleStatus"
                  value="BuiltToSuit"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="industrialForSaleStatus"
                  value="BareShell"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="industrialForSaleFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="industrialForSaleFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="industrialForSaleFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="industrialForSaleLength"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="industrialForSaleBreadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="industrialForSaleFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="industrialForSaleFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="industrialForSaleFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="industrialForSaleFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="industrialForSaleFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="industrialForSaleFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="industrialForSaleFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="industrialForSaleFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="industrialForSaleAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="industrialForSaleAmenities"
                    value="GYM"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="industrialForSaleAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="industrialForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="industrialForSaleAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="industrialForSaleAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="industrialForSaleAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="industrialForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleOtherAmenities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleOtherTags"
                required
              />
            </Form.Group>            
            
            
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="rindustrialForSaleMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="industrialForSaleMaxPrice"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleMaintainenssCharges"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForSaleSecurityDeposit"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="industrialForSaleAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="industrialForSaleAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="industrialForSaleOther"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {industrialForSaleSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Industrialforrent extends React.Component {

  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function industrialForRentSubmit(){              
      var items=document.getElementsByName('industrialForRentAmenities');
      var industrialForRentAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        industrialForRentAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+industrialForRentAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=industrialForRentStatus]:checked').value,
           "furnishing":document.querySelector('input[name=industrialForRentFurnishing]:checked').value, 
           "bathrooms":document.industrialForRent.industrialForRentBathrooms.value,
           "carpetArea":document.industrialForRent.industrialForRentCarpetArea.value,
           "superBuiltupArea":document.industrialForRent.industrialForRentSuperBuiltupArea.value,
           "length":document.industrialForRent.industrialForRentLength.value,
           "breadth":document.industrialForRent.industrialForRentBreadth.value,
           "roadFacingWidth":document.industrialForRent.industrialForRentRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=industrialForRentWaterSupply]:checked').value, 
           "groundWaterLevel":document.industrialForRent.industrialForRentGroundWaterLevel.value,
           "amenities":industrialForRentAmenitiesList, 
           "aboutLocation":document.industrialForRent.industrialForRentAboutLocation.value,
           "aboutProperty":document.industrialForRent.industrialForRentAboutProperty.value, 
           "other":document.industrialForRent.industrialForRentOther.value,
           "rooms":document.industrialForRent.industrialForRentRooms.value, 
           "floorNumber":document.industrialForRent.industrialForRentFloorNumber.value, 
           "balconies":document.industrialForRent.industrialForRentBalconies.value,
           "facing":document.querySelector('input[name=industrialForRentFacing]:checked').value,
           "otherTag":document.querySelector('input[name=industrialForRentOtherTags]:checked').value,
           "persons":document.industrialForRent.industrialForRentPersons.value,           
           "minPrice":document.industrialForRent.industrialForRentMinPrice.value,
           "maxPrice":document.industrialForRent.industrialForRentMaxPrice.value,           
           "longDescription":document.industrialForRent.industrialForRentLongDescription.value,
           "propertyVicinity":document.industrialForRent.industrialForRentPropertyVicinity.value,
           "coLiving":document.querySelector('input[name=industrialForRentCoLiving]:checked').value,
           "agreementPeriod":document.querySelector('input[name=industrialForRentAgreementPeriod]:checked').value, 
            "preference":document.querySelector('input[name=industrialForRentPereference]:checked').value, 
            "tenentHabitPreference":document.querySelector('input[name=industrialForRentTenentHabitsPreference]:checked').value,
            "maintainanceCharge":document.industrialForRent.industrialForRentMaintainenssCharges.value, 
            "securityDeposit":document.industrialForRent.industrialForRentSecurityDeposit.value, 
           //"NoOfImages":document.industrialForRent.industrialForRentNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Industrial for Rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="industrialForRentLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="industrialForRentStatus"
                  value="New"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="industrialForRentStatus"
                  value="Resale"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="industrialForRentStatus"
                  value="UnderConstructions"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="industrialForRentStatus"
                  value="ReadyToMoveIn"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="industrialForRentStatus"
                  value="BuiltToSuit"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="industrialForRentStatus"
                  value="BareShell"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="industrialForRentFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="industrialForRentFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="industrialForRentFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="industrialForRentLength"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="industrialForRentBreadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="industrialForRentFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="industrialForRentFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="industrialForRentFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="industrialForRentFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="industrialForRentFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="industrialForRentFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="industrialForRentFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="industrialForRentFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="industrialForRentAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="industrialForRentAmenities"
                    value="GYM"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="industrialForRentAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="industrialForRentAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="industrialForRentAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="industrialForRentAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="industrialForRentAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="industrialForRentAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentOtherAmenities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentOtherTags"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="industrialForRentAgreementPeriod"
                    value="3 months"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="industrialForRentAgreementPeriod"
                    value="6 months"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="industrialForRentAgreementPeriod"
                    value="12 months"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="industrialForRentAgreementPeriod"
                    value="2 years"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="industrialForRentPereference"
                    value="Veg"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="industrialForRentPereference"
                    value="NonVeg"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="industrialForRentPereference"
                    value="NoPreference"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="industrialForRentPereference"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="industrialForRentTenentHabitsPreference"
                    value="Smoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="industrialForRentTenentHabitsPreference"
                    value="NoDrinking"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="industrialForRentTenentHabitsPreference"
                    value="NoSmoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="industrialForRentTenentHabitsPreference"
                    value="NoPreference"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="industrialForRentMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="industrialForRentMaxPrice"
                required
              />
			      </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentMaintainenssCharges"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="industrialForRentSecurityDeposit"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="industrialForRentAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="industrialForRentAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="industrialForRentOther"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {industrialForRentSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Warehousingforsale extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function warehouseForSaleSubmit(){              
      var items=document.getElementsByName('warehouseForSaleAmenities');
      var warehouseForSaleAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        warehouseForSaleAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+warehouseForSaleAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=warehouseForSaleStatus]:checked').value,
           "furnishing":document.querySelector('input[name=warehouseForSaleFurnishing]:checked').value, 
           "bathrooms":document.warehouseForSale.warehouseForSaleBathrooms.value,
           "carpetArea":document.warehouseForSale.warehouseForSaleCarpetArea.value,
           "superBuiltupArea":document.warehouseForSale.warehouseForSaleSuperBuiltupArea.value,
           "length":document.warehouseForSale.warehouseForSaleLength.value,
           "breadth":document.warehouseForSale.warehouseForSaleBreadth.value,
           "roadFacingWidth":document.warehouseForSale.warehouseForSaleRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=warehouseForSaleWaterSupply]:checked').value, 
           "groundWaterLevel":document.warehouseForSale.warehouseForSaleGroundWaterLevel.value,
           "amenities":warehouseForSaleAmenitiesList, 
           "aboutLocation":document.warehouseForSale.warehouseForSaleAboutLocation.value,
           "aboutProperty":document.warehouseForSale.warehouseForSaleAboutProperty.value, 
           "other":document.warehouseForSale.warehouseForSaleOther.value,
           "rooms":document.warehouseForSale.warehouseForSaleRooms.value, 
           "floorNumber":document.warehouseForSale.warehouseForSaleFloorNumber.value, 
           "balconies":document.warehouseForSale.warehouseForSaleBalconies.value,
           "facing":document.querySelector('input[name=warehouseForSaleFacing]:checked').value,
           "otherTag":document.warehouseForSale.warehouseForSaleOtherTags.value,
           "persons":document.warehouseForSale.warehouseForSalePersons.value,           
           "minPrice":document.warehouseForSale.warehouseForSaleMinPrice.value,
           "maxPrice":document.warehouseForSale.warehouseForSaleMaxPrice.value,           
           "longDescription":document.warehouseForSale.warehouseForSaleLongDescription.value,
          // "propertyVicinity":document.warehouseForSale.warehouseForSalePropertyVicinity.value,
           "coLiving":document.querySelector('input[name=warehouseForSaleCoLiving]:checked').value,
           "maintainanceCharge":document.warehouseForSale.warehouseForSaleMaintainenssCharges.value, 
           "securityDeposit":document.warehouseForSale.warehouseForSaleSecurityDeposit.value, 
           //"NoOfImages":document.warehouseForSale.warehouseForSaleNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Warehousing for Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="warehouseForSaleLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="warehouseForSaleStatus"
                  value="New"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="warehouseForSaleStatus"
                  value="Resale"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="warehouseForSaleStatus"
                  value="UnderConstructions"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="warehouseForSaleStatus"
                  value="ReadyToMoveIn"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="warehouseForSaleStatus"
                  value="BuiltToSuit"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="warehouseForSaleStatus"
                  value="BareShell"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="warehouseForSaleFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="warehouseForSaleFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="warehouseForSaleFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleLength"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleBreadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="warehouseForSaleFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="warehouseForSaleFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="warehouseForSaleFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="warehouseForSaleFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="warehouseForSaleFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="warehouseForSaleFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="warehouseForSaleFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="warehouseForSaleFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="warehouseForSaleAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="warehouseForSaleAmenities"
                    value="GYM"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="warehouseForSaleAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="warehouseForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="warehouseForSaleAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="warehouseForSaleAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="warehouseForSaleAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="warehouseForSaleAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleOtherAmenities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleOtherTags"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="warehouseForSaleMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="warehouseForSaleMaxPrice"
                required
              />
			      </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleMaintainenssCharges"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForSaleSecurityDeposit"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="warehouseForSaleAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="warehouseForSaleAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="warehouseForSaleOther"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {warehouseForSaleSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Warehousingforrent extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    function warehouseForRentSubmit(){              
      var items=document.getElementsByName('warehouseForRentAmenities');
      var warehouseForRentAmenitiesList="";
      for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox' && items[i].checked==true)
        warehouseForRentAmenitiesList+=items[i].value+",";
      }
      console.log("Selected Amenities : "+warehouseForRentAmenitiesList);     
      fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
         method: 'POST',
         async: false,
         headers: {                               
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
         },
         body: JSON.stringify({
           "propertyName":document.uploadProperty.propertyName.value, 
           "propertyLocation":document.uploadProperty.propertyLocation.value,
           "propertyLandmark":document.uploadProperty.propertyLandmark.value,
           "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
           "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
           "shortDescription":document.uploadProperty.propertyDescription.value,
           "status":document.querySelector('input[name=warehouseForRentStatus]:checked').value,
           "furnishing":document.querySelector('input[name=warehouseForRentFurnishing]:checked').value, 
           "bathrooms":document.warehouseForRent.warehouseForRentBathrooms.value,
           "carpetArea":document.warehouseForRent.warehouseForRentCarpetArea.value,
           "superBuiltupArea":document.warehouseForRent.warehouseForRentSuperBuiltupArea.value,
           "length":document.warehouseForRent.warehouseForRentLength.value,
           "breadth":document.warehouseForRent.warehouseForRentBreadth.value,
           "roadFacingWidth":document.warehouseForRent.warehouseForRentRoadFacingWidth.value,
           "waterSupply":document.querySelector('input[name=warehouseForRentWaterSupply]:checked').value, 
           "groundWaterLevel":document.warehouseForRent.warehouseForRentGroundWaterLevel.value,
           "amenities":warehouseForRentAmenitiesList, 
           "aboutLocation":document.warehouseForRent.warehouseForRentAboutLocation.value,
           "aboutProperty":document.warehouseForRent.warehouseForRentAboutProperty.value, 
           "other":document.warehouseForRent.warehouseForRentOther.value,
           "rooms":document.warehouseForRent.warehouseForRentRooms.value, 
           "floorNumber":document.warehouseForRent.warehouseForRentFloorNumber.value, 
           "balconies":document.warehouseForRent.warehouseForRentBalconies.value,
           "facing":document.querySelector('input[name=warehouseForRentFacing]:checked').value,
           "otherTag":document.querySelector('input[name=warehouseForRentOtherTags]:checked').value,
           "persons":document.warehouseForRent.warehouseForRentPersons.value,           
           "minPrice":document.warehouseForRent.warehouseForRentMinPrice.value,
           "maxPrice":document.warehouseForRent.warehouseForRentMaxPrice.value,           
           "longDescription":document.warehouseForRent.warehouseForRentLongDescription.value,
           //"propertyVicinity":document.warehouseForRent.warehouseForRentPropertyVicinity.value,
           "coLiving":document.querySelector('input[name=warehouseForRentCoLiving]:checked').value,
           "agreementPeriod":document.querySelector('input[name=warehouseForRentAgreementPeriod]:checked').value, 
            "preference":document.querySelector('input[name=warehouseForRentPereference]:checked').value, 
            "tenentHabitPreference":document.querySelector('input[name=warehouseForRentTenentHabitsPreference]:checked').value,
            "maintainanceCharge":document.warehouseForRent.warehouseForRentMaintainenssCharges.value, 
            "securityDeposit":document.warehouseForRent.warehouseForRentSecurityDeposit.value,
           //"NoOfImages":document.warehouseForRent.warehouseForRentNoOfImages.value,                                             
         })
 }).then((response) => response.json())
         .then((responseJson) => {                  
                 console.log(responseJson);
                 if(responseJson.resultStatus == 2){
                   alert("");                    
                 }else if(responseJson.resultStatus == 1){                                                               
                     alert('');                    
                 }else{
                   alert("Error");
                 }
                 return responseJson.movies;
         })
         .catch((error) => {
                 console.error(error);
         });
     }
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Warehousing for Rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                id="warehouseForRentLongDescription"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Status</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="New"
                  name="warehouseForRentStatus"
                  value="New"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Resale"
                  name="warehouseForRentStatus"
                  value="Resale"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Under Constructions"
                  name="warehouseForRentStatus"
                  value="UnderConstructions"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Ready to move in"
                  name="warehouseForRentStatus"
                  value="ReadyToMoveIn"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Built to Suit"
                  name="warehouseForRentStatus"
                  value="BuiltToSuit"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Bare Shell"
                  name="warehouseForRentStatus"
                  value="BareShell"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Furnishing</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="furnished"
                  name="warehouseForRentFurnishing"
                  value="Furnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="semi-Furnished"
                  name="warehouseForRentFurnishing"
                  value="SemiFurnished"
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Un-Furnished"
                  name="warehouseForRentFurnishing"
                  value="UnFurnished"
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentRooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Floor Number</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentFloorNumber"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Balconies</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentBalconies"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Bath Rooms</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentBathrooms"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Carpet Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentCarpetArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Super built Up Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentSuperBuiltupArea"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Row>
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentLength"
                  required
                />
                <Form.Control
                  as={Col}
                  md="6"
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentBreadth"
                  required
                />
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="warehouseForRentFacing"
                    value="North"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="warehouseForRentFacing"
                    value="South"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="warehouseForRentFacing"
                    value="East"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="warehouseForRentFacing"
                    value="West"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="warehouseForRentFacing"
                    value="NorthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="warehouseForRentFacing"
                    value="NorthWest"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="warehouseForRentFacing"
                    value="SouthEast"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="warehouseForRentFacing"
                    value="SouthWest"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentRoadFacingWidth"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentGroundWaterLevel"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Amenities</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="A/c"
                    name="warehouseForRentAmenities"
                    value="AC"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="GYM"
                    name="warehouseForRentAmenities"
                    value="GYM"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Laundry"
                    name="warehouseForRentAmenities"
                    value="Laundry"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="warehouseForRentAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="WiFi"
                    name="warehouseForRentAmenities"
                    value="WiFi"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Security"
                    name="warehouseForRentAmenities"
                    value="Security"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="Pool"
                    name="warehouseForRentAmenities"
                    value="Pool"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="checkbox"
                    label="CCTV"
                    name="warehouseForRentAmenities"
                    value="CCTV"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other Aminities</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentOtherAmenities"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Other tags</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentOtherTags"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Agreement Period</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="3 months"
                    name="warehouseForRentAgreementPeriod"
                    value="3 months"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="6 months"
                    name="warehouseForRentAgreementPeriod"
                    value="6 months"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="12 months"
                    name="warehouseForRentAgreementPeriod"
                    value="12 months"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="2 years"
                    name="warehouseForRentAgreementPeriod"
                    value="2 years"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Pereference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Veg"
                    name="warehouseForRentPereference"
                    value="Veg"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Non Veg"
                    name="warehouseForRentPereference"
                    value="NonVeg"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No perference"
                    name="warehouseForRentPereference"
                    value="NoPreference"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label=""
                    name="warehouseForRentPereference"
                    value=""
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Tenent habits preference</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Smoking"
                    name="warehouseForRentTenentHabitsPreference"
                    value="Smoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Drinking"
                    name="warehouseForRentTenentHabitsPreference"
                    value="NoDrinking"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Smoking"
                    name="warehouseForRentTenentHabitsPreference"
                    value="NoSmoking"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="No Preference"
                    name="warehouseForRentTenentHabitsPreference"
                    value="NoPreference"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label><br/>
              <Form.Label>Minimum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Min Price"
                id="warehouseForRentMinPrice"
                required
              /><br/>
              <Form.Label>Maximum</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Max Price"
                id="warehouseForRentMaxPrice"
                required
              />
			      </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentMaintainenssCharges"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                id="warehouseForRentSecurityDeposit"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                id="warehouseForRentAboutLocation"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                id="warehouseForRentAboutProperty"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                id="warehouseForRentOther"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                onClick={e => {e.preventDefault(); {warehouseForRentSubmit()}}}
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Agriculturalforsale extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
   
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Agricultural For Sale
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Kharad Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Water supply</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Canal"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Muncipality"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Borewell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Soil type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Red"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Black"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>
                Structure <span>super built up area</span>
              </Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Wet"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Dry"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Highlands"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Irrigation type</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Drip"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Sprinkler"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Canal"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Pipe"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Plantation</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Banana"
                required
              />
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Plantation description"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Agriculturalforrent extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
  render() {
    
    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Agricultural for rent
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION *</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Kharad Area</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Facing</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="N"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="S"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="E"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="W"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="NW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SE"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="SW"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12">
              <Form.Label>Road Facing width</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Water supply</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Canal"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Muncipality"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Borewell"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Ground Water level</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Soil type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Red"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Black"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>
                Structure <span>super built up area</span>
              </Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Land type</Form.Label>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Wet"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Dry"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
              <Form.Group>
                <Form.Check
                  type="radio"
                  label="Highlands"
                  name="formHorizontalRadios"
                  id=""
                />
              </Form.Group>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>Irrigation type</Form.Label>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Drip"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check
                    type="radio"
                    label="Sprinkler"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col}>
                  <Form.Check                   
                    label="Canal"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios1"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Check                   
                    label="Pipe"
                    name="formHorizontalRadios"
                    id="formHorizontalRadios2"
                  />
                </Form.Group>
              </Form.Row>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Plantation</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Banana"
                required
              />
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Plantation description"
                required
              />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Location</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Location"
                required
              />
              <ButtonToolbar>
                <ButtonGroup>
                  <Button>
                    <i className="fas fa-align-left" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-center" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-right" />
                  </Button>
                  <Button>
                    <i className="fas fa-align-justify" />
                  </Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>About Property</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About Property"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLandmark">
              <Form.Label>Others</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Others"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Pricing</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Maintainenss charges/month</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicLocation">
              <Form.Label>Security Deposit</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="1"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                className="button1 btn-danger btnFullwidth"
                type="submit"
                size="lg"
                block
              >
                Post my listing
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class Loginmodal extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

  render() {
    let modalClose = () =>
      this.setState({
        modalShow1: false
      });

      

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter"></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container>
            <Row>
              <Col className="Loginmodaldiv1">
                <Image src={icon} fluid />
                <Image src={HomeIcon} fluid />

                <p>
                  Find the best matches for you Make the most of high seller
                  scores Experience a joyful journey
                </p>
              </Col>
              <Col className="Loginmodaldiv2">
                <p>You are moments away from selling with Beegru</p>
                <InputGroup className="mb-3 m1">
                  <InputGroup.Prepend>
                    <Button variant="outline">
                      <i className="fa fa-search"></i>
                    </Button>
                  </InputGroup.Prepend>
                  <FormControl aria-describedby="basic-addon1" />
                </InputGroup>
                <p>Who are you?</p>
                <CardDeck>
                  <Card onClick={() => this.setState({ modalShow1: true })}>
                    <Card.Img variant="top" src={avtar1} thumbnail />
                    <Card.Body>
                      <Card.Text>Owner</Card.Text>
                    </Card.Body>
                  </Card>
                  <Card onClick={() => this.setState({ modalShow1: true })}>
                    <Card.Img variant="top" src={avtar2} thumbnail />
                    <Card.Body>
                      <Card.Text>Seller</Card.Text>
                    </Card.Body>
                  </Card>
                  <Card onClick={() => this.setState({ modalShow1: true })}>
                    <Card.Img variant="top" src={avtar3} thumbnail />
                    <Card.Body>
                      <Card.Text>Agent</Card.Text>
                    </Card.Body>
                  </Card>
                </CardDeck>
                <Login show={this.state.modalShow1} onHide={modalClose} />
              </Col>
            </Row>
          </Container>
        </Modal.Body>
      </Modal>
    );
  }
}

export class OtpModal extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
  }

 

  render() {

    let closeOtpModal = () =>   
      this.setState({
        modalShow: false
      });
      
      function handleGameClik() {
        this.setState( {disabled: !this.state.disabled} )
      } 

    function verifyOtp(){      
      if(document.verifyOtpForm.otp.value == ""){
        alert("Please enter your OTP ")
      }else{
      fetch('https://aceimagingandvideo.com/beegru/website/verifyOtp', {
        method: 'POST',
        async: false,
        headers: {
                //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
        body: JSON.stringify({  
          "mobileNumber": document.registration.regPhoneNumber.value,                
          "recievedOtp": document.verifyOtpForm.otp.value                 
        })
    }).then((response) => response.json())
        .then((responseJson) => {
                console.log(responseJson);
                if(responseJson.resultStatus == 1){
                  alert("OTP Verified Successfully");
                  statusVariable = 1;
                  closeOtpModal();
                  handleGameClik();                                             
                }else if(responseJson.resultStatus == 2){
                  alert("Invalid OTP");
                }else{
                  alert("Internal Server Error");
                }
                return responseJson.movies;
        })
        .catch((error) => {
                console.error(error);
        });
      }
    }

    function resendOtp(){
      console.log(document.registration.regPhoneNumber.value);     
      fetch('https://aceimagingandvideo.com/beegru/website/sendOtp', {
        method: 'POST',
        async: false,
        headers: {
                //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                Accept: 'application/json',
                'Content-Type': 'application/json',
        },
        body: JSON.stringify({  
          "mobileNumber": document.registration.regPhoneNumber.value                
                           
        })
    }).then((response) => response.json())
        .then((responseJson) => {
                console.log(responseJson);
                return responseJson.movies;
        })
        .catch((error) => {
                console.error(error);
        });
    }
    

    return (
      <Modal
        {...this.props}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        id="otpModal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter"></Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form name="verifyOtpForm">
            <Form.Group as={Col} md="12">
              <Form.Label>Enter OTP</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="****"
                id="otp"                
              />
            </Form.Group>
            <Form.Row>
              <Form.Group as={Col} md="6" sm="6">
                <Button onClick={verifyOtp} className="btn btn-danger" type="submit" size="md" block>
                  Verify
                </Button>
              </Form.Group>
              <Form.Group as={Col} md="6" sm="6">
                <Button onClick={resendOtp} className="btn btn-danger" type="submit" size="md" block>
                  Resend
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class ListyourServices extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
  }

  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
  }

  render() {
    const { validated } = this.state;

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            List Your Services
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
          >
            <Form.Group as={Col} md="12" controlId="validationCustom23">
              <Form.Label>SERVICE TYPE *</Form.Label>

              <Form.Control
                className="controll"
                type="speciality"
                as="select"
                placeholder="Speciality"
                required
              >
                <option>ARCHITECTURE</option>
                <option>Lawyer</option>
                <option>Interior</option>
                <option>Banker</option>
                <option>Other</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>SERVICE NAME *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Name"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Form.Label>UPLOAD IMAGE *</Form.Label>
              <InputGroup>
                <UploadImage />
              </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>ABOUT</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="About your Services"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>Video URL</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="paste your video url"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="formBasicspeciality">
              <Form.Label>DESCRIPTION</Form.Label>

              <Form.Control
                className="controll"
                type="text"
                as="textarea"
                placeholder="Description about your Services"
                required
              ></Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationCustom21">
              <Form.Label>Rating For Your Services</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="paste your video url"
                required
              />
            </Form.Group>
            <Form.Group as={Col} md="11">
              <Button
                type="submit"
                className="btn btn-danger btnFullwidth"
                size="lg"
                block
              >
                Post Your Services
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export class UploadImage extends React.Component {
  constructor(props) {
		super(props);
		
		this.state = {
			files: [],
			urls: [],
			isDragging: false
		}
		
		this.onChange = this.onChange.bind(this);
		this.handleDrop = this.handleDrop.bind(this);
		this.handleDragEnter = this.handleDragEnter.bind(this);
		this.handleDragOver = this.handleDragOver.bind(this);
		this.handleDragLeave = this.handleDragLeave.bind(this);
		this.handleFiles = this.handleFiles.bind(this);
		this.onRemove = this.onRemove.bind(this);
  }
  
	
	onRemove(index) {
		var {files, urls} = this.state;
		let newFiles = files.filter((file, i) => i !== index);
		let newUrls = urls.filter((url, i) => i !== index);
		
		this.setState({
			...this.state,
			files: newFiles,
			urls: newUrls
		});
	}
	
	handleDrags(e) {
		e.preventDefault();
		e.stopPropagation();
		
		this.setState({
			...this.state,
			isDragging: true
		});
	}
	
	handleDragEnter(e) {
		this.handleDrags(e);
	}
	
	handleDragOver(e) {
		this.handleDrags(e);
	}
	
	handleDragLeave(e) {
		e.preventDefault();
		e.stopPropagation();
		
		this.setState({
			...this.state,
			isDragging: false
		});
	}
	
	onChange(e) {
		e.preventDefault()
		const files = e.target.files;
		[].forEach.call(files, this.handleFiles);
	}
	
	handleDrop(e) {
		e.stopPropagation();
		e.preventDefault();
		
		const data = e.dataTransfer;
		const files = data.files;
		console.log("Oops...you dropped this: ", files);
		
		[].forEach.call(files, this.handleFiles);
		
		this.setState({
			...this.state,
			isDragging: false
		});
	}
	
	handleFiles(file) {
		
		// this could be refactored to not use the file reader
		
		var reader = new FileReader();
		
		reader.onloadend = () => {

			var imageUrl = window.URL.createObjectURL(file);
			
			this.setState({
				files: [file, ...this.state.files],
				urls: [imageUrl, ...this.state.urls]
			});
			
    }
    
    
		
		reader.readAsDataURL(file);
	}
  render() {
    const {urls, files, isDragging} = this.state;
		const dropClass = isDragging ? "dragDrop dragging" : "dragDrop";
    
    return (
      <Container>
      <div className="uploadInput" >
      <input type="file"
        onChange={this.onChange}
        accept="image/*"
        multiple
      />
      <div className={dropClass} 
        onDrop={this.handleDrop}
        onDragOver={this.handleDragOver}
        onDragEnter={this.handleDragEnter}
        onDragLeave={this.handleDragLeave} >
        <div className="inside">
          <span>Drop files here</span>
          <div>
            <i className="material-icons">cloud_upload</i>
          </div>
        </div>
      </div>	
    </div>
    <div className="imagePreviewContainer">
      {
        urls && (urls.map((url, i) => (
          <div className="previewItem" as={Col} md="5">
            <img className="imagePreview" src={url} />
            <div className="details">
              <h6>{files[i].name}</h6>
              <h6>{files[i].size.toLocaleString()} KBs</h6>
              <h6>{files[i].type}</h6>
              <i className="material-icons" 
              onClick={() => this.onRemove(i)}>delete</i>
            </div>
          </div>
        )))
      }
    </div>
    </Container>
    );
  }
}

export class LoginwithOTP extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
    
 }
 


  render() {
    let modalClose = () =>
      this.setState({
        modalShow: false
      });

    return (
      <Modal
        {...this.props}
        size="sm"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Login with OTP</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12">
              <Form.Label>Enter Mobile number or Email</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Mobile number or Email"
                required
              />
            </Form.Group>
           
             
              <Button
                    variant="outline-secondary"
                    className="btn btn-danger"
                    onClick={() => this.setState({ modalShow: true })}
                  >
                    Sent OTP
                  </Button>
               
              <OtpModal show={this.state.modalShow} onHide={modalClose} />
             
              
           
           
          </Form>
          
         

        </Modal.Body>
      </Modal>
    );
  }
}

export class ForgotPassword extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { modalShow: false };
    
 }
 


  render() {
    return (
      <Modal
        {...this.props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Forgot Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Col} md="12">
              <Form.Label>Enter Email id or Password </Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Enter Email id or Password"
                required
              />
            </Form.Group>
            
              
             
                <Button
                  className="btn btn-danger"
                  type="submit"
                  size="md"
                 
                >
                  Reset
                </Button>
              
           
          </Form>
          
         

        </Modal.Body>
      </Modal>
    );
  }
}

export class addOtherAminities extends React.Component {
  state = {
    questions: ['Amenities']
  }

  handleText = i => e => {
    let questions = [...this.state.questions]
    questions[i] = e.target.value
    this.setState({
      questions
    })
  }

  handleDelete = i => e => {
    e.preventDefault()
    let questions = [
      ...this.state.questions.slice(0, i),
      ...this.state.questions.slice(i + 1)
    ]
    this.setState({
      questions
    })
  }

  addQuestion = e => {
    e.preventDefault()
    let questions = this.state.questions.concat([''])
    this.setState({
      questions
    })
  }
 


  render() {
    
    return (
      <div>
      {this.state.questions.map((question, index) => (
        <span  key={index}>
          <Form.Row>
          <Form.Control
            className="controll"
            type="text"
            onChange={this.handleText(index)}
            value={question}
            placeholder="1"
            required
            as={Col} md="8"
          />
          <Button as={Col} md="4" onClick={this.handleDelete(index)}>Delete</Button>
          </Form.Row>
          </span>
        ))}
         <Button onClick={this.addQuestion}>Add</Button>

        </div>
    );
  }
}

