import React, { Component } from 'react'
import { BrowserRouter as Router ,Link} from "react-router-dom";
import { Container, Row, Col } from 'react-bootstrap';
import "../../css/services.css";
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Architecture from "../../images/Architecture.png"


export default class SubServices extends Component {
    state = {}
    render() {
        return (

            <Router>
                <Container className="ServicesPageWrapper">
                    <div className="heading">
                        <ul>
                            <li>Other services</li>
                            <i className='fas fa-angle-right'></i>
                            <li>Architecture</li>

                        </ul>
                    </div>
                    <div className="subheading"><i className="fas fa-city" aria-hidden="true"></i> Architechture</div>
                    <div><ServiceCardDeck /></div>
                    <div><ServiceCardDeck /></div>
                </Container>
            </Router >

        );
    }
}
function ServiceCardDeck() {
    return (
        <CardDeck className="ArchitectureCardContainer">
              <Link to="/CompanyServices">
            <Card>

                <Card.Img variant="top" src={Architecture} fluid />
                <Card.Body>
                    <Card.Title>KSM Architecture</Card.Title>
                    <hr></hr>
                    <Row>
                        <Col className="Servicesiocns" >
                            <p>Budget:<i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                            </p>
                            <p>Rating:  <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                            </p>
                        </Col>
                        <Col>
                            <p>Qty:<i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                            </p>
                            <p>Speed:<i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                            </p>
                        </Col>
                    </Row>
                </Card.Body>

            </Card>
            </Link>
            <Card>
                <Card.Img variant="top" src={Architecture} fluid />
                <Card.Body>
                    <Card.Title>KSM Architecture</Card.Title>
                    <hr></hr>
                    <Row>
                    <Col className="Servicesiocns" >
                            <p>Budget:<i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                            </p>
                            <p>Rating:  <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                            </p>
                        </Col>
                        <Col>
                            <p>Qty:<i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                            </p>
                            <p>Speed:<i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                            </p>
                        </Col>
                    </Row>
                </Card.Body>


            </Card>
            <Card>
                <Card.Img variant="top" src={Architecture} fluid />
                <Card.Body>
                    <Card.Title>KSM Architecture</Card.Title>
                    <hr></hr>
                    <Row>
                    <Col className="Servicesiocns" >
                            <p>Budget:<i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                                <i className="fas fa-rupee-sign" aria-hidden="true"></i>
                            </p>
                            <p>Rating:  <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                                <i className="fas fa-star" aria-hidden="true"></i>
                            </p>
                        </Col>
                        <Col>
                            <p>Qty:<i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                                <i className="fas fa-check-circle" aria-hidden="true"></i>
                            </p>
                            <p>Speed:<i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                                <i className="fas fa-bolt" aria-hidden="true"></i>
                            </p>
                        </Col>
                    </Row>
                </Card.Body>


            </Card>
        </CardDeck>

    );
}
