import React, { Component } from 'react'
import { Link } from "react-router-dom";
import img from "../../images/Services1.png"
import { Container, Row, Col, Image } from 'react-bootstrap';
import "../../css/services.css";
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'



export default class MainServices extends Component {
    state = {}


    handleSubService() {
        alert("sadsad");
        window.location.assign("./CompanyServices");
    }

    render() {
        return (

            <div>
                <Container className="ServicesPageWrapper">
                    <Row>
                        <Col xs={12} sm={9} md={6} lg={6}>
                            <p>Other services</p>
                            <h1>We are affiliated with some service providers in the field of</h1>
                        </Col>
                        <Col xs={12} sm={9} md={6} lg={6}>
                            <Image src={img} fluid />
                        </Col>
                    </Row>


                </Container>
                <Container className="serviceCardContainer">

                    <CardDeck className="servicescard-deck">
                        <Link to="/SubServices">
                            <Card>
                                <i className="fas fa-city" aria-hidden="true"></i>
                                {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
                                <Card.Body>
                                    <Card.Text>
                                        Architecture
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Link>


                        <Card onClick={() => this.handleSubService()}>
                            <i className="fas fa-home" aria-hidden="true"></i>

                            <Card.Body>
                                <Card.Text>
                                    Home loan
                                    </Card.Text>
                            </Card.Body>
                        </Card>

                      
                            <Card>
                                <i className="fas fa-heartbeat" aria-hidden="true"></i>
                                <Card.Body>
                                    <Card.Text>
                                        Fitness machines
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        
                        <Card>
                            <i className="fas fa-user-tie" aria-hidden="true"></i>
                            <Card.Body>
                                <Card.Text>
                                    Legal support
                                    </Card.Text>
                            </Card.Body>
                        </Card>
                    </CardDeck>

                    <CardDeck className="servicescard-deck">
                        <Card>
                            <i className="fas fa-tree" aria-hidden="true"></i>
                            <Card.Body>
                                <Card.Text>
                                    Gradening
                                    </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card>
                            <i className="fas fa-couch" aria-hidden="true"></i>
                            <Card.Body>
                                <Card.Text>
                                    Furniture makers
                                    </Card.Text>
                            </Card.Body>
                        </Card>

                        <Card>
                            <i className="fas fa-vector-square" aria-hidden="true"></i>
                            <Card.Body>
                                <Card.Text>
                                    Interior
                                    </Card.Text>
                            </Card.Body>
                        </Card>

                        <Card>
                            <i className="fas fa-building" aria-hidden="true"></i>
                            <Card.Body>
                                <Card.Text>
                                    Home loan
                                    </Card.Text>
                            </Card.Body>
                        </Card>

                    </CardDeck>
                    <Row className="spantext">
                        <span>If you need any help we can connect you with them!</span>
                    </Row>

                </Container>

                </div>
     

        );
    }
}


