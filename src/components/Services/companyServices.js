import React, { Component } from 'react'
import { BrowserRouter as Router } from "react-router-dom";
import { Container, Row, Col, Image } from 'react-bootstrap';
import "../../css/services.css";
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Architecture from "../../images/Architecture.png"
import CompanyImg from "../../images/companyServices.png"
import { Player } from 'video-react';
import ContactUs from "../Home/ContactUs";
import img1 from "../../images/serviceCardImg1.png";
import img2 from "../../images/serviceCardImg2.png";


export default class CompanyServices extends Component {
    state = {}
    render() {
        return (

            <Router>
                <Container className="ServicesPageWrapper">
                    <div className="heading">
                        <ul>
                            <li>Other services</li>
                            <i className='fas fa-angle-right'></i>
                            <li>Architecture</li>
                            <i className='fas fa-angle-right'></i>
                            <li>KSM Architecture</li>

                        </ul>
                    </div>
                    <div className="subheading"><Image src={Architecture} className="IconImg" /> KSM Architecture</div>
                    <CompanyServiceImage />
                    <CompanyServiceAbout />
                    <CompanyServiceCardContainer />

                    <ContactUs />
                </Container>
            </Router >

        );
    }
}
function CompanyServiceImage() {
    return (
        <Container>
            <Row>
                <Image src={CompanyImg} fluid />
            </Row>
        </Container>
    );
}
function CompanyServiceAbout() {
    return (
        <Container className="AboutServicesPageWrapper">
            <Row className="aboutServicetext">
                <Col xs={12} sm={9} md={2} lg={2}>
                    <h1>About</h1>
                </Col>
                <Col xs={12} sm={9} md={4} lg={4}>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                </p>
                </Col>
                <Col xs={12} sm={9} md={5} lg={5} className="companyaboutImg">

                    <Player
                        playsInline
                        poster="https://s3-alpha-sig.figma.com/img/bc26/ff9d/be283928c64ed77f5c4ced5aa75411bf?Expires=1580688000&Signature=QntbV47H3XS8ycYs-HIDmc5-FefH0YQdDZSxPdbq1aYQdux1KPwVS2t-qidwqLrTYdpwntBQXCWpup8aYlhsjdFzhMM7jnpkq9fEqNO6S1VRv0DEZzX74L5yfciHMWkvOaJSL4UQNRiERZ98bLIdh720RwSfCrpXqqiA9UBqO6yzhHl79v3zWiROBAeq-Op5KVzUHQNUnZ3x7xCfdNa3cjAOo4jLdYVt7jxZ8lomI7IbBa1BY9U~xcy63x4Z11Z23BEK~L3gL9MnmMi8RsX3Ktc2oyE6vkFP2Fg4dsUmQHwB~-HDnoGL2cuz-YjdLhi9UK0aNF-vUOKYjdSHhRTyLw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                        src="https://www.radiantmediaplayer.com/media/bbb-360p.mp4"
                    />

                </Col>
            </Row>
            <Row className="companyServiceAbout">
                <h2>Description at style</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy text
                    of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                     Lorem Ipsum is simply dummy
                     text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
            </Row>
        </Container>
    );
}

function CompanyServiceCardContainer() {

    return (
        <Container className="AboutServicesCardPageWrapper" >
            <h2>Projects connected in pool</h2>
            <CompanyServiceCardDeck />
            <CompanyServiceCardDeck />
        </Container>
    )


}
function CompanyServiceCardDeck() {
    return (

        <CardDeck className="CompanyServiceCardContainer">
            <Card>

                <Card.Img variant="top" src={img1} fluid />
                <Card.Body>
                    <Card.Text>
                        <p>Oromo mall -<span> India</span></p>
                        <p>  <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            (4 review)
                        </p>
                    </Card.Text>
                </Card.Body>



            </Card>
            <Card>
                <Card.Img variant="top" src={img2} fluid />
                <Card.Body>
                    <Card.Text>
                        <p> Mom mall -<span> India</span></p>
                        <p>  <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            (4 review)
                            </p>
                    </Card.Text>
                </Card.Body>


            </Card>
            <Card>
                <Card.Img variant="top" src={img2} fluid />
                <Card.Body>
                    <Card.Text>
                        <p> Mom mall -<span> India</span></p>
                        <p>  <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            <i className="fas fa-star" aria-hidden="true"></i>
                            (4 review)
                            </p>
                    </Card.Text>
                </Card.Body>


            </Card>
        </CardDeck>


    );
}
