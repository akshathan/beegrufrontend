import React from 'react';
import NewPropertyCard from "../coreComponents/NewPropertyCard";
// import PropertyPage from  "../PropertyPage/PropertyPage";
import { Row, Col, Container, Button, Card } from 'react-bootstrap';
import SimilarPropertyCard from "../coreComponents/SimilarPropertyCard";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from "../models/Login.js";
import { Redirect } from 'react-router-dom';
import CardDeck from 'react-bootstrap/CardDeck';
import { UncontrolledPopover, PopoverBody } from 'reactstrap';
import image from "../../images/property1.png";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
const responsive = {
        superLargeDesktop: {
                // the naming can be any, depends on you.
                breakpoint: { max: 4000, min: 3000 },
                items: 3,
        },
        desktop: {
                breakpoint: { max: 3000, min: 1024 },
                items: 2,
        },
        tablet: {
                breakpoint: { max: 1024, min: 464 },
                items: 2,
        },
        mobile: {
                breakpoint: { max: 464, min: 0 },
                items: 1,
        },
};



export default class NewCardDeckContainer extends React.Component {
        constructor(props) {
                super(props);
                this.getCardData = this.getCardData.bind(this);
                this.onClickHandler = this.onClickHandler.bind(this);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);
                this.state = { users: [] };
                this.viewAll = this.props.viewAll ? this.props.viewAll : "No more data";
                this.loadPropertyData = this.loadPropertyData.bind(this);
                //this.state = { limit: 4};
        }

        componentDidMount() {
                fetch('https://aceimagingandvideo.com/beegru/website/getSimilerProperties', {
                        method: 'POST',
                        async: false,
                        headers: {
                                //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                "propertyId":"15",
                                "mobileNumber": "8970685540"
                        })
                }).then(response => {
                        return response.json();
                }).then(result => {
                        console.log("Result", result);
                        this.setState({
                                users: result,
                        });
                        console.log("Featured Properties Result", this.state.users);

                });

        }



        loadPropertyData() {
               
                let that = this;
                let propertyData = [];
                console.log("this.state.users.NewPropertiesList", that.state.users)
                if (that.state.users && that.state.users && that.state.users.length) {

                        propertyData = that.state.users && that.state.users.map((proper, index) => {
                                // console.log("sunil::proper", proper, index);
                                const srcName = "https://aceimagingandvideo.com/assets/beegru/".concat(proper.propertyType).concat("_").concat(proper.propertyStatus).concat("/").concat(proper.propertyId).concat("/").concat("image1.png");
                                var style1 = {
                                        backgroundImage: 'url(' + srcName + ')',
                                };
                                
                                if (this.state.redirect) {
                                        return <Redirect push to="/PropertyPage" />;
                                }
                                return (


                                        <Col >

                                                <div className="propertyCard cardBottom" id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}></div>
                                                <div className="Newcard pointer" onClick={() => this.singlepropertypage(proper.propertyId)}>
                                                        <div className="propertyCardImg" src="https://aceimagingandvideo.com/assets/beegru/Resedential_Rent/15/image1.png"></div>
                                                        <div className="pxc-stopper" style={style1}>

                                                                <div className="propertyHeading">{proper.propertyName}
                                                                        <p ><i className="fas fa-map-marker-alt"></i> {proper.propertyAddress}
                                                                                <span className="house">{proper.propertyType} for {proper.propertyStatus}sqft</span></p>
                                                                        {/* <div id="buttonheart"> <i className="far fa-heart"></i></div> */}
                                                                </div>
                                                        </div>

                                                        <div className="pxc-subcard">
                                                                <div className="bhr">
                                                                        <button className="salebutton">{proper.propertyStatus}</button>
                                                                        {/* <button className="salebutton">SALE</button>
                                                                        <button className="rentbutton">RENT</button> */}
                                                                        {/* <button className="investbutton">INVEST</button> */}
                                                                </div>

                                                                <p className="Newcard-text" > <i className="fas fa-building"></i>{proper.rooms} BHK {proper.squareFeet}sqft</p>
                                                                <p className="Newcard-text" ><i className="fas fa-rupee-sign"></i>{proper.propertyAmount}</p>
                                                        </div>

                                                        <div id="buttonshare">
                                                                <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like to Share"></i>

                                                        </div>
                                                        <UncontrolledPopover trigger="legacy" placement="bottom" target="buttonshare">
                                                                {/* <PopoverHeader>Legacy Trigger</PopoverHeader> */}
                                                                <PopoverBody>
                                                                        <div className="popicon">
                                                                                <Link><i className='fab fa-facebook-f'></i></Link>

                                                                                <Link > <i className='fab fa-twitter' ></i></Link>

                                                                                <Link ><i className='fab fa-instagram' ></i></Link>

                                                                                <Link ><i className='fab fa-whatsapp' ></i></Link>
                                                                        </div>
                                                                </PopoverBody>
                                                        </UncontrolledPopover>


                                                </div>

                                        </Col>
                                )
                        });
                }
                console.log("")
                return propertyData;
        }

        singlepropertypage(propertyId) {
                // alert(propertyId);
                localStorage.setItem('propertyId', propertyId)
                this.setState({ redirect: true });
                // window.location="/PropertyPage";
        }



        onClickHandler = cardData => {
                this.props.onClickHandler(cardData);

        }
        getCardData() {
                let length = this.props && this.props.cardData ? this.props.cardData.length : 0;
                if (length > 0) {
                        //this.setState({data : cardData});
                        this.generatedData = this.props.cardData.map((card) => {
                                return <SimilarPropertyCard onClickHandler={() => this.onClickHandler()} cardData={card} />
                        });
                        return this.generatedData;
                }
        }
        render() {
                let featuredpropertyData = this.loadPropertyData();
                return (
                        <Container>

                                <Carousel responsive={responsive} autoPlay={true}>
                                        {featuredpropertyData}
                                        {/* <div>  {featuredpropertyData}</div> */}
                                </Carousel>
                        </Container>
                )
        }
};

