import React from 'react';
import "../../css/NewPropertyCard.css";
import image from "../../images/property1.png";
import { Col } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { UncontrolledPopover, PopoverBody } from 'reactstrap';



export default class NewPropertyCard extends React.Component {
        constructor(props) {
                super(props);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);

        }
        onClickHandler = cardDataa => {

                // <Router>
                //<Link to="/PropertyPage"> </Link>
                // </Router>
                //return <PropertyPage cardData={this.cardData} />
                // let cardData = this.cardData;
                this.props.onClickHandler(this.cardData);
        }
        render() {




                let propertyDetails = this.cardData ? this.cardData.propertyDetails[0] : {};

                return (



                        <Col >

                                <div className="propertyCard cardBottom" id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}></div>
                                <div className="Newcard">
                                        <div className={"propertyCardImg"} src={/*this.cardData.images[0].mobile*/image}></div>
                                        <div className="pxc-stopper">
                                                <div className="propertyHeading">{propertyDetails.heading}
                                                        <p ><i className="fas fa-map-marker-alt"></i> {propertyDetails.locality + propertyDetails.distict + propertyDetails.state}
                                                                <span className="house">HOUSE: TOLET / FOR SALE</span></p>
                                                        {/* <div id="buttonheart"> <i className="far fa-heart"></i></div> */}
                                                </div>
                                        </div>

                                        <div className="pxc-subcard">
                                                <div className="bhr">
                                                        <button className="salebutton">SALE</button>
                                                        <button className="rentbutton">RENT</button>
                                                        <button className="investbutton">INVEST</button>
                                                </div>

                                                <p className="Newcard-text" > <i className="fas fa-building"></i> 2-5 BHK</p>
                                                <p className="Newcard-text" ><i className="fas fa-rupee-sign"></i>{propertyDetails.priceRange}</p>
                                        </div>

                                        <div id="buttonshare">
                                                <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like to Share"></i>

                                        </div>
                                        <UncontrolledPopover trigger="legacy" placement="bottom" target="buttonshare">
                                                {/* <PopoverHeader>Legacy Trigger</PopoverHeader> */}
                                                <PopoverBody>
                                                        <div className="popicon">
                                                                <Link><i className='fab fa-facebook-f'></i></Link>

                                                                <Link > <i className='fab fa-twitter' ></i></Link>

                                                                <Link ><i className='fab fa-instagram' ></i></Link>

                                                                <Link ><i className='fab fa-whatsapp' ></i></Link>
                                                        </div>
                                                </PopoverBody>
                                        </UncontrolledPopover>


                                </div>

                        </Col>





                )

        }
};

