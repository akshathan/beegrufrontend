import React from 'react';
import PropertyCard from "../coreComponents/PropertyCard";
// import PropertyPage from  "../PropertyPage/PropertyPage";
import CardDeck from 'react-bootstrap/CardDeck';
import { Row, Col, Container, Button, Card } from 'react-bootstrap';
// import Card from 'react-bootstrap/Card';
import image from "../../images/property.png";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from "../models/Login.js";
import { Redirect } from 'react-router-dom';
import { ResponsiveEmbed, Image } from 'react-bootstrap';


export default class CardDeckContainer extends React.Component {
        constructor(props) {
                super(props);
                this.getCardData = this.getCardData.bind(this);
                this.onClickHandler = this.onClickHandler.bind(this);
                this.cardData = this.props.cardData ? this.props.cardData : {};
                this.generatedData = undefined;
                this.onClickHandler = this.onClickHandler.bind(this);
                this.state = { users: [] };
                this.viewAll = this.props.viewAll ? this.props.viewAll : "No more data";
                this.loadPropertyData = this.loadPropertyData.bind(this);
                this.state = { limit: 4 };
                this.setState({ totalProperties: 0 });
                this.setState({ isPropertyFound: true });
                this.wishListIcon = '';
        }


        onLoadMore = () => {

                this.setState({ limit: this.state.limit + 4 });
                // alert(this.state.limit);
                if (this.state.limit >= this.state.totalProperties) {
                        this.setState({ isPropertyFound: true });
                }
        }

        changeColor(i) {
                //                 alert(i);
                //                 console.log(this.state.users.featuredPropertiesList)
                //                 var a = JSON.stringify(this.state.users.featuredPropertiesList)
                //                 console.log(a);
                //                 console.log(a.indexOf(i));
                //                  a = JSON.parse(a);
                // console.log(a);

                //                 console.log(a.indexOf(i));
                //         //let rows = [...this.state.rows]
                //        // rows.splice(i, 1)
                //      //  property1004
                //        alert(i)
                //        let element = document.getElementById('demoproperty1004')
                //        // element.style.backgroundColor = 'red'
                //        console.log(element.style.color+"sdddddd");
                //        //element.classList.add("fa fa-heart");
                //        if(element.style.color == ''){
                //                 element.style.color = 'red'
                //        }else {
                //         element.style.color = ''   
                //        }

                //        // this.setState({ 
                //          // rows: rows
                //         //})

        }

        componentDidMount() {
                // fetch('https://192.168.100.110:8081/website/fetchIndexInfo')
                //         .then(response => {
                //                 return response.json();
                //         }).then(result => {
                //                 console.log("sunil:result*********", result.featuredPropertiesList.length);
                //                 this.setState({totalProperties: result.featuredPropertiesList.length});
                //                 this.setState({
                //                         users: result
                //                 });

                //         });

                fetch('https://aceimagingandvideo.com/beegru/website/fetchIndexInfo', {
                        method: 'POST',
                        async: false,
                        headers: {
                                // "Authorization": "Basic " + btoa("8970685540"+ ":" +"123"),
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                "mobileNumber": "8970685540"
                        })
                }).then((response) => response.json())
                        .then((responseJson) => {
                                console.log(responseJson);
                                console.log("sunil:result*********", responseJson.featuredPropertiesList.length);
                                this.setState({ totalProperties: responseJson.featuredPropertiesList.length });
                                this.setState({
                                        users: responseJson
                                });
                        })
                        .catch((error) => {
                                console.error(error);
                        });
        }

        singlepropertypage(propertyId) {
                // alert(propertyId);
                localStorage.setItem('propertyId', propertyId)
                this.setState({ redirect: true });
                // window.location="/PropertyPage";
        }

        addOrDeleteWishList(propertyId) {

                //         alert('property'+productId);
                //        alert(productId)
                let element = document.getElementById('productId' + propertyId);

                //        console.log(element.style.color+"sdddddd");

                //        if(element.style.color == ''){
                //                 element.style.color = 'red'
                //                 this.wishListIcon= 'far fa-heart'
                //        }else {
                //         element.style.color = ''   
                //        }

                fetch('https://aceimagingandvideo.com/beegru/login/saveWishList', {
                        method: 'POST',
                        async: false,
                        crossDomain: false,
                        headers: {
                                "Authorization": "Basic " + btoa("9902376090" + ":" + "swa123thi"),
                                'Access-Control-Allow-Origin': '*',
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                "propertyId": propertyId
                        })
                }).then(response => {
                        return response.json();
                }).then(result => {
                        console.log(result.resultStatus);
                        if (result.resultStatus == 1) {
                                //    alert("dsdsd");
                                element.style.color = 'red';
                                element.style.fontWeight = '900';
                        } else if (result.resultStatus == 3) {
                                element.style.color = 'white';
                                element.style.fontWeight = '100';
                        }
                });

        }

        loadPropertyData() {
                let that = this;
                let propertyData = [];
                let wishList;
                let modalClose = () =>
                        this.setState({
                                modalShow: false
                        });
                var style = {
                        fontWeight: 'bold',
                        //  fontWeight: 900 !important',
                        color: 'blue',
                };


                console.log("this.state.users.featuredPropertiesList", that.state.users)
                if (that.state.users && that.state.users.featuredPropertiesList && that.state.users.featuredPropertiesList.length) {

                        propertyData = that.state.users && that.state.users.featuredPropertiesList.slice(0, this.state.limit).map((proper, index) => {
                                console.log("sunil::proper", proper, index);
                                if (proper.propertyLiked) {
                                        var style = {
                                                color: 'red',
                                                fontWeight: '900',
                                        };
                                        this.wishListIcon = 'fa fa-heart';
                                } else {
                                        var style = {
                                                fontWeight: 'normal',
                                                color: 'white',
                                        };
                                }

                                if (!this.state.isloaded) {
                                        wishList = (
                                                <Link onClick={() => this.setState({ modalShow: true })}><i className="far fa-heart heart"></i></Link>

                                        );
                                } else {
                                        wishList = (
                                                <i className="far fa-heart" onClick={() => this.addOrDeleteWishList(proper.propertyId)} style={style} id={"productId" + proper.propertyId}></i>
                                        );
                                }
                                if (this.state.redirect) {
                                        return <Redirect push to="/PropertyPage" />;
                                }
                                var imageName = '';
                                const srcName = "https://aceimagingandvideo.com/assets/beegru/".concat(proper.propertyType).concat("_").concat(proper.propertyStatus).concat("/").concat(proper.propertyId).concat("/");
                                return (

                                        <div className="propertyCard col-lg-5" id={proper.propertyId} onClick={(cardData) => this.onClickHandler(proper.propertyId)}>
                                                {/* <img style={{display:"none"}} src={img}></img> */}
                                                {/* <Link to="./../PropertyPage/PropertyPage" > */}
                                                <Card className="h-100">
                                                        <div className="top-left">
                                                                {/* <Link onClick={() => this.setState({ modalShow: true })}>
                                                                        <i className="fa fa-heart" ></i>
                                                                </Link> */}
                                                                {wishList}
                                                                <Login
                                                                        show={this.state.modalShow}
                                                                        onHide={modalClose}
                                                                />
                                                                {/* <i className="fa fa-heart"  onClick={() => this.addOrDeleteWishList(proper.propertyId)} style={style} id={"productId" + proper.propertyId}></i> */}
                                                        </div>
                                                        <Card.Img variant="top" resizemode="cover" className="card-img-top pointer" src={srcName.concat("image1.png")} onClick={() => this.singlepropertypage(proper.propertyId)} >

                                                        </Card.Img>
                                                        {/* <div><img src={image}/></div> */}
                                                        {/* <Link to="./../PropertyPage/PropertyPage" >*/}
                                                        <Card.Body className="propertyCardData">
                                                                <Card.Title>
                                                                        <h4>
                                                                                <i className="fas fa-rupee-sign"></i> {proper.propertyAmount}
                                                                        </h4>
                                                                </Card.Title>
                                                                <Card.Text>
                                                                        <h1 className="propertyHeading">{proper.propertyName}</h1>

                                                                        <p className="space">
                                                                                <i className="fas fa-map-marker-alt"></i>
                                                                                {proper.propertyAddress}
                                                                        </p>
                                                                        <p className="space hr">
                                                                                <Row>
                                                                                        <Col lg={5} sm={5} md={5} xs={5}>
                                                                                                <i className="fas fa-home"></i> {proper.propertyType}
                                                                                        </Col>
                                                                                        <Col lg={3} sm={3} md={3} xs={3}>
                                                                                                <i className="far fa-star"></i> Top 5
                                                                        </Col>
                                                                                        <Col lg={4} sm={4} md={4} xs={4}>
                                                                                                <i className="fas fa-hand-holding-usd"></i> {proper.propertyStatus}
                                                                                        </Col>


                                                                                </Row>
                                                                        </p>
                                                                </Card.Text>
                                                                <Row className="hr">
                                                                        <Col>
                                                                                <i className="far fa-object-ungroup"></i>   {proper.squareFeet}sqft
                                                                                        </Col>
                                                                        <Col>
                                                                                <i className="far fa-building"></i>   {proper.rooms} BHK
                                                                </Col>

                                                                </Row>
                                                        </Card.Body>
                                                        {/* </Link>  */}
                                                </Card>


                                        </div>
                                )
                        });
                }
                console.log("")
                return propertyData;
        }

        onClickHandler = cardData => {
                this.props.onClickHandler(cardData);

        }

        getCardData() {
                let length = this.props && this.props.cardData ? this.props.cardData.length : 0;
                if (length > 0) {
                        // this.setState({data : cardData});
                        this.generatedData = this.props.cardData.map((card) => {
                                return <PropertyCard onClickHandler={() => this.onClickHandler()} cardData={card} />
                        });
                        return this.generatedData;
                }
        }


        render() {

                let featuredpropertyData = this.loadPropertyData();
                let style = this.props.style ? this.props.style : undefined;

                let that = this;
                return (
                        <Container className="propertyCardContainer">
                                <CardDeck>
                                        <div id="cardContainer" >
                                                {/* <PropertyCard cardData={this.props.cardData} onClickHandler={this.props.clickHandler}></PropertyCard>
                                 */}
                                                {featuredpropertyData}
                                                {/* {this.getCardData()} */}
                                                {/* <PropertyCard  /> */}
                                        </div>
                                </CardDeck>
                                <div className="actionButtonContainer">

                                        <Button className="seemorebtn" type="submit" style={{ style }} onClick={this.onLoadMore}>
                                                {(that.state.isPropertyFound) ? "No More Data" : "See More Featured Properties"}
                                        </Button>
                                </div>
                        </Container>
                );
        }
}

