import React from "react";
import HomeSearch from "./HomeSearch.jsx";
import NewProperties from "./Home/NewProperties";
import SimilarProperties from "./Home/SimilarProperties";
import AboutBeeger from "./Home/AboutBeeger";
import FindPropertiesNearBy from "./Home/FindPropertiesNearBy";
import ContactUs from "../components/Home/ContactUs";
import Chatbuttons from "../components/Home/Chatbuttons";
// import AboutUs from "../components/Home/AboutUs";
import FeaturedProperties from "./featuredProperites.jsx";
import PropertyPage from "../components/PropertyPage/PropertyPage";
import NewThings from "./Home/NewThings";
import HappyTestimonials from "./testimonials";
import Banner1 from "./Banner1";
import Banner2 from "./Banner2";
import "../css/mediaQuery.css";
import place from '../SampleData/data.json' 

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
    
    this.place =  place ? place.placeSelections :{};
      
    };
   
  
  onClickHandler(cardData) {
    //   this.props.onClickHandler();
    return <PropertyPage data={this.data} />;
  }

  render() {
    return (
      <div className="home">
        {/* <div className="containnerWrapper hero">
                <PropertyPage >
                </PropertyPage>
            </div> */}
        <div className="containnerWrapper hero">
          <HomeSearch options={this.place} />
        </div>
        <div className="containnerWrapper properties featuredProperties">
          <FeaturedProperties onClickHandler={this.onClickHandler} />
        </div>
        <div className="containnerWrapper newThings">
          <NewThings />
        </div>
        <div className="containnerWrapper aboutBeeger">
          <AboutBeeger />
        </div>
        {/* <div className="containnerWrapper properties newProperties">
          <NewProperties onClickHandler={this.onClickHandler} />
        </div> */}
        <div className="containnerWrapper">
          <HappyTestimonials />
        </div>
        <div className="containnerWrapper properties newProperties">
          <SimilarProperties onClickHandler={this.onClickHandler} />
        </div>
        <div className="containnerWrapper">
          <FindPropertiesNearBy />
        </div>

        <div className="containnerWrapper contactUs">
          <ContactUs />
        </div>
        <div className="containnerWrapper">
          <Banner1 />
        </div>

        <div className="containnerWrapper">
          <Chatbuttons />
        </div>
        <div className="containnerWrapper banner1">
          <Banner2 />
        </div>
      </div>
    );
  }
}
