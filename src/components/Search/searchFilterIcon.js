import React from 'react';
import data from "../../SampleData/data.json";

export default class searchFilterIcon extends React.Component {
        constructor(props){
                super(props);
                this.getSearchIcons = this.getSearchIcons.bind(this);
                
	
            }
            
            getSearchIcons(){
               return data.PropertyType.map((value) =>{
                 let unicode = value.unicode,
                  
                 classNameVal = value.className;
                 return <div className="searchFilterIcon"> 
                            <div className= "unicode" style= {{fontFamily: "FontAwesome","content":unicode}}>
                                <i className={classNameVal}>
                                    {/* {"&#x" + unicode} */}
                                </i>
                            </div>
                            <div className= "unicodeText">
                                {value.value}
                               
                            </div>
                        </div>
                    });
                }
        render() {
           
                return this.getSearchIcons();
        }
}
