import React from 'react';
import { Row, Col, Container, Button, Form, Card } from 'react-bootstrap';
import '../../css/demo.css'

class TopCategories extends React.Component {

  constructor(props) {
    super(props);
    this.state = {addClass: false}
  }
  toggle() {
    this.setState({addClass: !this.state.addClass});
  }
  render() {
    let boxClass = ["box"];
    if(this.state.addClass) {
      boxClass.push('green');
    }
    return(
        <div className={boxClass.join(' ')} onClick={this.toggle.bind(this)}>
          {this.state.addClass ? "Remove a class" : "Add a class (click the box)"}</div>       
    );
  }
}


export default TopCategories

