import React, { Component } from "react";
// import Smallheader from './SmallHeader';
import CardDeckContainer from "../coreComponents/CardDeckContainer";
import { Card, CardImg, CardTitle, CardSubtitle } from "reactstrap";

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],


  };
  }
  renderResults() {
    let { users } = this.props;
    console.log("results", users);
    if (users && users.length) {
      return users.map(({  }, i) => {
        return (
          <Card key={"card-" + i}>
            <a href="#">
          
              <CardTitle>{users.priceRange}</CardTitle>
              <CardSubtitle>{users.heading}</CardSubtitle>
            </a>
          </Card>
        
        );
      });
    }
  }

  render() {
    return (
      <div>
        {/* <Smallheader /> */}
        <div className="my-5">
          <div className="container text-center" id="contactContainer">
            <div className="row">
              <div className="col-lg-12 mx-auto">
                <h2 className="text-center">Search Results</h2>
                <hr className="my-4 thick-hr" />
              </div>
            </div>
            <div className="row">
              <div className="col-6 col-lg-3 mt-4">{this.renderResults()}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchResult;


