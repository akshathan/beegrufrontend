import React from 'react'
import { Button } from 'semantic-ui-react'

const SocialmediaIcon = () => (
  <div>
    <Button circular color='facebook' icon='facebook' />
    <Button circular color='twitter' icon='twitter' />
    <Button circular color='linkedin' icon='linkedin' />
    <Button circular color='google plus' icon='google plus' />
    <Button circular color='whatsapp' icon='whatsapp' />
  </div>
)

export default SocialmediaIcon