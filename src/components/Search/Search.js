import React from 'react';
import { Row, Col, Container, Button, Form, Card } from 'react-bootstrap';
import SearchFilterIcon from '../Search/searchFilterIcon';
import CollectionIcon from '../Search/CollectionIcon';
import "../../css/propertyListPage.css";
import ContactUs from "../../components/Home/ContactUs";
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import SearchResult from './SearchResults';
import CardDeck from 'react-bootstrap/CardDeck';
import { UncontrolledPopover, PopoverBody } from 'reactstrap';

// import Card from 'react-bootstrap/Card';
import "../../css/NewPropertyCard.css";
import image from "../../images/property1.png";
import { Link } from "react-router-dom";
import data from "../../SampleData/data.json";


export default class PropertyListPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            checkbox: [],
        };
        this.state = {
            component: 'buy',
            statuscomponent: 'relavent',
            collectioncomponent: 'NRI',
            propertytypecomponent: 'House'
          }
        this.statusValue = 'relavent';
        this.displayData = []
        this.state = {
            totalamount: '',
            propertyPlace: '',
            modelSelected: "",
            locationSelected: "",
            value: '',
            key: [],
            propertystatus:localStorage.getItem("propertyStatus"),
            propertyType: [],
            subLocation: '',
            minPrice:'10000',
            maxPrice:'10000000'

        };
        this.propertyStatus = '';
        this.priceRangeStatus = 1;
        this.minimumPrice = 0;
        this.maximumPrice = 0;
        this.propertyPlace = '';
        this.propertystatus = this.props.propertystatus ? this.props.propertystatus : {};
        this.cardData = this.props.cardData ? this.props.cardData : {};
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubLocationChange = this.handleSubLocationChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handlepropertyTypeSelect = this.handlepropertyTypeSelect.bind(this);
        this.handlecollectionSelect = this.handlecollectionSelect.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.loadPropertyData = this.loadPropertyData.bind(this);
        this.renderProperties = this.renderProperties(this);
        this.getCollectionIcons = this.getCollectionIcons.bind(this);
        this.getSearchIcons = this.getSearchIcons.bind(this);
        this.getSublocationDiv = this.getSublocationDiv.bind(this);
        this.searchSubLocation = this.searchSubLocation.bind(this);
        this.doFilter = this.doFilter.bind(this);
        this.collection = [];
        this.resultsubSearch = [];
        this.subsearchLocation = '';
        //  this.setState({ propertystatus: event.target.attributes.getNamedItem("data-key").value }, () => console.log('propertyType', this.state.propertystatus));
        this.propertyStatus = localStorage.getItem("propertyStatus");
        this.propertyPlace = localStorage.getItem("propertyPlace");
        this.filterPropertyType = [];
        this.setState({ radio: 1 });
        let that = this;
        // fetch('https://aceimagingandvideo.com/beegru/website/propertiesBasedOnSearch', {
        //     method: 'POST',
        //     async: false,
        //     crossDomain: false,
        //     headers: {
        //         //   "Authorization": "Basic " + btoa("8970685540"+ ":" +"776447"),
        //         'Access-Control-Allow-Origin': '*',
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify({
        //         "propertyPlace": localStorage.getItem("propertyPlace"),
        //         "propertyStatus": localStorage.getItem("propertyStatus"),
        //         "mobileNumber":"8970685540"
        //     })
        // }).then(response => {
        //     return response.json();
        // }).then(result => {
        //     this.setState({
        //         users: result,
        //     });
        //     console.log(this.state.users);
        // });

    }

    display =  (data) => {
        let that = this;
        this.setState({
                users: data,
        });
       that.state.users && that.state.users.map((proper, index) => {
            console.log("shruthi::proper", proper, index);
                if(index == 0){
                   this.minAmount = proper.minPrice;
                   this.maxAmount = proper.maxPrice;
                }          
                if(proper.minPrice<this.minAmount){
                    this.minAmount = proper.minPrice;
                }
                   
                if(proper.maxPrice>this.maxAmount){
                    this.maxAmount = proper.maxPrice;
                }
        });
      
        console.log(this.minAmount);
        console.log(this.maxAmount);
        this.minimumPrice = this.minAmount;
        this.maximumPrice = this.maxAmount;
        this.setState({ minPrice: this.minAmount });
        this.setState({ maxPrice: this.maxAmount });
    }
    
      componentDidMount() {
        if(localStorage.getItem("searchStatus") == "1")  {
        fetch('https://aceimagingandvideo.com/beegru/website/propertiesBasedOnSearch',{
                method: 'POST',
                async: false,
                headers: {
                       // "Authorization": "Basic " + btoa("8970685540"+ ":" +"776447"),
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                },
                body: JSON.stringify({                  
                    "propertyPlace":localStorage.getItem("propertyPlace"),
                    "propertyStatus":localStorage.getItem("propertyStatus"), 
                    "mobileNumber":"8970685540"                
                })
                }).then(response => response.json())
                .then(this.display)
                .catch() 
                
            }else{
               // alert("pincode");
                fetch('https://aceimagingandvideo.com/beegru/website/propertiesBasedOnPincode',{
                method: 'POST',
                async: false,
                headers: {
                        "Authorization": "Basic " + btoa("8970685540"+ ":" +"776447"),
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                },
                body: JSON.stringify({                  
                    "pincode": localStorage.getItem("pincode"),
                    "mobileNumber":"8970685540"
                })
                }).then(response => response.json())
                .then(this.display)
                .catch() 
            }
        }

    
    setComponent(component) {
       // alert(component);
        this.setState({
          component
        });
        this.propertyStatus = component;
        this.doFilter(); 
      }
    setStatusComponent(statuscomponent, status) {
        this.setState({
            statuscomponent
        });
        this.statusValue = statuscomponent;
        this.priceRangeStatus = status;
        //alert(this.priceRangeStatus);
        this.doFilter();
      }
      setCollectionComponent(collectioncomponent) {
        this.setState({
            collectioncomponent
        });
      }
      setPropertyTypeComponent(propertytypecomponent) {
        this.setState({
            propertytypecomponent
        });
      }
     
    getSearchIcons() {
        return data.PropertyType.map((value) => {
            let unicode = value.unicode,

                classNameVal = value.className;
            return <div onClick={() => this.handleFilter(value.value)} id={"status" + (value.value)} className="searchFilterIcon">

                <div className="unicode" style={{ fontFamily: "FontAwesome", "content": unicode }}>
                    <i className={classNameVal}>
                        {/* {"&#x" + unicode} */}
                    </i>
                </div>
                <div className="unicodeText">
                    {value.value}

                </div>
            </div>
        });
    }

    getCollectionIcons() {
        return data.Collection.map((value) => {
            let unicode = value.unicode,
                classNameVal = value.className;
            return <div onClick={() => this.handleDeleteRow(value.value)} id={"status" + (value.value)} className="searchFilterIcon">
                <div className="unicode" style={{ fontFamily: "FontAwesome", "content": unicode }}>
                    <i className={classNameVal}>
                        {/* {"&#x" + unicode} */}
                    </i>
                </div>
                <div className="unicodeText">
                    {value.value}
                </div>
            </div>
        });
    }

    minPriceChangeHandler = (event) => {
        this.setState({ minPrice: event.target.value });
        this.minimumPrice = event.target.value;
        this.doFilter();
    }

   maxPriceChangeHandler  = (event) => {
        this.setState({ maxPrice: event.target.value });
        this.maximumPrice = event.target.value;
        this.doFilter();

    }

    handleFilter(i) {
        let element = document.getElementById('status' + i);
      //  element.style.border = '1.5px solid #AA0000';
        if (!(this.filterPropertyType.includes(i))) {
            element.style.border = '1.5px solid #AA0000';
            this.filterPropertyType.push(i);
        }else{
            element.style.border = '1.5px solid #BDBDBD';
            var index = this.filterPropertyType.indexOf(i);
            this.filterPropertyType.splice(index,1);
        }
        this.doFilter();
    }

    handleDeleteRow(i) {
        let element = document.getElementById('status' + i);
       // element.style.border = '1.5px solid #AA0000';
        if (!(this.collection.includes(i))) {
            element.style.border = '1.5px solid #AA0000';
            this.collection.push(i);
        }else{
            element.style.border = '1.5px solid #BDBDBD';
            var index = this.collection.indexOf(i);
            this.collection.splice(index,1);
        }
        console.log(this.collection);
        this.doFilter();
    }


    radio(i) {
          //alert(i);
        this.priceRangeStatus = i;
        this.doFilter();
        //  alert(this.priceRangeStatus);
    }

    doFilter() {
       // alert(this.propertyPlace);
        this.displayData = [];
      
        fetch('https://aceimagingandvideo.com/beegru/website/propertiesBasedOnFilters', {
            method: 'POST',
            async: false,
            crossDomain: false,
            headers: {
                // "Authorization": "Basic " + btoa("8970685540"+ ":" +"776447"),
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "propertyPlace": this.propertyPlace,
                "minPrice": this.minimumPrice,
                "maxPrice": this.maximumPrice,
                "propertyType": this.filterPropertyType,
                "collection": this.collection,
                "priceRangeStatus": this.priceRangeStatus,
                "propertyStatus": this.propertyStatus,
                "mobileNumber":"8970685540"   

            })
        }).then(response => {
            return response.json();
        }).then(result => {
            this.setState({
                users: [],
            });
            this.setState({
                users: result,
            });
            console.log(this.state.users.length);
        });
    }

 

    handlepropertyTypeSelect(event) {
        this.setState({ value: event.target.value }, () => console.log('propertyType', this.state.value));
    }
    handlecollectionSelect(event) {
        this.setState({ key: event.target.value }, () => console.log('collection', this.state.key));
    }
    handleButtonClick = event => {
        //this.setState({ propertystatus: event.target.attributes.getNamedItem("data-key").value }, () => console.log('propertyType', this.state.propertystatus));
    }

    handleLocationChange = (event) => {
       // alert(event.target.value);
        this.propertyPlace = event.target.value;
        this.setState({ propertyPlace: event.target.value }, () => console.log('propertyPlace', this.state.propertyPlace));
    }

    handleSubLocationChange = (event) => {
        this.subsearchLocation = event.target.value;
        this.setState({ propertyPlace1: event.target.value }, () => console.log('propertyPlace1', this.state.propertyPlace1));
        fetch('https://aceimagingandvideo.com/beegru/website/fetchLocation', {
            method: 'POST',
            async: false,
            crossDomain: false,
            headers: {
                "Authorization": "Basic " + btoa("9113855936" + ":" + "123"),
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "location": this.subsearchLocation
            })
        }).then(response => {
            return response.json();
        }).then(result1 => {
            this.resultsubSearch.push(result1);
            console.log("inside search sub location");
            console.log(this.resultsubSearch);

        });

    }

    searchSubLocation() {
            this.doFilter();
    }

    getSublocationDiv() {

    }


    handleClearForm(event) {
        event.preventDefault();
        this.setState({
            minPrice: '',
            maxPrice: '',
            propertyPlace: '',
            propertyStatus: '',
            propertyType: [],
            key: [],
        });
    }


    handleFormSubmit(event) {

        event.preventDefault();
        const formPayload = {
            minPrice: this.state.minPrice,
            maxPrice: this.state.maxPrice,
            propertyPlace: this.state.propertyPlace,
            propertyType: this.state.value,
            pricefilter: this.state.radio,
            collection: this.state.key,
            propertyStatus: this.state.propertystatus,
        };
        console.log('Send this in a POST request:', formPayload);
        fetch('https://aceimagingandvideo.com/beegru/website/propertiesBasedOnFilters', {
            method: 'POST',
            async: false,
            headers: {
                //  "Authorization": "Basic " + btoa("8970685540"+ ":" +"950893"),
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "propertyPlace": this.state.propertyPlace,
                "minPrice": this.state.minPrice,
                "maxPrice": this.state.maxPrice,
                "propertyType": this.state.value,
                "collection": this.state.key,
                "propertyStatus": this.state.propertystatus,
            })
        }).then(response => {
            return response.json();
        }).then(result => {
            console.log("Result", result);
            this.setState({
                users: result,
            });
            console.log(" Properties Result", this.state.users);

        });
        this.handleClearForm(event);
    }

    renderProperties() {




    }

    onClickHandler = cardData => {
        this.props.onClickHandler(cardData);

    }
    loadPropertyData() {

        //if(formPayload !== undefined){

        let that = this;
        let propertyData = [];
        console.log("shruthi", that.state.users)
        if (that.state.users && that.state.users.length) {

            propertyData = that.state.users && that.state.users.slice(0, this.state.limit).map((proper, index) => {
                console.log("shruthi::proper", proper, index);
                return (
                    <Col lg={6} md={6} sm={12} xs={12}>
                        <Link to="./../PropertyPage/PropertyPage" >
                            <div className="propertyCard cardBottom" id={this.cardData.id} onClick={(cardData) => this.onClickHandler(this.props.cardData)}></div>
                            <div className="Newcard pointer" onClick={() => this.singlepropertypage(proper.propertyId)}>
                                <div className={"propertyCardImg"} src={/*this.cardData.images[0].mobile*/image}></div>
                                <div className="pxc-stopper">
                                    <div className="propertyHeading">{proper.propertyName}
                                        <p ><i className="fas fa-map-marker-alt"></i> {proper.propertyLocation}
                                            <span className="house">{proper.propertyType} for {proper.propertyStatus}</span></p>

                                    </div>
                                </div>

                                <div className="pxc-subcard">
                                    <div className="bhr">
                                        <button className="salebutton">{proper.propertyStatus}</button>
                                        {/* <button className="rentbutton">RENT</button>
                                            <button className="investbutton">INVEST</button> */}
                                    </div>

                                    <p className="Newcard-text" > <i className="fas fa-building"></i> {proper.rooms} BHK</p>
                                    <p className="Newcard-text" ><i className="fas fa-rupee-sign"></i>{proper.displayPrice}</p>
                                </div>
                                {/* <div id="buttonheart"> <i className="far fa-heart"></i></div>  */}
                                <div id="buttonshare">
                                    <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like to Share"></i>

                                </div>
                                <UncontrolledPopover trigger="legacy" placement="bottom" target="buttonshare">
                                    {/* <PopoverHeader>Legacy Trigger</PopoverHeader> */}
                                    <PopoverBody>
                                        <div className="popicon">
                                            <Link><i className='fab fa-facebook-f'></i></Link>

                                            <Link > <i className='fab fa-twitter' ></i></Link>

                                            <Link ><i className='fab fa-instagram' ></i></Link>

                                            <Link ><i className='fab fa-whatsapp' ></i></Link>
                                        </div>
                                    </PopoverBody>
                                </UncontrolledPopover>

                            </div>
                        </Link>

                    </Col>
                )
            });
            
        }
        else{
            
                propertyData.push(<h1>No Property Found</h1>);
         
           
        }

        // console.log("")
        return propertyData;
    }

    singlepropertypage(propertyId){
        // alert(propertyId);
         localStorage.setItem('propertyId',propertyId)

         window.location="./../PropertyPage/PropertyPage";
 }


    render() {
        const { component } = this.state;
        const {statuscomponent} = this.state;
        const {propertytypecomponent}= this.state;
        const {collectioncomponent}= this.state;
        
        //  alert("pp");
        console.log("result");
        console.log(this.resultsubSearch);

        let that = this;
        let featuredpropertyData = this.loadPropertyData();
        // that.state.users && that.state.users.slice(0, this.state.limit).map((proper, index) => {
        //     this.displayData.push(<h1>{proper.propertyName}</h1>);
        // });   
        let collectionData = this.getCollectionIcons();
        let filterData = this.getSearchIcons();
        return (

            <Container className="propertyListPageWrapper">


                <Container className="searchAndSortContainer">

                    <Row>
                        <Col xs={12} sm={4} md={6} lg={6}>
                            <div className="buttonoptions d-flex flex-column">
                                <ButtonGroup size="md" onClick={this.handleButtonClick}>
                                    <Button data-key="sale" className={`buttongrp ${this.propertyStatus === 'sell' ? 'active' :''}`}
                                      onClick={e => this.setComponent('sell')} >Buy</Button>
                                    {/* <Button data-key="buy" className={`buttongrp ${this.propertyStatus === 'buy' ? 'active' :''}`}
                                    onClick={e => this.setComponent('buy')}>BUY</Button> */}
                                    <Button data-key="rent" className={`buttongrp ${this.propertyStatus === 'rent' ? 'active' :''}`}
                                    onClick={e => this.setComponent('rent')}>RENT</Button>
                                    <Button data-key="jiont venture"  className={`buttongrp ${this.propertyStatus === 'jiont venture' ? 'active' :''}`}
                                    onClick={e => this.setComponent('jiont venture')}>Joint venture</Button>
                                    <Button data-key="invest"  className={`buttongrp ${this.propertyStatus === 'invest' ? 'active' :''}`}
                                    onClick={e => this.setComponent('invest')}>Invest</Button>
                                    <Button data-key="lease"  className={`buttongrp ${this.propertyStatus === 'lease' ? 'active' :''}`}
                                    onClick={e => this.setComponent('lease')}>Lease</Button>

                                    {/* <Button onClick={() => this.checkbox(1)} active={this.state.checkbox.indexOf(1) !== -1} className="buttongrp active">BUY</Button>
                                        <Button onClick={() => this.checkbox(2)} active={this.state.checkbox.indexOf(2) !== -1} className="buttongrp active">SALE</Button>
                                        <Button onClick={() => this.checkbox(3)} active={this.state.checkbox.indexOf(3) !== -1} className="buttongrp">RENT</Button>
                                        <Button onClick={() => this.checkbox(4)} active={this.state.checkbox.indexOf(4) !== -1} className="buttongrp">Join venture</Button>
                                        <Button onClick={() => this.checkbox(5)} active={this.state.checkbox.indexOf(5) !== -1} className="buttongrp active">Invest</Button>
                                          */}
                                </ButtonGroup>
                            </div>
                        </Col>
                        <Col xs={12} sm={4} md={2} lg={1}>

                        </Col>
                        <Col xs={12} sm={4} md={4} lg={5}>
                            <ButtonGroup size="md">
                                {/* <Button onClick={() => this.radio(1)} active={this.state.radio === 1} >Relevent</Button>
                                <Button onClick={() => this.radio(3)} active={this.state.radio === 3} className="button rent">Low - High</Button>
                                <Button onClick={() => this.radio(2)} active={this.state.radio === 2} className="button rent">High - Low</Button>*/}
                                <Button  className={`button rent ${this.statusValue == 'relavent' ? 'active' :''}`}
                                      onClick={e => this.setStatusComponent('relavent',1)}>Relavent</Button>
                                <Button  className={`button rent ${this.statusValue === 'low' ? 'active' :''}`}
                                      onClick={e => this. setStatusComponent('low',3)}>Low - High</Button>
                                <Button className={`button rent ${this.statusValue === 'high' ? 'active' :''}`}
                                      onClick={e => this. setStatusComponent('high',2)}>High - Low</Button>

                            </ButtonGroup>
                        </Col>
                    </Row>
                </Container>

                <Container>

                    <Row >
                        <Col xs={12} sm={9} md={5} lg={5} >

                            <div className="budget">
                                <h5 className="h5">Budget</h5>

                                <div data-role="rangeslider" className="rangeSlider">
                                    <Form.Group as={Row} >
                                        <Form.Label column sm="2">
                                            Min
                                        </Form.Label>
                                        <Col sm="10">
                                            <input className="form-control-range inputrange" type="range"
                                                onChange={this.minPriceChangeHandler} min="10000" max="10000000" step="10000" id="formControlRange1" />
                                        </Col>
                                    </Form.Group>
                                    <Form.Group as={Row} >
                                        <Form.Label column sm="2">
                                            Max
                                        </Form.Label>
                                        <Col sm="10">
                                            <input className="form-control-range inputrange" type="range"
                                                onChange={this.maxPriceChangeHandler} min="10000000" max="2000000000" step="100000000" id="formControlRange1" />
                                        </Col>
                                    </Form.Group>

                                </div>
                                <div className="textAndDropDownWrapper">



                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text id="basic-addon1">Rs.</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl
                                            placeholder="Min"
                                            value={this.state.minPrice} />


                                    </InputGroup>
                                    <span className="to">to</span>
                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text id="basic-addon1">Rs.</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl
                                            placeholder="Max"
                                            value={this.state.maxPrice}
                                            aria-describedby="basic-addon2"
                                        />



                                    </InputGroup>


                                </div>
                            </div>
                        </Col>
                        <Col xs={12} sm={9} md={7} lg={7} >
                            <div className="propertyType">
                                <h5 className="h5">Collection</h5>
                                <div className="searchFilterIcons">
                                    {/* <CollectionIcon onChange={this.handlecollectionSelect} /> */}
                                    {collectionData}
                                </div>
                            </div>

                        </Col>

                    </Row>
                </Container>
                <Container >

                    <Row>
                        {/* <Col xs={12} sm={9} md={5} lg={5} >

                            <Container className="placeSearch">
                                <h5 className="h5">Location</h5>
                                <InputGroup className="mb-3"> */}
                        {/* <InputGroup.Prepend>
                                            <Button variant="outline-secondary"><i className="fa fa-search" aria-hidden="true"></i></Button>
                                        </InputGroup.Prepend> */}
                        {/* <FormControl aria-describedby="basic-addon1" placeholder="enter the location" type="text" value={this.state.propertyPlace1} onChange={this.handleSubLocationChange} />
                                    <InputGroup.Append>

                                        <Button variant="outline-secondary"><i className="fa fa-crosshairs"></i></Button>
                                        <Button variant="outline-secondary" onClick={() => this.searchSubLocation()} ><i className="fa fa-search" aria-hidden="true"></i></Button>
                                    </InputGroup.Append>
                                </InputGroup>

                                <div className="searchButtonss">
                                    {this.resultsubSearch}
                                </div>
                            </Container>
                        </Col> */}
                        <Col xs={12} sm={12} md={12} lg={12} >
                            <div className="propertyType">
                                <h5 className="h5"> Property Type </h5>
                                <Col xs={12} sm={12} md={12} lg={12} >
                                    <div className="searchFilterIcons">

                                        {/* <SearchFilterIcon
                                            onChange={this.handlepropertyTypeSelect}

                                        /> */}
                                        {filterData}

                                    </div>
                                </Col>
                            </div>


                        </Col>
                    </Row>
                </Container>
                <Container >
                    <Row >
                        <Col xs={12} sm={9} md={9} lg={12}>
                            <InputGroup className=" searchTextPropertyListPage mb-3" size="lg">
                                {/* <InputGroup.Prepend>
                                    <Button variant="outline"><i className="fa fa-search" aria-hidden="true"></i></Button>
                                </InputGroup.Prepend> */}
                                <FormControl aria-describedby="basic-addon1" placeholder="Bangalore" type="text" value={this.state.propertyPlace} onChange={this.handleLocationChange} />
                                <InputGroup.Append>

                                    <Button variant="outline"><i className="fa fa-crosshairs"></i></Button>
                                    <Button variant="outline" onClick={() => this.searchSubLocation()}><i className="fa fa-search" aria-hidden="true"></i></Button>
                                </InputGroup.Append>
                            </InputGroup>
                            <div className="searchButtonss">
                                {this.resultsubSearch}
                            </div>
                        </Col>


                    </Row>
                </Container>



                {/* {*<SearchResult /> *}  */}
                <Container className="propertyCardContainer">
                    <h2 className="text-center">Search Results</h2>
                    <CardDeck>
                        <div id="cardContainer" >
                            {/* <PropertyCard cardData={this.props.cardData} onClickHandler={this.props.clickHandler}></PropertyCard>
                                 */}
                            {featuredpropertyData}
                            {/* {this.getCardData()} */}
                            {/* <PropertyCard  /> */}
                        </div>
                    </CardDeck>
                </Container>
                <div className="containnerWrapper contactUs">
                    <ContactUs />
                </div>


            </Container>
        )
    }
}



