import React, { Component, useState, useCallback } from 'react'
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Container, Row, Col, Image } from 'react-bootstrap';
import "../../css/Blog.css";
import subBlog from "../../images/subBlog.jpg";
import person from "../../images/person.png";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import { Blogphotos } from "./BlogPhotos";



export default class SubMainBlog extends Component {
    state = {}
    render() {
        return (

            <Router>
                <Container className="BlogPageWrapper">
                    <div className="heading">
                        <ul>
                            <li>Blog</li>
                            <i className='fas fa-angle-right'></i>
                            <li>Luxury neighborhoods for you</li>


                        </ul>
                    </div>
                    <Row>
                        <Col xs={12} sm={9} md={2} lg={2}></Col>
                        <Col xs={12} sm={9} md={8} lg={8}>
                            <div className="subheading">Lorem ipsum dolor sit amet</div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                 et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                 aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
                            </p>
                        </Col>
                        <Col xs={12} sm={9} md={2} lg={2}></Col>
                    </Row>
                    <Image src={subBlog} fluid />
                    <Row>
                        <Col xs={12} sm={9} md={2} lg={2}></Col>
                        <Col xs={12} sm={9} md={8} lg={8}>
                            <p>
                                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                                sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </p>
                            <div className="subheading">Sed ut perspiciatis unde omnis iste</div>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                                dicta sunt explicabo.
                             Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                        </Col>
                        <Col xs={12} sm={9} md={2} lg={2}></Col>
                    </Row>
                    <BlogPhotoGallery />
                    <Row>
                        <Col xs={12} sm={9} md={2} lg={2}></Col>
                        <Col xs={12} sm={9} md={8} lg={8}>

                            <div className="subheading">Duis aute irure dolor in reprehenderit</div>
                            <p>
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                Excepteur sint occaecat cupidatat non proident,
                                sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <div className="italicword">
                                “At vero eos et accusamus et iusto odio dignissimos ducimus
                                qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias”
                            </div>
                            <div className="boldword">
                                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                                 sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </div>
                            <div className="boldword">
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus
                                 id quod maxime placeat facere possimus,
                                 omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis.
                            </div>
                        </Col>
                        <Col xs={12} sm={9} md={2} lg={2}></Col>
                    </Row>

                    <BlogreviewCard />
                </Container>

            </Router >

        );
    }
}

function BlogPhotoGallery() {
    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);

    const openLightbox = useCallback((event, { photo, index }) => {
        setCurrentImage(index);
        setViewerIsOpen(true);
    }, []);

    const closeLightbox = () => {
        setCurrentImage(0);
        setViewerIsOpen(false);
    };

    return (
        <div>

            <Gallery photos={Blogphotos} onClick={openLightbox} />
            <ModalGateway>
                {viewerIsOpen ? (
                    <Modal onClose={closeLightbox}>
                        <Carousel
                            currentIndex={currentImage}
                            views={Blogphotos.map(x => ({
                                ...x,
                                srcset: x.srcSet,
                                caption: x.title
                            }))}
                        />
                    </Modal>
                ) : null}
            </ModalGateway>
        </div>
    );
}
function BlogreviewCard() {
    return (
        <Container className="BlogreviewCardContainer">

            <Row className=" BlogreviewCard">
                <Col xs={12} sm={9} md={2} lg={2}></Col>
                <Col xs={12} sm={9} md={8} lg={8}>
                    <Row>

                        <Col xs={12} sm={3} md={3} lg={3}>
                            <Image src={person} roundedCircle fluid />
                        </Col>
                        <Col Col xs={12} sm={9} md={9} lg={9}>
                            <div className="BlogShareBtn"><i className='fa fa-share-alt ' ></i></div>
                            <h3>Matthew Schwartz</h3>
                            <p>
                                Nam libero tempore, cum soluta nobis est eligendi optio cumque
                               nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est.
                            </p>
                        </Col>

                    </Row>
                    <Row>
                        <Col>
                            <Link to="/">MORE FROM AUTHOR</Link>

                        </Col>
                        <Col>
                            <hr />

                        </Col>
                        <Col>
                            <Link> <i className='fas fa-angle-left'></i>   older </Link>

                            <Link> newer <i className='fas fa-angle-right'></i> </Link>
                        </Col>
                    </Row>
                </Col>
                <Col xs={12} sm={9} md={2} lg={2}></Col>

            </Row>


        </Container>
    );
}


