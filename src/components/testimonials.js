import React, { useState } from "react";
import { Container,Row,Col} from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";
import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import Modal from 'react-bootstrap/Modal';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import '../css/testimonials.css';
import PropertyImage from "./PropertyPage/PropertyImage";
import customerImg from "../images/customer1.png"
import customerImg1 from "../images/customer2.png"
import { UncontrolledPopover, PopoverBody } from 'reactstrap';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";



  class HappyTestimonials extends React.Component {
      state = {  }
     
      componentDidMount() {
       // alert("sasdd");
        fetch('https://aceimagingandvideo.com/beegru/website/fetchIndexInfo', {
                method: 'POST',
                async: false,
                headers: {
                        // "Authorization": "Basic " + btoa("8970685540"+ ":" +"123"),
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                        "mobileNumber": localStorage.getItem("mobileNumber")
                })
            }).then((response) => response.json())
                .then((responseJson) => {
                        
                        this.setState({
                                users: responseJson
                        });
                })
                .catch((error) => {
                        console.error(error);
                });
        }


        loadTestimonialData() {
          let that = this;
          let testimonialData = [];
          let ratings = ''
        
          if (that.state.users && that.state.users.testimonialsList && that.state.users.testimonialsList.length) {  
              testimonialData = that.state.users && that.state.users.testimonialsList.map((proper, index) => {
                    if(proper.ratings == 1){
                        ratings = <div><i className='fa fa-star'></i></div>
                    } else if(proper.ratings == 2){
                      ratings =  <div><i className='fa fa-star'></i><i className='fa fa-star'></i></div>
                    }else if(proper.ratings == 3){
                      ratings =  <div><i className='fa fa-star'></i><i className='fa fa-star'></i><i className='fa fa-star'></i></div>
                    }else if(proper.ratings == 4){
                      ratings =  <div><i className='fa fa-star'></i><i className='fa fa-star'></i><i className='fa fa-star'></i><i className='fa fa-star'></i></div>
                    }else if(proper.ratings == 5){
                      ratings =  <div><i className='fa fa-star'></i><i className='fa fa-star'></i><i className='fa fa-star'></i><i className='fa fa-star'></i><i className='fa fa-star'></i></div>
                    }
                        
                          return (
                            <CardDeck>
                            <Card>
                                <Card.Header>
                                  <Row>
                                    <Col xs="8">
                                   {ratings}
  
                                    </Col>
                                    <Col xs="4" className="date">
                                      {proper.date}
                                    </Col>
                                  </Row>
                                </Card.Header>
  
                                <Card.Body>
                          <Card.Title>{proper.subject}</Card.Title>
      
                          <Card.Text>{proper.description}</Card.Text>
                                     
                                </Card.Body>
                                <Card.Footer>
                                  <Row>
                                    <Col xs="3">
                                      
                                    <img src={customerImg1} className="testimonial-image" ></img>
                                    </Col>  
                                    <Col xs="4">
                                      <p>{proper.testimonialName}</p>
                                      <p className="p1">{proper.testimonialAddress}</p>
                                    </Col>
                                    <Col xs="1">
                                    <i className='fab fa-youtube' ></i>
  
                                    </Col>
                                    <Col xs="1">
                                      <i className='fa fa-heart' ></i>
                                      </Col>
                                    <Col xs="1">
                                      <div id="share">
                                        <i className="fas fa-share-alt" data-toggle="tooltip" data-placement="top" title="I like to Share"></i>

                                      </div>
                                      <UncontrolledPopover trigger="legacy" placement="bottom" target="share">
                  {/* <PopoverHeader>Legacy Trigger</PopoverHeader> */}
                  <PopoverBody>
                    <div className="popicon">
                      <Link><i className='fab fa-facebook-f'></i></Link>

                      <Link > <i className='fab fa-twitter' ></i></Link>

                      <Link ><i className='fab fa-instagram' ></i></Link>

                      <Link ><i className='fab fa-whatsapp' ></i></Link>
                    </div>
                  </PopoverBody>
                </UncontrolledPopover>
                                    </Col>
  
  
                                  </Row>
                               
                                 
                                </Card.Footer>
                                
                            </Card>
                          </CardDeck>
                          )
                  });
          }
          return testimonialData;
  }

      render() { 
        let testimonialData = this.loadTestimonialData();
        const responsive = {
          superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 5,
          },
          desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 1.5,
          },
          tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1,
          },
          mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
          },
        };

       

          return (  
            <Container fluid className="testimonialcontainer">
              
                <Row>
                    <Col md="6">
                      
                      <div className="testimonialheading">
                        <p> Happy People’s</p>
                       Testimonials
                        
                      </div>
                    </Col>

                    <Col md="6">
                    <Carousel responsive={responsive}>
                      
                     
                     {testimonialData}
                      
                    </Carousel>
                    </Col>
                   
            </Row>
          </Container>
          );
      }
  }
 export default HappyTestimonials;


 function Testimonialcard(){
  const [show, setShow] = useState(false);
   return(
        <CardDeck>
          <Card onClick={() => setShow(true)}>
            <Card.Header>
              <Row>
                <Col xs="8">
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>
                  <i className='fa fa-star' ></i>

                </Col>
                <Col xs="4" className="date">
                  May 10th
                </Col>
               </Row>
            </Card.Header>
            <Card.Body>
              <Card.Title>Awesome Experience!</Card.Title>
              <Card.Text>We enjoyed our Israel trip immensely. The hotels were wonderful…we really
                  enjoyed Meir, our tour guide. He was so knowledgeable. His experience as a soldier
                  and commander in the ‘67 war and the Yom Kippur War made us feel like we were seeing 
                  the country through the eyes of a 
                  person that was instrumental in its formation and growth. A true Sabra! </Card.Text>
            </Card.Body>
            <Card.Footer>
              <Row>
                <Col xs="2">
                <img src={customerImg}  className="testimonial-image"></img>
                </Col>  
                <Col xs="6">
                  <p>Sofie Olsen</p>
                  <p className="p1">Shriram Value Homes At Divin</p>
                </Col>
                <Col xs="2">
                                  <i className='fa fa-youtube-play' ></i>

                                  </Col>
                <Col xs="2">
                  <i className='far fa-heart' ></i>
                  <i className='fa fa-share-alt' ></i>
                </Col>
              </Row>
            </Card.Footer>
          </Card>
          <Modal 
          show={show}
          size="lg"
          onHide={() => setShow(false)} 
          >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
                    
                         Happy People’s
                       Testimonials
                        
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
             <Col md="7">
             <PropertyImage />
               
             </Col>

             <Col md="5" >
             <Row>
                                  <Col xs="8">
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>
                                  <i className='fa fa-star' ></i>

                                  </Col>
                                  <Col xs="4" className="date">
                                     Sept 14th
                                  </Col>
                                </Row>
                                <Row>
                                <Card.Body>
              <Card.Title>Awesome Experience!</Card.Title>
              <Card.Text>We enjoyed our Israel trip immensely. The hotels were wonderful…we really
                  enjoyed Meir, our tour guide. He was so knowledgeable. His experience as a soldier
                  and commander in the ‘67 war and the Yom Kippur War made us feel like we were seeing 
                  the country through the eyes of a 
                  person that was instrumental in its formation and growth. A true Sabra! </Card.Text>
            </Card.Body>
                                </Row>
                                <Row>
                                  <Col xs="2">
                                    <img src={customerImg} className="testimonial-image"></img>
                                  </Col>  
                                  <Col xs="6">
                                     <p>Maja Christiansen</p>
                                     <p className="p1">Shriram Value Homes At Divin..</p>
                                  </Col>
                                  <Col xs="2">
                                  <i className='fa fa-youtube-play' ></i>

                                  </Col>
                                  <Col xs="2">
                                    <i className='far fa-heart' ></i>
                                    <i className='fa fa-share-alt' ></i>
                                  </Col>


                                </Row>
             </Col>

          </Row>

        </Modal.Body>
      </Modal>
      
        </CardDeck>
       
        
   );

 }

 