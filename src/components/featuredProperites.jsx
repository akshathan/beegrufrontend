import React, { Component } from "react";
import data from "../SampleData/data.json";
import CardDeckContainer from "./coreComponents/CardDeckContainer";
import "../css/featuredProperites.css";
//import ActionButton from "./coreComponents/ActionButton.js";

export default class FeaturedProperties extends Component {
  constructor(props) {
    super(props);
    this.data = data ? data.featuredProperties : {};

    this.onClickHandler = this.onClickHandler.bind(this);
  }
  onClickHandler = cardData => {
    // this.props.onClickHandler(cardData);
  };

  render() {
    return (
      <div id="featuredProperties">
        {/* <div className="propertysubheading">WE GOT SOME OF THE BEST</div> */}
        <div className="propertyheading">Featured Properties</div>
        <CardDeckContainer
          cardData={this.data}
          onClickHandler={() => this.onClickHandler()}
        />
        {/* <ActionButton viewAll={this.data.viewAll} /> */}
      </div>
    );
  }
}

