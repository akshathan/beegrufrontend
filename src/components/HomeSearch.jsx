import React from "react";
import { Redirect } from 'react-router-dom';
import { BrowserRouter as Router } from "react-router-dom";
import {Link} from 'react-router-dom';
import { Button, Row, Col, Container } from "react-bootstrap";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import "../css/HomeSearch.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import { geolocated } from "react-geolocated";
import locationButton from "./locationButton";
import PropTypes from "prop-types";
import PropertyList from "../components/Search/Search";

class HomeSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      component: 'buy',
      filteredOptions : [],
      propertystatus: ''
    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  static propTypes = {
    options: PropTypes.instanceOf(Array).isRequired,
  };
  state = {
    activeOption: 0,
    filteredOptions: [],
    showOptions: false,
    userInput: "",
    redirect: false,
  };

  setComponent(component) {
    this.setState({
      component
    });
  }

  handleButtonClick = (event) => {
    this.setState(
      {
        propertystatus: event.target.attributes.getNamedItem("data-key").value,
      },
      () => console.log("propertyType", this.state.propertystatus)
    );
  };
  handleClearForm(event) {
    event.preventDefault();
    this.setState({
      propertyStatus: "",
      userInput:'',
    });
  }

  display = (data) => {
    this.setState({ filteredOptions: data });
  //  alert(this.state.filteredOptions.length);
  }
  onChange = (e) => {
  //  alert("Inside OnChange");
  const { options } = '';
  const userInput = e.currentTarget.value;
 
  const filteredOptions = '';

  this.setState({
    activeOption: 0,
    filteredOptions,
    showOptions: true,
    userInput: e.currentTarget.value,
  });
   // alert(userInput);
   if(userInput.length > 0){
    fetch('https://www.aceimagingandvideo.com/beegru/website/fetchLocation', {
      method: 'POST',
      async: false,
      headers: {
              //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
              Accept: 'application/json',
              'Content-Type': 'application/json',
      },
      body: JSON.stringify({                  
              "location": userInput                 
      })
  }).then((response) => response.json())
  .then(this.display)
  .catch()
   }
   
  }
  onClick = (e) => {
  //  alert("onClick");
    this.setState({
      activeOption: 0,
      filteredOptions: [],
      showOptions: false,
      userInput: e.currentTarget.innerText,
    });
  //  alert("userInput2", e.currentTarget.innerText);
  };
  onKeyDown = (e) => {
    const { activeOption, filteredOptions } = this.state;
    console.log("on key down");
    if (e.keyCode === 13) {
      this.setState({
        activeOption: 0,
        showOptions: false,
        userInput: filteredOptions[activeOption],
       
      });
     
    } else if (e.keyCode === 38) {
      if (activeOption === 0) {
        return;
      }
      this.setState({ activeOption: activeOption - 1 });
    } else if (e.keyCode === 40) {
      if (activeOption === filteredOptions.length - 1) {
        console.log(activeOption);
        return;
      }
      this.setState({ activeOption: activeOption + 1 });
    }
  };
  handleFormSubmit(e) {
    //alert(e.target.innerText);
    e.preventDefault();
    const formPayload = {
      propertyStatus: this.state.propertystatus,
      userInput: e.currentTarget.value,
    };
    if(formPayload !== undefined){
      return  <Redirect to="./../PropertyPage/PropertyPage" />
    }
    console.log("Send this in a POST request:", formPayload);
    this.handleClearForm(e);
    
  }
  
  onLoadMore = () => {
    //alert(this.state.userInput);
    localStorage.removeItem("propertyPlace");
    localStorage.removeItem("propertyStatus");
    localStorage.setItem("searchStatus","1");
    localStorage.setItem("propertyPlace", this.state.userInput);
    localStorage.setItem("propertyStatus",this.state.propertystatus);
    this.setState({redirect:true});
 }
  
  render() {
  //  alert("Inside Render "+this.state.filteredOptions.length);
    if(this.state.redirect){
      return <Redirect push to="/propertyList"/>;
    }
    const { component } = this.state;
    const {
      onChange,
      onClick,
      onKeyDown,

      state: { activeOption, showOptions, userInput },
    } = this;
    let optionList;
    if(this.state.filteredOptions.length > 0){
    //  alert("Inside if");  
      optionList = (
          <ul className="options">
            {this.state.filteredOptions.map((optionName, index) => {
              let className;
              if (index === activeOption) {
                className = "option-active";
              }
              return (
                <li className={className} key={optionName} onClick={onClick}>
                  {optionName}
                </li>
              );
            })}
          </ul>
        );
      }
    return (
      <Router>
        <React.Fragment>
          <form onSubmit={this.handleFormSubmit}>
            <div className="Homefrantpage">
              <Row className="buttonContainer nomargin">
                <Col>
                  {" "}
                  <ButtonGroup
                    className="HomepageButtons "
                    onClick={this.handleButtonClick}
                  >
                    <button
                      className={`BuyButton buttonHomepage ${component === 'sell' ? 'active' :''}`}
                      data-key="sell"
                      onClick={e => this.setComponent('sell')}>
                      
                    
                      Buy
                    </button>
                    {/* <div className="divider"></div> */}
                    <button
                      className={`RentButton buttonHomepage ${component === 'rent' ? 'active' :''}`}
                      data-key="rent"
                      onClick={e => this.setComponent('rent')}>
                   
                      Rent
                    </button>
                    <button
                      className={`LeaseButton buttonHomepage ${component === 'lease' ? 'active' :''}`}
                      data-key="lease"
                      onClick={e => this.setComponent('lease')}>
                   
                      Lease
                    </button>
                    <button
                      className={`InvestButton buttonHomepage ${component === 'invest' ? 'active' :''}`}
                      data-key="invest"
                      onClick={e => this.setComponent('invest')}>
                   
                      Invest
                    </button>
                    <button
                      className={`InvestButton buttonHomepage ${component === 'jiont venture' ? 'active' :''}`}
                      data-key="joint venture"
                      onClick={e => this.setComponent('joint venture')}>
                    
                      Joint Venture
                    </button>
                  </ButtonGroup>
                </Col>
              </Row>

              <Container>
                <InputGroup size="lg" className="mb-3">
                  <FormControl
                    type="search"
                    name="search"
                    autoComplete="off"
                    placeholder="Enter Place,Builder, Project..."
                    aria-label="Enter Place,Builder, Project..."
                    aria-describedby="basic-addon2"
                    onChange={onChange}
                    onKeyDown={onKeyDown}
                    value={userInput}
                  />

                  <InputGroup.Append>
                    <InputGroup.Text>
                      <i
                        className="fa fa-crosshairs"
                        onClick={locationButton}
                      ></i>
                    </InputGroup.Text>
                    <Button
                      variant="danger"
                      value="search"
                      className="searchButton1"
                      type="submit" 
                      onClick={this.onLoadMore}
                    >
                      search
                    </Button>
                  </InputGroup.Append>
                  
                </InputGroup>
                <div className="optiondiv">{optionList}</div>
              </Container>
            </div>
          </form>
         
        </React.Fragment>
      </Router>
    );
  }
}
export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(HomeSearch);

// function HomepageButtons() {
//   return (
//     <ButtonGroup className="HomepageButtons " onClick={this.handleButtonClick}>
//       <button className="BuyButton buttonHomepage" active>
//         Buy
//       </button>
//       {/* <div className="divider"></div> */}
//       <button className="RentButton buttonHomepage">Rent</button>
//       <button className="LeaseButton buttonHomepage">Lease</button>
//       <button className="InvestButton buttonHomepage">Invest</button>
//       <button className="InvestButton buttonHomepage">Joint Venture</button>
//     </ButtonGroup>
//   );
// }

