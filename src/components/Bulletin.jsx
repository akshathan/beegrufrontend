import React, { Component } from "react";
import { Container } from "react-bootstrap";
import MainBlog from "./Blog/mainBlog";
import SubMainBlog from "./Blog/submainBlog";

class Bulletin extends Component {
  state = {};
  render() {
    return (
      <Container>
            <MainBlog />
            <SubMainBlog />
          
        
      </Container>
    );
  }
}

export default Bulletin;
