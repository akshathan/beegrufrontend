import React, { Component } from 'react'
import { BrowserRouter as Router } from "react-router-dom";
import {Form, Col , Row , Button, Container}  from 'react-bootstrap';
import '../css/properties.css';
 
class Properties extends Component {
  state = {  }
  render() { 
    return ( 
      <Router>
        <Container>
          <div className="heading"> List Your Property</div>
          <Form>
              <Form.Group as={Row} controlId="formHorizontalRequirement">
                <Form.Label className="labels" column sm={2}>
                   I Want to :
                </Form.Label>
                <Col sm={4}>
                  <Form.Control as="select">
                    <option>SELECT</option>
                    <option>SELL</option>
                    <option>RENT</option>
                    <option>FIND JOINT VENTURE PARTNER-INVESTOR</option>
                 </Form.Control>
                </Col>
              </Form.Group>

            <Form.Group as={Row} controlId="formPropertyType">
              <Form.Label className="labels" column sm={2} >
                 Type of Property : 
              </Form.Label>
              <Col sm={12}>
                {['Checkbox'].map(type => (
                <div key={`inline-${type}`} className="mb-3">
                  <Form.Check inline label="Residential" type={type} id={`inline-${type}-1`} />
                  <Form.Check inline label="Commercial" type={type} id={`inline-${type}-2`} />
                  <Form.Check inline label="Industrial" type={type} id={`inline-${type}-3`} />
                  <Form.Check inline label="Ware housing " type={type} id={`inline-${type}-4`} />
                  <Form.Check inline label="Agricultural (disabled)" type={type} id={`inline-${type}-5`} />
                </div>
                ))}
              </Col>
            </Form.Group>
           
            <Form.Group as={Row} controlId="formLocation">
              <Form.Label className="labels" column sm={2} >
                 Location : 
              </Form.Label>
              <Col sm={4}>
                <Form.Control type="text" placeholder="Pin Code" required/>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="formLocationDescription">
              <Form.Label className="labels" column sm={2} >
                 Landmarks : 
              </Form.Label>
              <Col sm={4}>
                <Form.Control as="textarea" type="textarea" placeholder="Description of getting to location from Landmark " required />
              </Col> 
            </Form.Group>
            
            <Form.Group as={Row} controlId="formuploadImage">
              <Form.Label className="labels" column sm={2} >
                 Upload Image : 
              </Form.Label>
              <Col sm={4}>
                <Form.Control type="file" placeholder="image upload" required/>
              </Col>
            </Form.Group>

            <Form.Group as={Row} controlId="formuploadVideo">
              <Form.Label className="labels" column sm={2} >
                 Upload Video :
              </Form.Label>
              <Col sm={4}>
                <Form.Control type="file" placeholder="video upload"/>
              </Col>
            </Form.Group>
  
 

            <Button className="started" variant="primary" type="submit">
                Let's get Started!
            </Button>
           </Form>
      </Container>
      </Router>
     );
  }
}
 
export default Properties;
