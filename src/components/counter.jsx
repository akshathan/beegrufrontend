import React, { Component } from 'react'
class Counter extends Component { 
    
    x = 5;
    x
    state= {
         count: 0,
        tags: ['shruthi','swathi','akshatha'],
        
    };


   handleIncrement = () => {
       this.state.count++;
       this.setState({count : this.state.count+1})
   };

    render() { 

        return (

        <div>

            <span className={this.getbadgeclasses()}>{this.formatCount()}</span>
            <button className='btn btn-secondary btn-sm ' onClick={this.handleIncrement}>increment</button>
           
            <ul>
                {this.state.tags.map(tag => <li key={tag}>{tag}</li>)}
            </ul>

        </div>
        );
    }
     
    getbadgeclasses() {
        let classes = "badge m-2 badge-";
        classes += (this.state.count === 0) ? "danger" : "primary";
        return classes;
    }

    formatCount(){
        const { count }=this.state;
        return count === 0 ? "Zero" : count;
    }
}
 
export default Counter ;
