import React, { Component } from "react";
import ScrollToTop from "react-scroll-up";
import "../css/Navbar.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./home.jsx";
import Bulletin from "./Bulletin.jsx";
import Services from "./services";
import About from "./about.jsx";
import Login from "../components/models/Login.js";
import PropertyPage from "../components/PropertyPage/PropertyPage";
import PropertyList from "../components/Search/Search";
import logo from "../images/logo.png"; // Tell Webpack this JS file uses this image
import { Navbar, Nav } from "react-bootstrap";
import {
  
  Button,
  
} from "react-bootstrap";

export default class Beegrunav extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
      this.toggleNavbar = this.toggleNavbar.bind(this);
      this.state = {
        collapsed: true
      };
    }
    toggleNavbar() {
      this.setState({
        collapsed: !this.state.collapsed
      });
    }
  
    render() {
      var status = localStorage.getItem('loginStatus');
  
      function logoutMethod() {
        localStorage. removeItem('loginStatus');
        window.location.reload();
      }
  
      let modalClose = () =>
        this.setState({
          modalShow1: false
        });
      
      const collapsed = this.state.collapsed;
      const classOne = collapsed
        ? "collapse navbar-collapse"
        : "collapse navbar-collapse show";
      const classTwo = collapsed
        ? "navbar-toggler navbar-toggler-right collapsed"
        : "navbar-toggler navbar-toggler-right";
      if(status != "loginStatus"){
      return (
        <Router>        
          <Navbar collapseOnSelect expand="lg" variant="inverse">
            <Navbar.Brand href="#home">
              {" "}
              <img src={logo} alt="Logo" />
            </Navbar.Brand>
            <Navbar.Toggle
              aria-controls="responsive-navbar-nav"
              className="pull-left"
            />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="ml-auto nav1 ">
                <Nav.Link eventKey="1" as={Link} to="/home">
                  HomePage
                </Nav.Link>
                {/* <Nav.Link eventKey="2" as={Link} to="/propertyPage">
                  Property
                </Nav.Link> */}
                <Nav.Link eventKey="3" as={Link} to="/services">
                  SERVICES
                </Nav.Link>
                <Nav.Link eventKey="4" as={Link} to="/propertyList">
                  Property List
                </Nav.Link>
                <Nav.Link eventKey="5" as={Link} to="/about">
                  About Us
                </Nav.Link>
                <Nav.Link eventKey="6" as={Link} to="/bulletin">
                  Bulletin
                </Nav.Link>
              
              <Nav.Link eventKey="7"  to=""
               
                onClick={() => this.setState({ modalShow1: true })}>
                  Login
                </Nav.Link>
                <Login show={this.state.modalShow1} onHide={modalClose} />
              </Nav>
            </Navbar.Collapse>          
          </Navbar>
  
          <Route exact path="/" component={Home} />
          <Route exact path="/home" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/propertyPage" component={PropertyPage} />
          <Route path="/propertyList" component={PropertyList} />
  
          <Route path="/services" component={Services} />
          <Route path="/bulletin" component={Bulletin} />        
        </Router>            
      );
    }else{
      return (
        <Router>        
          <Navbar collapseOnSelect expand="lg" variant="inverse">
            <Navbar.Brand href="#home">
              {" "}
              <img src={logo} alt="Logo" />
            </Navbar.Brand>
            <Navbar.Toggle
              aria-controls="responsive-navbar-nav"
              className="pull-left"
            />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="ml-auto nav1 ">
                <Nav.Link eventKey="1" as={Link} to="/home">
                  HomePage
                </Nav.Link>
                {/* <Nav.Link eventKey="2" as={Link} to="/propertyPage">
                  Property
                </Nav.Link> */}
                <Nav.Link eventKey="3" as={Link} to="/services">
                  SERVICES
                </Nav.Link>
                <Nav.Link eventKey="4" as={Link} to="/propertyList">
                  Property List
                </Nav.Link>
                <Nav.Link eventKey="5" as={Link} to="/about">
                  About Us
                </Nav.Link>
                <Nav.Link eventKey="6" as={Link} to="/bulletin">
                  Bulletin
                </Nav.Link>
                <Nav.Link eventKey="7" as={Link} to="/account">
                  My Account
                </Nav.Link>
                                       
              </Nav>
            </Navbar.Collapse> 
            <Button
                        type="submit"                      
                        onClick={logoutMethod}                      
                      >
                        Logout
                      </Button>         
          </Navbar>
  
          <Route exact path="/" component={Home} />
          <Route exact path="/home" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/propertyPage" component={PropertyPage} />
          <Route path="/propertyList" component={PropertyList} />
  
          <Route path="/services" component={Services} />
          <Route path="/bulletin" component={Bulletin} /> 
          {/* <Route path='/account' component={() => { 
            window.location.href = 'https://localhost:3001'; 
            return null;
        }}/>        */}
        </Router>            
      );
    }
  }
  }
  