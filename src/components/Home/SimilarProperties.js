import React, { Component } from 'react';
import SimilarCardDeckContainer from "../coreComponents/SimilarCardDeckContainer";
import data from "../../SampleData/newdata.json";
// import ActionButton from "../coreComponents/ActionButton";
// import PropertyPage from '../PropertyPage/PropertyPage';


export default class SimilarProperties extends Component {
        constructor(props){
                super(props);
                this.data =  data ? data.similarProperties : {};
                this.onClickHandler = this.onClickHandler.bind(this);

        }
        onClickHandler(cardData){
               // this.props.onClickHandler(cardData);
                 // return  <PropertyPage data={this.data}/>
        }
        //  
        render() {
                return (
                        <div className="newProperties"> 
                                <div className="newPropSelect">
                                       New Properties
                                        {/* in <span className = "propertyCitySearch" >{this.data.city ? this.data.city : "Bangalore"}</span> */}
                                </div>
                                <SimilarCardDeckContainer  cardData= {this.data} onClickHandler= {() => this.props.onClickHandler()}/>
                                
                        </div>
                )
        }
}


