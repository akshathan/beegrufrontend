import React, { Component } from 'react'
import { Container, Row, Col, Form } from 'react-bootstrap';

export default class ContactUs extends Component {

        constructor() {
                super();
                this.handleSubmit = this.handleSubmit.bind(this);
        }

        handleSubmit(e) {
                e.preventDefault();
                //alert(document.contactUs.email.value);
                const form = e.currentTarget;
                if (form.checkValidity() === false) {
                        e.preventDefault();
                        e.stopPropagation();
                }
                this.setState({ validated: true });
                e.preventDefault();                
                console.log(document.contactUs.message.value);
                
                fetch('https://aceimagingandvideo.com/beegru/website/saveContactUs', {
                        method: 'POST',
                        async: false,
                        headers: {
                                //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                "name": document.contactUs.fullName.value,
                                "mobileNumber": document.contactUs.phoneNumber.value,
                                "emailId": document.contactUs.email.value,
                                "location": document.contactUs.location.value,
                                "message": document.contactUs.message.value
                        })
                }).then((response) => response.json())
                        .then((responseJson) => {
                                console.log(responseJson);
                                if(responseJson.resultStatus == 0 && responseJson.httpStatus == 200){
                                       alert("Request Sent Successfully");
                                       document.getElementById("contactusForm").reset();
                                } else{
                                        alert("Error");
                                      }     
                        })
                        .catch((error) => {
                                console.error(error);
                        });
        }

        render() {
                return (

                        <div >
                                <Container id="contactUs">
                                        <h6>Contact Us</h6>
                                        <Row>
                                                <Col md="8">
                                                        <Form onSubmit={this.handleSubmit} name="contactUs" id="contactusForm">
                                                                <Form.Row>
                                                                        <Form.Group as={Col} controlId="formFullName">
                                                                                <Form.Label className="textBoxHeading">FULL NAME *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="text" placeholder="Full Name" id="fullName" />
                                                                        </Form.Group>
                                                                        <Form.Group as={Col} controlId="formEmail">
                                                                                <Form.Label className="textBoxHeading">EMAIL *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="email" placeholder="Email" id="email"  />
                                                                        </Form.Group>

                                                                </Form.Row>
                                                                <Form.Row>
                                                                        <Form.Group as={Col} controlId="formPhoneNumber">
                                                                                <Form.Label className="textBoxHeading">PHONE NUMBER *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="text" placeholder="Enter Phone Number" id="phoneNumber"  />

                                                                        </Form.Group>

                                                                        <Form.Group as={Col} controlId="formLocation">
                                                                                <Form.Label className="textBoxHeading">LOCATION *</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="text" placeholder="Location" id="location" />
                                                                        </Form.Group>

                                                                </Form.Row>
                                                                <Form.Row>
                                                                        <Form.Group as={Col} controlId="formMessage">

                                                                                <Form.Label className="textBoxHeading">MESSAGE</Form.Label>
                                                                                <Form.Control className="headingTextBox" type="textarea" as="textarea" placeholder="Message" id="message" />

                                                                        </Form.Group>
                                                                        <Form.Group as={Col} controlId="formMessage">

                                                                        </Form.Group>
                                                                </Form.Row>

                                                                <button className="button submitText">Submit</button>
                                                        </Form>
                                                </Col>
                                                <Col md={1}>
                                                        <div className="divider">

                                                        </div>
                                                </Col>
                                                <Col md={3}>
                                                        <div className="submitFormImage">

                                                        </div>
                                                </Col>
                                        </Row>
                                </Container>


                        </div>

                )
        }
}




