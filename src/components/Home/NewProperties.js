import React, { Component } from 'react';
import NewCardDeckContainer from "../coreComponents/NewCardDeckContainer";
import data from "../../SampleData/data.json";
import ActionButton from "../coreComponents/ActionButton";
//import PropertyPage from '../PropertyPage/PropertyPage';


export default class NewProperties extends Component {
        constructor(props){
                super(props);
                this.data =  data ? data.properties : {};
                this.onClickHandler = this.onClickHandler.bind(this);

        }
        onClickHandler(cardData){
               // this.props.onClickHandler(cardData);
                 // return  <PropertyPage data={this.data}/>
        }
        //  
        render() {
                return (
                        <div className="newProperties"> 
                                <div className="newPropSelect">
                                       Similar Properties
                                        {/* in <span className = "propertyCitySearch" >{this.data.city ? this.data.city : "Bangalore"}</span> */}
                                </div>
                                <NewCardDeckContainer  cardData= {this.data} onClickHandler= {() => this.props.onClickHandler()}/>
                                {/* <ActionButton viewAll = {this.data.viewAll} /> */}
                        </div>
                )
        }
}


