import React, { Component } from 'react';
// import Carousel from 'react-bootstrap/Carousel';
import '../../css/aboutUs.css';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar'
import { Container, Row, Col } from 'react-bootstrap';
import Badge from 'react-bootstrap/Badge';
import GalleryImageComponent from '../Gallery/GalleryImageComponent';
import Accordion from 'react-bootstrap/Accordion';
import "video-react/dist/video-react.css";
import { Player } from 'video-react';
import Blog from "./Blog"
// import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup'
// import ToggleButton from 'react-bootstrap/ToggleButton'
import Card from 'react-bootstrap/Card'
import Nav from 'react-bootstrap/Nav';
// import TargetScroller from 'react-target-scroller';
// import { Link } from 'react-router-dom';

export default class AboutUs extends Component {
    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         scrollTarget: null,
    //     };

    //     this.onNavLinkClick = this.onNavLinkClick.bind(this);
    // }

    // onNavLinkClick(evt) {
       
    //     const hash = this.href.indexOf('/') > -1 ? this.href.split('/')[1] : null;
       
    //     if (!hash) {
    //         return;
    //     }

    //     this.setState({
    //         scrollTarget: `#${hash}`,
    //     });
    // }
    

     render() {
    //     const {
    //         scrollTarget,
    //     } = this.state;

        return (

            <div>
                <Container className="defaultTop aboutNav">
                    <Navbar className="aboutNavbar" expand="lg">
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="justify-content-start mr-auto" defaultActiveKey="/home">
                          


                                <Nav.Item>
                                    <Nav.Link href="#about" onClick={this.onNavLinkClick} >About us</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#whybeegru" onClick={this.onNavLinkClick} >Why Beegru</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#beegruservices" onClick={this.onNavLinkClick} >Beegru’s Services</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#beeguworks" onClick={this.onNavLinkClick} >How it works</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#beegrufaq" onClick={this.onNavLinkClick} >FAQ</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#beegrucareers" onClick={this.onNavLinkClick} >Careers</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#beegrugallery" onClick={this.onNavLinkClick} >Gallery</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link href="#beegrublog" onClick={this.onNavLinkClick} >Blog</Nav.Link>
                                </Nav.Item>
                            </Nav>
                            {/* <TargetScroller target={scrollTarget} /> */}
                        </Navbar.Collapse>
                    </Navbar>
                   
                </Container>


                <Container className="defaultTop" id="about">



                    <Row className="aboutContainer">
                        <Col md={6} lg={6} sm={12} xs={12} >
                            {/* <h1 className="aboutUsmainheading">The smartest way to Buy & Rent home.</h1>
                     <p className="jobDescript">How Beegru is different from others? 
                      <span className="apply">Learn more</span>
                      </p> */}
                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                <h1 className="aboutText">About Us</h1>
                            </div>
                            <div md={6} lg={6} sm={12} xs={12} className=" aboutUsText">
                                <p><b>Beegru</b> We want to leverage our connections with real estate experts ,
                        strategist, builders, professionals, agents and lawyers  to simply the process of real
                         estate transacations of worthwhile properties worldwide.</p>

                            </div>
                        </Col>
                        <Col md={6} lg={6} sm={12} xs={12} className="aboutUsImage">
                            <Player
                                playsInline
                                poster="https://s3-alpha-sig.figma.com/img/15ca/9e43/c2367f8006f6c17567824248c7fd24cb?Expires=1579478400&Signature=FPhJ34STiUSOe67Ky~aWsTKUtZFwmHyTK-MELhVa7~J44tegNAPhLfhHWG8fr1UIb4L6Uu02pKUYm7PkMHKARgCaNPGLR5GfpLBz7Vi4e9VDoDGzscN~0JADWFq4E0UuobHVb8lqVsAUE8OKcA2ifYPzXrojeAUDWMoBhiQuRUYcl4Y2XRBjxZgEep3oUxurDkByCogtO~rBx3lZBWcIa8vHLojWoig9YF9BXflDgfN5kx3jWykomO5NPmDKFdaGfVXcKYU5Uu2zAANC8neTJyw~--fTjRVuGW~LJbwnKds9zZbqhv3FP0F6if-OBODls6KQ0AvAprUrZ7BWT3N8HA__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA"
                                src="https://www.radiantmediaplayer.com/media/bbb-360p.mp4"
                            />
                        </Col>

                    </Row>
                </Container>



                <Container id="whybeegru">

                    <div className="defaultTop">
                        <h1 className="whyBeegruText">Why Beegru ?</h1>
                    </div>
                    <Row>
                        <Col md={6} lg={7} sm={12} xs={12} className="whyBeegruContainer">
                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                                <p className=" aboutUsText"><b className="order">1.</b> We will present your property to the client in the most honest and detailed way possible, so the client is absolutely clear about the property. </p>
                            </div>

                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                                <p className=" aboutUsText"><b className="order">2.</b> We are RERA approved and are transparent with respect to our transacations with the client. </p>
                            </div>

                            <div className="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                                <p className=" aboutUsText"><b className="order">3.</b> We have a vision of connecting clients of real estate industry on a global platform and are constantly striving to implement the latest technological innovations in how we do business and connect clients. </p>
                            </div>
                        </Col>
                        <Col md={6} lg={5} sm={12} xs={12} className="whyBeegruImage" >
                            {/* <Image src=".../images/ybeegru.png" rounded /> */}
                        </Col>
                    </Row>

                </Container>


                <Container id="beegruservices">
                    <div className=" defaultTop">
                        <h1 className="whyBeegruText">Beegru’s Services</h1>
                    </div>
                    <Row className="services">
                        <Col md={4} lg={4} sm={12} xs={12}>
                            <Col md={12} lg={12} sm={12} xs={12} className=" centeraligns serviceimg1">

                            </Col>
                            <Col md={12} lg={12} sm={12} xs={12}>
                                <b className="servicesHeading">Tantants</b>
                                <p >

                                    We connect buyers, sellers, lessors,
                                    investors and joint venture partners in real estate transacations.
                                    </p>
                            </Col>

                        </Col>
                        <Col md={4} lg={4} sm={12} xs={12}>

                            <Col md={12} lg={12} sm={12} xs={12} className=" centeraligns serviceimg2">

                            </Col>
                            <Col md={12} lg={12} sm={12} xs={12}>
                                <h1 className="servicesHeading">Landlords</h1>
                                <div  >
                                    We provide branding and marketing services for real
                                        estate projects and individual properties. </div>
                            </Col>

                        </Col>
                        <Col md={4} lg={4} sm={12} xs={12}>
                            <Col md={12} lg={12} sm={12} xs={12} className="centeraligns serviceimg3">

                            </Col>
                            <Col md={12} lg={12} sm={12} xs={12}>
                                <h1 className="servicesHeading">Partners</h1>
                                <p  >

                                    We help connect real estate service providers with their potential clients.
                                    </p>

                            </Col>
                        </Col>






                    </Row>

                </Container>


                <Container id="beegruworks">
                    <div className=" defaultTop">
                        <h1 className="howItWorks">How it works</h1>
                    </div>
                    <Row>
                        <Col md={6} lg={6} sm={12} xs={12}>
                            <div className="howItWorksContainerSubText">
                                <p className="pText">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                            </div>
                        </Col>
                        <Col md={6} lg={6} sm={12} xs={12} className="">

                            <ButtonGroup aria-label="" className=" flex-wrap">
                                {/* <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
                       <ToggleButton value={1} variant="secondary" className="buyerBtn ">Buyers</ToggleButton>
                       <ToggleButton value={2} variant="secondary" className="buyerBtn ">Sellers</ToggleButton>
                       <ToggleButton value={3} variant="secondary" className="SellersBtn">Agents</ToggleButton>
                       <ToggleButton value={4} variant="secondary" className="SellersBtn">Services</ToggleButton>
                    </ToggleButtonGroup> */}
                                <Button variant="secondary" className="buyerBtn active ">Buyers</Button>
                                <Button variant="secondary" className="buyerBtn ">Sellers</Button>
                                <Button variant="secondary" className="SellersBtn">Agents</Button>
                                <Button variant="secondary" className="SellersBtn">Services</Button>
                            </ButtonGroup>


                        </Col>

                    </Row>
                </Container>
                <Container id="beegru">
                    <Row>
                        <Col lg={4} md={4} sm={12} xs={12} className="boxBorder">
                            <div className="browswpropeties"></div>
                            <h4 className="requirementText">Browse Properties</h4>
                            <p className="requirementTextPara">Browse through properties on beegru.com.
                         Each listing is complete with details and pictures to help you shortlist properties at your leisure.</p>
                        </Col>

                        <Col lg={4} md={4} sm={12} xs={12} className="boxBorder">
                            <div className="sitevisit"></div>
                            <h4 className="requirementText">Site Visit</h4>
                            <p className="requirementTextPara">Get in touch with us for an on-site walkthrough.
                         Beegru with guide you through every detail of the preperty.</p>
                        </Col>

                        <Col lg={4} md={4} sm={12} xs={12} className="boxBorder">
                            <div className="register"></div>
                            <h4 className="requirementText">Register</h4>
                            <p className="requirementTextPara">Complete the transaction and register the property with the help ofBeegru.</p>
                        </Col>

                    </Row>
                </Container>



                {/* <Container>
                         <div className=" defaultTop">
                           <h3 className="team">Team</h3>
                             <p className="teamText">We believe that financing your home should be easy.
                              So we built everything from the ground up getting rid of all the nonsense.
                            </p>
                            </div>
                    
                        <Carousel>                      
                        <Carousel.Item>
                            <img
                                className="d-block w-40"
                                src="/static/media/mobile.6e627553.jpg"
                                alt="Will Smith"
                                width="100%"
                            />
                            <Carousel.Caption>
                                <h3>Rajeev(FOUNDER)</h3>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                className="d-block w-40"
                                src="/static/media/mobile.6e627553.jpg"
                                alt="Will Smith"
                                width="100%"
                            />
                            <Carousel.Caption>
                                <h3>Shruti(CO-FOUNDER)</h3>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                                className="d-block w-40"
                                src="/static/media/mobile.6e627553.jpg"
                                alt="Will Smith"
                                width="100%"
                            />
                            <Carousel.Caption>
                                <h3>Sunil (CO-FOUNDER)</h3>
                                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
                   
              


                </Container> */}
                <Container id="beegrufaq">
                    <div className="defaultTop">
                        <h1 className="faqheading">FAQ</h1>
                        <p className="faqsubheadig">We believe in trasparency, browse through the FAQ
                            if you have any other questions feel free to Contact us</p>
                    </div>
                    <Row>
                        <Col lg={8} md={8}>
                            <Accordion onSelect={this.handleSelect} defaultActiveKey="0">
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                            <i className='fas fa-plus'></i>
                                        </Accordion.Toggle>
                                    Who is a NRI?

                                </Card.Header>
                                    <Accordion.Collapse eventKey="0">
                                        <Card.Body>Non Resident Indian (NRI) is a citizen of India,
                                        who stays abroad for employment/carrying on business or vocation outside India or
                                        stays abroad under circumstances indicating an intention for an uncertain duration
                                        of stay abroad is a non-resident. Non-resident foreign citizens of Indian Origin
                                    are treated at par with Non Resident Indian (NRIs).</Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                            <i className='fas fa-plus'></i>
                                        </Accordion.Toggle>
                                     Who is a PCI?

                                </Card.Header>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>Non Resident Indian (NRI) is a citizen of India,
                                        who stays abroad for employment/carrying on business or vocation outside India or
                                        stays abroad under circumstances indicating an intention for an uncertain duration
                                        of stay abroad is a non-resident. Non-resident foreign citizens of Indian Origin
                                    are treated at par with Non Resident Indian (NRIs).</Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                            <i className='fas fa-plus'></i>
                                        </Accordion.Toggle>
                                    Who is a PIO?

                                </Card.Header>
                                    <Accordion.Collapse eventKey="2">
                                        <Card.Body>Non Resident Indian (NRI) is a citizen of India,
                                        who stays abroad for employment/carrying on business or vocation outside India or
                                        stays abroad under circumstances indicating an intention for an uncertain duration
                                        of stay abroad is a non-resident. Non-resident foreign citizens of Indian Origin
                                    are treated at par with Non Resident Indian (NRIs).</Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                                <Card>
                                    <Card.Header>
                                        <Accordion.Toggle as={Button} variant="link" eventKey="3">
                                            <i className='fas fa-plus'></i>
                                        </Accordion.Toggle>
                                    Who is a OCI?

                                </Card.Header>
                                    <Accordion.Collapse eventKey="3">
                                        <Card.Body>
                                            Non Resident Indian (NRI) is a citizen of India,
                                            who stays abroad for employment/carrying on business or vocation outside India or
                                            stays abroad under circumstances indicating an intention for an uncertain duration
                                            of stay abroad is a non-resident. Non-resident foreign citizens of Indian Origin
                                    are treated at par with Non Resident Indian (NRIs).</Card.Body>
                                    </Accordion.Collapse>
                                </Card>

                            </Accordion>


                        </Col>
                        <Col lg={4} md={4} className="faqImage">

                        </Col>
                    </Row>
                </Container>

                <Container id="beegrucareers">
                    <div className="defaultTop">
                        <h1 className="openPositionText  ">Careers</h1>
                    </div>

                    <Row>
                        <Col xs={6} md={2} className="designation">Motion Designer
                            </Col>
                        <Col xs={6} md={2}> <Badge variant="dark">Full-Time</Badge>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={9} >
                            <p className="jobDescript">A serial entrepreneur who believes in making home ownership accessible to everyone. A serial entrepreneur who believes in making home ownership accessible to everyone.</p>

                        </Col>

                        <Col md={3} className="centeralign">
                            <h1 className="apply">Apply</h1>
                        </Col>

                    </Row>




                    <Row >
                        <Col xs={6} md="auto" className="designation">Interaction Designer
                            </Col>
                        <Col xs={6} md={2}>
                            <Badge variant="dark">Full-Time</Badge><Badge variant="dark">Remote</Badge>

                        </Col>
                    </Row>
                    <Row>
                        <Col md={9} className="bottomborder">
                            <p className="jobDescript ">A serial entrepreneur who believes in making home ownership accessible to everyone. A serial entrepreneur who believes in making home ownership accessible to everyone.</p>

                        </Col>

                        <Col md={3} className="centeralign">
                            <h1 className="apply">Apply</h1>
                        </Col>

                    </Row>




                    <Row >
                        <Col xs={6} md="auto" className="designation">Lead Product Designer
                            </Col>
                        <Col xs={6} md={2}> <Badge variant="dark">Full-Time</Badge><Badge variant="dark">Local</Badge>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={9} >
                            <p className="jobDescript">A serial entrepreneur who believes in making home ownership accessible to everyone. A serial entrepreneur who believes in making home ownership accessible to everyone.</p>

                        </Col>

                        <Col md={3} className="centeralign">
                            <h1 className="apply">Apply</h1>
                        </Col>

                    </Row>






                </Container>

                <Container id="beegrugallery">
                    <div className="defaultTop">
                        <h1 className="openPositionText  ">Gallery</h1>
                    </div>
                    <Row className="nomargin" >

                        <GalleryImageComponent />

                    </Row>
                </Container>
                <Container id="beegrublog">
                    <div className="defaultTop">
                        <h1 className="openPositionText  ">Blog</h1>
                    </div>
                    <Row className="nomargin" >
                        <Blog />


                    </Row>

                </Container>

            </div>
        )
    }
}

