import React, { Component } from 'react';
import { Row, Col, Container, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
// import About from "./../about";
import imgfamily from "./../../images/familyPaintingWallsTogether.png"
import { Player } from 'video-react';


export default class AboutBeeger extends Component {
        constructor(props) {
                super(props);
               this.state = {  
                       users : {}
               }
              //  this.loadaboutData = this.loadaboutData.bind(this);
       
        }
        
        componentDidMount() {
                fetch('https://aceimagingandvideo.com/beegru/website/fetchIndexInfo', {
                        method: 'POST',
                        async: false,
                        headers: {
                                //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                "mobileNumber":"8970685540"
                        })
                }).then(response => {
                        return response.json();
                }).then(result => {
                        console.log("Result", result);
                        this.setState({
                                users: result,
                        });
                       // alert("about Result"+this.state.users.aboutUsDto.description);

                });

        }

      
        render() {
                return (
                        

                        <div id="aboutBeegru">
                                

                                <Container>
                                        <h1 className="aboutBeegruHeading mr-auto">About Beegru</h1>

                                        <Row>

                                                <Col md={4}>
                                                        <div className="aboutBeegruText">
                                                        {Object.keys(this.state.users).length > 0 &&
                                                                this.state.users.aboutUsDto.description}
                                                            </div>
                                                        <div className="knowMoreSection">
                                                                <Link to="/about">
                                                                        <Button className="knowMoreButton" variant="secondary">Know More</Button>
                                                                </Link>

                                                        </div>
                                                </Col>

                                                <Col md={{ span: 4, offset: 4 }} >

                                                        <Player
                                                                playsInline
                                                                poster={imgfamily}
                                                                src=""
                                                        />

                                                </Col>

                                        </Row>

                                </Container>
                        </div>

                )
        }
}

    //function to call SWAPI and get data.
   
    
   