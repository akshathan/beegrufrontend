import React, { Component } from 'react'
import { Row, Col, Container , Button} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default class FindPropertiesNearBy extends Component {

        constructor(props) {
                super(props);
                this.state = {
                   pincode: '',
                };
        }
        handleChange= (e) => {
                this.setState({ pincode: e.target.value });
                console.log(e.target.value);
              }

        findProperties = () => {
                localStorage.setItem("searchStatus","2");
                localStorage.setItem("pincode",this.state.pincode);
              //  window.location = "../propertyList"
              this.setState({redirect:true});
        }

              

        render() {
                if(this.state.redirect){
                        return <Redirect push to="/propertyList"/>;
                }
                return (
                        <Container fluid>
                                <Row className="findPropertynearyou">
                                        <Col xs lg="3">
                                                <h1>Find Properties</h1>
                                                <h2>near you</h2>
                                        </Col>
                                        <Col xs lg="3">
                                                <div className="enterPincode">
                                                        <input type="text" onChange={this.handleChange} placeholder="Enter your Pincode" />
                                                        <Button onClick={this.findProperties} className="setLocation" variant="dark">Find Properties</Button>

                                                </div>

                                        </Col>
                                        <Col lg="1">
                                                <span className="or">or</span>
                                        </Col>
                                        <Col xs lg="2">

                                                <div className="useMyLocation">

                                                        <span className="locationIcon"><i className="fas fa-map-marker-alt" aria-hidden="true"></i></span>
                                                        <span className="locationIconText">Use my location</span>
                                                </div>
                                        </Col>
                                </Row>
                        </Container>

                       

                        //         {/* <div id="findPropertiesNearBy">
                        //                 <div className="findPropHeading">
                        //                         <span className="bold">Find Properties</span><br />
                        //                         near you
                        //         </div>
                        //                 <div className="enterPincode">
                        //                         <input type="text" placeholder="Enter your Pincode" />
                        //                         <span className="setLocation">Set Location</span>
                        //                 </div>
                        //                 <div className="or">Or</div>
                        //                 <div className="useMyLocation">
                        //                         <span className="locationIcon"><i className="fas fa-map-marker-alt" aria-hidden="true"></i></span>
                        //                         <span className="locationIconText">Use my location</span>
                        //                 </div>
                        //         </div> */}
                        
                )
        }
}
