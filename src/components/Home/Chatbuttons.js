import React, { Component } from 'react'
import { Container,Row,Col,Form } from 'react-bootstrap';
import "../../css/Chatbuttons.css";
class Chatbuttons extends Component {
    state = {  }
    render() { 
        return ( 
            <div>

                            <Container>
                                <Row>
                                       <Col md={3}>
                                       <a href="tel:+918861039999">
                                              <div className="contactUsButton">
                                              
                                                <span className="buttonSpan">
                                                        <i className="fa fa-phone" aria-hidden="true"></i>
                                                </span>
                                                <span className="buttonSpan buttonSpanText">Call</span>
                                                <span className="buttonSpan">
                                                         <i className="fas fa-arrow-right"></i>
                                                </span>
                                                
                                               </div>
                                               </a>
                                        </Col>
                                        <Col md={3} >
                                        <a href="mailto:mail@beegru.com">
                                        <div className="contactUsButton">
                                                <span className="buttonSpan"><i className="fa fa-envelope" aria-hidden="true"></i></span><span className="buttonSpan buttonSpanText">Mail</span> <span className="buttonSpan"> <i className="fas fa-arrow-right"></i> </span>
                                        </div>
                                        </a>
                                        </Col>
                                        <Col md={3}>
                                        <a href="https://api.whatsapp.com/send?phone=918861039999" title="Whatsapp Chat">
                                        {/* <a href="https://wa.me/919591674565?text=Hi"> */}
                                        <div className="contactUsButton">
                                                <span className="buttonSpan"><i className="fab fa-whatsapp" aria-hidden="true"></i></span><span className="buttonSpan buttonSpanText">Whatsapp Us</span> <span className="buttonSpan"> <i className="fas fa-arrow-right"></i> </span>
                                        </div>
                                        </a>
                                        </Col>
                                        <Col md={3} >
                                        <div className="contactUsButton">
                                                <span className="buttonSpan"><i className="fas fa-comments" aria-hidden="true"></i></span><span className="buttonSpan buttonSpanText">Chat Us</span> <span className="buttonSpan"> <i className="fas fa-arrow-right"></i> </span>
                                        </div>
                                        </Col>
                               
                                </Row>

                            </Container>
            </div>
         );
    }
}
 
export default Chatbuttons;