import React, { Component } from "react";
//import ScrollToTop from "react-scroll-up";
import {CircleArrow as ScrollUpButton} from "react-scroll-up-button"; //Add this line Here
import "../css/Navbar.css";
import  Beegrunav  from "./Beegrunav.js";
import Header from "./Header.jsx";
import Beegrufooter from "./Footer.jsx";

export default class BeegruNavbar extends Component {
  state = {};
  render() {
    return (
      <div>
        <Header />
        <Beegrunav />
       
        <ScrollUpButton />

        {/* <ScrollToTop showUnder={160}>
          <span>
            <i className="fas fa-arrow-up myBtn"></i>
          </span>
        </ScrollToTop> */}

        <Beegrufooter />
      </div>
    );
  }
}

