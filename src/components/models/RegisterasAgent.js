import React from "react";
import {
  Form,
  Col,
  Modal,
  Button

} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";

export default class RegisterasAgent extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { validated: false };
      this.state = { modalShow: false };
    }
    
  
    handleSubmit(event) {
      const form = event.currentTarget; 
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.setState({ validated: true });
    }
  
    render() {
      const { validated } = this.state;
  
      let modalClose = () =>
      this.setState({
        modalShow1: false
      });
  
      let otpModal = () =>
      this.setState({
        modalShow1: true
      });
  
      let closeRegistrationModal = () =>
      this.setState({
        modalShow: false
      });
  
  
      function verifyAgentRegistration(){
       
       // if(statusVariable == 1){
        fetch('https://aceimagingandvideo.com/beegru/website/saveAgent', {
                            method: 'POST',
                            async: false,
                            headers: {
                                    //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                    "name": document.agentregistration.regagentName.value,
                                    "mobileNumber": document.agentregistration.regagentPhoneNumber.value,
                                    "emailId": document.agentregistration.regagentEmail.value,
                                    "password": document.agentregistration.regagentPassword.value,
                                    "address": document.agentregistration.regagentAddress.value,
                                    "activeStatus": 2,
                                    "roles":[
                                      {
                                      "roleId":"5"
                                      }
                                      ],
                                      "location":document.agentregistration.regagentLocation.value,
                                      "agentDetails":{
  
                                      }
                                     })
                    }).then((response) => response.json())
                            .then((responseJson) => {
                                    console.log(responseJson);
                                   
                                    if(responseJson.resultStatus == 3){
                                      alert("Email ID already Registered");
                                      //document.getElementById("agentregistrationForm").reset();
                                    }else if(responseJson.resultStatus == 2){
                                      alert("mobile number already Registered");
                                      //document.getElementById("agentregistrationForm").reset();
                                    }
                                    else if(responseJson.resultStatus == 1){
                                        alert("Registration Successfull");
                                        document.getElementById("agentregistrationForm").reset();
                                        //window.location.reload();
                                        closeRegistrationModal();
                                    }else{
                                      alert("Error");
                                    }
                                   
                            })
                            .catch((error) => {
                                    console.error(error);
                            });
                          // }else{
                          // alert("Please verify your mobile number");
                          // }
      }
  
  
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Register as agent
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form
              noValidate
              validated={validated}
              onSubmit={e => this.handleSubmit(e)}
              name="agentregistration"
              id="agentregistrationForm"
            >
              <Form.Group as={Col} md="12" controlId="validationCustom21">
                <Form.Label>NAME *</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Name"
                  id="regagentName"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="validationCustom03">
                <Form.Label>PHONE *</Form.Label>
                {/* <InputGroup className="mb-3"> */}
                  <Form.Control
                    className="controll"                  
                    type="text"
                    placeholder="Phone"
                    id="regagentPhoneNumber"
                    disabled = {(this.state.disabled)? "disabled" : ""}                  
                  />
                  <Form.Control.Feedback type="invalid">
                    must be 10 numbers. Please provide a valid Phone number.
                  </Form.Control.Feedback>
                  {/* <InputGroup.Append>
                    <Button variant="outline-secondary" className="btn btn-danger"
                    onClick={onClick}>Sent OTP</Button>
                  </InputGroup.Append>
                </InputGroup>
                <OtpModal show={this.state.modalShow1} onHide={modalClose} />*/}
              </Form.Group> 
  
              <Form.Group as={Col} md="12" controlId="validationCustom04">
                <Form.Label>EMAIL *</Form.Label>
                <Form.Control
                  className="controll"
                  type="email"
                  placeholder="Email"
                  id="regagentEmail"
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Please provide a valid Email id.
                </Form.Control.Feedback>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="validationCustom05">
                <Form.Label>PASSWORD *</Form.Label>
                <Form.Control
                  className="controll"
                  type="password"
                  placeholder="Password"
                  id="regagentPassword"
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Please provide a valid password.
                </Form.Control.Feedback>
                <Form.Text className="blue">
                  Must be more than 8 Characters.
                </Form.Text>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="validationCustom22">
                <Form.Label>ADDRESS *</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Address"
                  id="regagentAddress"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="validationCustom23">
                <Form.Label>SPECIALITY *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="speciality"
                  as="select"
                  placeholder="Speciality"
                  id="regagentSpeciality"
                  required
                >
                  <option>Speciality</option>
                  <option>bangalore</option>
                  <option>mysore</option>
                  <option>chitradurga</option>
                </Form.Control>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="validationCustom24">
                <Form.Label>LOCATION I AM EXPECT IN *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="Location"
                  as="select"
                  placeholder="Locations I am expert in"
                  id="regagentLocation"
                  required
                >
                  <option>Location i am expert in</option>
                  <option>bangalore</option>
                  <option>mysore</option>
                  <option>chitradurga</option>
                </Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="11">
                <Button
                  type="button"
                  onClick={verifyAgentRegistration}
                  className="btn btn-danger btnFullwidth"
                  size="lg"
                  block
                >
                  Register As Agent
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }
  