import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup,
  
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Commercialforsale extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
    render() {
      function commercialForSaleSubmit(){              
        var items=document.getElementsByName('commercialForSaleAmenities');
        var commercialForSaleAmenitiesList="";
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox' && items[i].checked==true)
          commercialForSaleAmenitiesList+=items[i].value+",";
        }
        console.log("Selected Amenities : "+commercialForSaleAmenitiesList);     
        fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
           method: 'POST',
           async: false,
           headers: {                               
                   Accept: 'application/json',
                   'Content-Type': 'application/json',
           },
           body: JSON.stringify({
             "propertyName":document.uploadProperty.propertyName.value, 
             "propertyLocation":document.uploadProperty.propertyLocation.value,
             "propertyLandmark":document.uploadProperty.propertyLandmark.value,
             "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
             "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
             "shortDescription":document.uploadProperty.propertyDescription.value,
             "status":document.querySelector('input[name=commercialForSaleStatus]:checked').value,
             "furnishing":document.querySelector('input[name=commercialForSaleFurnishing]:checked').value, 
             "bathrooms":document.commercialForSale.commercialForSaleBathrooms.value,
             "carpetArea":document.commercialForSale.commercialForSaleCarpetArea.value,
             "superBuiltupArea":document.commercialForSale.commercialForSaleSuperBuiltupArea.value,
             "length":document.commercialForSale.commercialForSaleLength.value,
             "breadth":document.commercialForSale.commercialForSaleBreadth.value,
             "roadFacingWidth":document.commercialForSale.commercialForSaleRoadFacingWidth.value,
             "waterSupply":document.querySelector('input[name=commercialForSaleWaterSupply]:checked').value, 
             "groundWaterLevel":document.commercialForSale.commercialForSaleGroundWaterLevel.value,
             "amenities":commercialForSaleAmenitiesList, 
             "aboutLocation":document.commercialForSale.commercialForSaleAboutLocation.value,
             "aboutProperty":document.commercialForSale.commercialForSaleAboutProperty.value, 
             "other":document.commercialForSale.commercialForSaleOther.value,
             "rooms":document.commercialForSale.commercialForSaleRooms.value, 
             "floorNumber":document.commercialForSale.commercialForSaleFloorNumber.value, 
             "balconies":document.commercialForSale.commercialForSaleBalconies.value,
             "facing":document.querySelector('input[name=commercialForSaleFacing]:checked').value,
             "otherTag":document.querySelector('input[name=commercialForSaleOtherTags]:checked').value,
             "persons":document.commercialForSale.commercialForSalePersons.value,           
             "minPrice":document.commercialForSale.commercialForSaleMinPrice.value,
             "maxPrice":document.commercialForSale.commercialForSaleMaxPrice.value,           
             "longDescription":document.commercialForSale.commercialForSaleLongDescription.value,
             "propertyVicinity":document.commercialForSale.commercialForSalePropertyVicinity.value,
             "coLiving":document.querySelector('input[name=commercialForSaleCoLiving]:checked').value,
             "NoOfImages":document.commercialForSale.commercialForSaleNoOfImages.value,                                             
           })
   }).then((response) => response.json())
           .then((responseJson) => {                  
                   console.log(responseJson);
                   if(responseJson.resultStatus == 2){
                     alert("");                    
                   }else if(responseJson.resultStatus == 1){                                                               
                       alert('');                    
                   }else{
                     alert("Error");
                   }
                   return responseJson.movies;
           })
           .catch((error) => {
                   console.error(error);
           });
       }
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Commercial for Sale
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="commercialForSaleLongDescription"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Status</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="New"
                    name="commercialForSaleStatus"
                    value="New"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    name="commercialForSaleStatus"
                    value="Resale"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    name="commercialForSaleStatus"
                    value="UnderConstructions"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    name="commercialForSaleStatus"
                    value="ReadyToMoveIn"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    name="commercialForSaleStatus"
                    value="BuiltToSuit"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    name="commercialForSaleStatus"
                    value="BareShell"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Furnishing</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    name="commercialForSaleFurnishing"
                    value="Furnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    name="commercialForSaleFurnishing"
                    value="SemiFurnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    name="commercialForSaleFurnishing"
                    value="UnFurnished"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleRooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>if Co-Living Person</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSalePersons"
                  required
                />
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Private Cabin"
                      name="commercialForSaleCoLiving"
                      id="PrivateCabin"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Shared Work Space"
                      name="commercialForSaleCoLiving"
                      id="SharedWorkSpace"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Floor Number</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleFloorNumber"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Balconies</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleBalconies"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Bath Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleBathrooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Carpet Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleCarpetArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Super built Up Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleSuperBuiltupArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Row>
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="Length"
                    id="commercialForSaleLength"
                    required
                  />
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="Breadth"
                    id="commercialForSaleBreadth"
                    required
                  />
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="commercialForSaleFacing"
                      value="North"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="commercialForSaleFacing"
                      value="South"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="commercialForSaleFacing"
                      value="East"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="commercialForSaleFacing"
                      value="West"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="commercialForSaleFacing"
                      value="NorthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="commercialForSaleFacing"
                      value="NorthWest"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="commercialForSaleFacing"
                      value="SouthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="commercialForSaleFacing"
                      value="SouthWest"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleRoadFacingWidth"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleGroundWaterLevel"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Amenities</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="A/c"
                      name="commercialForSaleAmenities"
                      value="AC"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="GYM"
                      name="commercialForSaleAmenities"
                      value="Gym"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Laundry"
                      name="commercialForSaleAmenities"
                      value="Laundry"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="commercialForSaleAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="WiFi"
                      name="commercialForSaleAmenities"
                      value="WiFi"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Security"
                      name="commercialForSaleAmenities"
                      value="Security"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Pool"
                      name="commercialForSaleAmenities"
                      value="Pool"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="commercialForSaleAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other Aminities</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForSaleOtherAminities"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other tags</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NRI"
                      name="commercialForSaleOtherTags"
                      value="NRI"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Mansion"
                      name="commercialForSaleOtherTags"
                      value="Mansion"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  id="commercialForSaleAboutLocation"
                  required
                />
                <ButtonToolbar>
                  <ButtonGroup>
                    <Button>
                      <i className="fas fa-align-left" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-center" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-right" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-justify" />
                    </Button>
                  </ButtonGroup>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  id="commercialForSaleAboutProperty"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  id="commercialForSaleOther"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label><br/>
                <Form.Label>Minimum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Min Price"
                  id="commercialForSaleMinPrice"
                  required
                /><br/>
                <Form.Label>Maximum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Max Price"
                  id="commercialForSaleMaxPrice"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="11">
                <Button 
                  className="button1 btn-danger btnFullwidth"
                  onClick={e => {e.preventDefault(); {commercialForSaleSubmit()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }