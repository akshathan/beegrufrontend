import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup,
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Commercialforrent extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
    render() {
      function commercialForRentSubmit(){              
        var items=document.getElementsByName('commercialForRentAmenities');
        var commercialForRentAmenitiesList="";
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox' && items[i].checked==true)
          commercialForRentAmenitiesList+=items[i].value+",";
        }
        console.log("Selected Amenities : "+commercialForRentAmenitiesList);     
        fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
           method: 'POST',
           async: false,
           headers: {                               
                   Accept: 'application/json',
                   'Content-Type': 'application/json',
           },
           body: JSON.stringify({
             "propertyName":document.uploadProperty.propertyName.value, 
             "propertyLocation":document.uploadProperty.propertyLocation.value,
             "propertyLandmark":document.uploadProperty.propertyLandmark.value,
             "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
             "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
             "shortDescription":document.uploadProperty.propertyDescription.value,
             "status":document.querySelector('input[name=commercialForRentStatus]:checked').value,
             "furnishing":document.querySelector('input[name=commercialForRentFurnishing]:checked').value, 
             "bathrooms":document.commercialForRent.commercialForRentBathrooms.value,
             "carpetArea":document.commercialForRent.commercialForRentCarpetArea.value,
             "superBuiltupArea":document.commercialForRent.commercialForRentSuperBuiltupArea.value,
             "length":document.commercialForRent.commercialForRentLength.value,
             "breadth":document.commercialForRent.commercialForRentBreadth.value,
             "roadFacingWidth":document.commercialForRent.commercialForRentRoadFacingWidth.value,
             "waterSupply":document.querySelector('input[name=commercialForRentWaterSupply]:checked').value, 
             "groundWaterLevel":document.commercialForRent.commercialForRentGroundWaterLevel.value,
             "amenities":commercialForRentAmenitiesList, 
             "aboutLocation":document.commercialForRent.commercialForRentAboutLocation.value,
             "aboutProperty":document.commercialForRent.commercialForRentAboutProperty.value, 
             "other":document.commercialForRent.commercialForRentOther.value,
             "rooms":document.commercialForRent.commercialForRentRooms.value, 
             "floorNumber":document.commercialForRent.commercialForRentFloorNumber.value, 
             "balconies":document.commercialForRent.commercialForRentBalconies.value,
             "facing":document.querySelector('input[name=commercialForRentFacing]:checked').value,
             "otherTag":document.commercialForRent.commercialForRentOtherTags.value,
             "persons":document.commercialForRent.commercialForRentPersons.value,           
             "minPrice":document.commercialForRent.commercialForRentMinPrice.value,
             "maxPrice":document.commercialForRent.commercialForRentMaxPrice.value,           
             "longDescription":document.commercialForRent.commercialForRentLongDescription.value,
             "propertyVicinity":document.commercialForRent.commercialForRentPropertyVicinity.value,
             "coLiving":document.querySelector('input[name=commercialForRentCoLiving]:checked').value,
             "agreementPeriod":document.querySelector('input[name=commercialForRentAgreementPeriod]:checked').value, 
             "preference":document.querySelector('input[name=commercialForRentPereference]:checked').value, 
             "tenentHabitPreference":document.querySelector('input[name=commercialForRentTenentHabitsPreference]:checked').value,
             "maintainanceCharge":document.commercialForRent.commercialForRentMaintainenssCharges.value, 
             "securityDeposit":document.commercialForRent.commercialForRentSecurityDeposit.value, 
            //  "NoOfImages":document.commercialForRent.commercialForRentNoOfImages.value,                                             
           })
   }).then((response) => response.json())
           .then((responseJson) => {                  
                   console.log(responseJson);
                   if(responseJson.resultStatus == 2){
                     alert("");                    
                   }else if(responseJson.resultStatus == 1){                                                               
                       alert('');                    
                   }else{
                     alert("Error");
                   }
                   return responseJson.movies;
           })
           .catch((error) => {
                   console.error(error);
           });
       }
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Commercial for Rent
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="commercialForRentLongDescription"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Status</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="New"
                    name="commercialForRentStatus"
                    value="New"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    name="commercialForRentStatus"
                    value="Resale"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    name="commercialForRentStatus"
                    value="UnderConstructions"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    name="commercialForRentStatus"
                    value="ReadyToMoveIn"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    name="commercialForRentStatus"
                    value="BuiltToSuit"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    name="commercialForRentStatus"
                    value="BareShell"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Furnishing</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    name="commercialForRentFurnishing"
                    value="Furnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    name="commercialForRentFurnishing"
                    value="SemiFurnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    name="commercialForRentFurnishing"
                    value="UnFurnished"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentRooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>if Co-Living Person</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentPersons"
                  required
                />
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Private Cabin"
                      name="commercialForRentCoLiving"
                      value="PrivateCabin"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Shared Work Space"
                      name="commercialForRentCoLiving"
                      value="SharedWorkSpace"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Floor Number</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentFloorNumber"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Balconies</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentBalconies"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Bath Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentBathrooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Carpet Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentCarpetArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Super built Up Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentSuperBuiltupArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Row>
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="Length"
                    id="commercialForRentLength"
                    required
                  />
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="Breadth"
                    id="commercialForRentBreadth"
                    required
                  />
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="commercialForRentFacing"
                      value="North"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="commercialForRentFacing"
                      value="South"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="commercialForRentFacing"
                      value="East"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="commercialForRentFacing"
                      value="West"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="commercialForRentFacing"
                      value="NorthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="commercialForRentFacing"
                      value="NorthWest"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="commercialForRentFacing"
                      value="SouthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="commercialForRentFacing"
                      value="SouthWest"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentRoadFacingWidth"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentGroundWaterLevel"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Amenities</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="A/c"
                      name="commercialForRentAmenities"
                      value="AC"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="GYM"
                      name="commercialForRentAmenities"
                      value="GYM"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Laundry"
                      name="commercialForRentAmenities"
                      value="Laundry"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="commercialForRentAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="WiFi"
                      name="commercialForRentAmenities"
                      value="WiFi"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Security"
                      name="commercialForRentAmenities"
                      value="Security"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Pool"
                      name="commercialForRentAmenities"
                      value="Pool"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="commercialForRentAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other Aminities</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentOtherAmenities"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other tags</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentOtherTags"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Agreement Period</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="3 months"
                      name="commercialForRentAgreementPeriod"
                      value="3 months"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="6 months"
                      name="commercialForRentAgreementPeriod"
                      value="6 months"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="12 months"
                      name="commercialForRentAgreementPeriod"
                      value="12 months"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="2 years"
                      name="commercialForRentAgreementPeriod"
                      value="2 years"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Pereference</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Veg"
                      name="commercialForRentPereference"
                      value="Veg"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Non Veg"
                      name="commercialForRentPereference"
                      value="NonVeg"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No perference"
                      name="commercialForRentPereference"
                      value="NoPreference"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label=""
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Tenent habits preference</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Smoking"
                      name="commercialForRentTenentHabitsPreference"
                      value="Smoking"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Drinking"
                      name="commercialForRentTenentHabitsPreference"
                      value="NoDrinking"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Smoking"
                      name="commercialForRentTenentHabitsPreference"
                      value="NoSmoking"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Preference"
                      name="commercialForRentTenentHabitsPreference"
                      value="NoPreference"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label><br/>
                <Form.Label>Minimum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Min Price"
                  id="commercialForRentMinPrice"
                  required
                /><br/>
                <Form.Label>Maximum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Max Price"
                  id="commercialForRentMaxPrice"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Maintainenss charges/month</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentMaintainenssCharges"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Security Deposit</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="commercialForRentSecurityDeposit"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  id="commercialForRentAboutLocation"
                  required
                />
                <ButtonToolbar>
                  <ButtonGroup>
                    <Button>
                      <i className="fas fa-align-left" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-center" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-right" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-justify" />
                    </Button>
                  </ButtonGroup>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  id="commercialForRentAboutProperty"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  id="commercialForRentOther"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="11">
                <Button
                  className="button1 btn-danger btnFullwidth"
                  onClick={e => {e.preventDefault(); {commercialForRentSubmit()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }