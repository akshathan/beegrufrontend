import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button

} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";
var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class OtpModal extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
    }
  
   
  
    render() {
  
      let closeOtpModal = () =>   
        this.setState({
          modalShow: false
        });
        
        function handleGameClik() {
          this.setState( {disabled: !this.state.disabled} )
        } 
  
      function verifyOtp(){      
        if(document.verifyOtpForm.otp.value == ""){
          alert("Please enter your OTP ")
        }else{
        fetch('https://aceimagingandvideo.com/beegru/website/verifyOtp', {
          method: 'POST',
          async: false,
          headers: {
                  //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({  
            "mobileNumber": document.registration.regPhoneNumber.value,                
            "recievedOtp": document.verifyOtpForm.otp.value                 
          })
      }).then((response) => response.json())
          .then((responseJson) => {
                  console.log(responseJson);
                  if(responseJson.resultStatus == 1){
                    alert("OTP Verified Successfully");
                    statusVariable = 1;
                    closeOtpModal();
                    handleGameClik();                                             
                  }else if(responseJson.resultStatus == 2){
                    alert("Invalid OTP");
                  }else{
                    alert("Internal Server Error");
                  }
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
        }
      }
  
      function resendOtp(){
        console.log(document.registration.regPhoneNumber.value);     
        fetch('https://aceimagingandvideo.com/beegru/website/sendOtp', {
          method: 'POST',
          async: false,
          headers: {
                  //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({  
            "mobileNumber": document.registration.regPhoneNumber.value                
                             
          })
      }).then((response) => response.json())
          .then((responseJson) => {
                  console.log(responseJson);
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
      }
      
  
      return (
        <Modal
          {...this.props}
          size="sm"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          id="otpModal"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter"></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form name="verifyOtpForm">
              <Form.Group as={Col} md="12">
                <Form.Label>Enter OTP</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="****"
                  id="otp"                
                />
              </Form.Group>
              <Form.Row>
                <Form.Group as={Col} md="6" sm="6">
                  <Button onClick={verifyOtp} className="btn btn-danger" type="submit" size="md" block>
                    Verify
                  </Button>
                </Form.Group>
                <Form.Group as={Col} md="6" sm="6">
                  <Button onClick={resendOtp} className="btn btn-danger" type="submit" size="md" block>
                    Resend
                  </Button>
                </Form.Group>
              </Form.Row>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }