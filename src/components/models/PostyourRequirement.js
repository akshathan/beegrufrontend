import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";
import UploadImage from "./UploadImage.js"

import "../../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class PostyourRequirement extends React.Component {
    render() {
  
      function postYourRequirement(){        
        fetch('https://aceimagingandvideo.com/beegru/website/postYourRequirement', {
          method: 'POST',
          async: false,
          headers: {                               
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({
                  "name": document.postYourRequirementForm.postYourRequirement.value,
                  "comments": document.postYourRequirementForm.postYourRequirement.value,
                  "wantTo": document.postYourRequirementForm.postYourRequirementWantTo.value,
                  "location": document.postYourRequirementForm.postYourRequirementLocation.value,                                
          })
  }).then((response) => response.json())
          .then((responseJson) => {                  
                  console.log(responseJson);
                  if(responseJson.resultStatus == 2){
                    alert("");                    
                  }else if(responseJson.resultStatus == 1){                                                               
                      alert('');
                      //document.getElementById("postYourRequirementFormType").reset();
                      //window.location.reload();
                  }else{
                    alert("Error");
                  }
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
      }
  
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Post your requirement
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p className="postform_text">
            If you have any specific requirements with respect of property buying, selling, renting or join ventures transactions, send us a message or get in touch with us
            </p>
            <p className="postform_p">
              Email: mail@beegru.com
              <br />
              Phone: +91 88610 39999
            </p>
            {/* <Link to="/home">
              <p className="postform_link">or upload details</p>
            </Link> */}
            <Form name="postYourRequirementForm" id="postYourRequirementFormType">
              <Form.Group as={Col} md="12" controlId="formBasicName">
                <Form.Label>I WANT TO</Form.Label>
                <ButtonToolbar>
                  <Button variant="outline-secondary" className="mlr-3">SELL</Button>

                  <Button variant="outline-secondary" className="mlr-3">BUY</Button>

                  <Button variant="outline-secondary" className="mlr-3">RENT</Button>

                  <Button variant="outline-secondary" className="mlr-3">RENT-OUT</Button>
                  
                  <Button variant="outline-secondary" className="mlr-3">JOINT-VENTURES</Button>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>PROPERTY LOCATION</Form.Label>
                <InputGroup className="mb-3 m1">
                  <InputGroup.Prepend>
                    <Button variant="outline">
                      <i className="fa fa-search"></i>
                    </Button>
                  </InputGroup.Prepend>
                  <FormControl aria-describedby="basic-addon1" />
                </InputGroup>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>UPLOAD IMAGE *</Form.Label>
                <InputGroup>
                <UploadImage />
                  {/* <Form.Control placeholder="UPLOAD" type="file" multiple /> */}
                </InputGroup>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>COMMENTS</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="COMMENTS"
                  required
                ></Form.Control>
              </Form.Group>
             
              <Form.Group>
                <Form.Check
                  required
                  className="bordernone"
                  label="I have read and agree to the Privacy Policy and 
                  Terms of use
                  *"
                  feedback="You must agree before submitting."
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Button
                  className="button1 btn-danger btnFullwidth"
                  onClick={e => {e.preventDefault(); {postYourRequirement()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post requirement
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }
