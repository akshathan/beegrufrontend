import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup,
  
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Warehousingforrent extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
    render() {
      function warehouseForRentSubmit(){              
        var items=document.getElementsByName('warehouseForRentAmenities');
        var warehouseForRentAmenitiesList="";
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox' && items[i].checked==true)
          warehouseForRentAmenitiesList+=items[i].value+",";
        }
        console.log("Selected Amenities : "+warehouseForRentAmenitiesList);     
        fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
           method: 'POST',
           async: false,
           headers: {                               
                   Accept: 'application/json',
                   'Content-Type': 'application/json',
           },
           body: JSON.stringify({
             "propertyName":document.uploadProperty.propertyName.value, 
             "propertyLocation":document.uploadProperty.propertyLocation.value,
             "propertyLandmark":document.uploadProperty.propertyLandmark.value,
             "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
             "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
             "shortDescription":document.uploadProperty.propertyDescription.value,
             "status":document.querySelector('input[name=warehouseForRentStatus]:checked').value,
             "furnishing":document.querySelector('input[name=warehouseForRentFurnishing]:checked').value, 
             "bathrooms":document.warehouseForRent.warehouseForRentBathrooms.value,
             "carpetArea":document.warehouseForRent.warehouseForRentCarpetArea.value,
             "superBuiltupArea":document.warehouseForRent.warehouseForRentSuperBuiltupArea.value,
             "length":document.warehouseForRent.warehouseForRentLength.value,
             "breadth":document.warehouseForRent.warehouseForRentBreadth.value,
             "roadFacingWidth":document.warehouseForRent.warehouseForRentRoadFacingWidth.value,
             "waterSupply":document.querySelector('input[name=warehouseForRentWaterSupply]:checked').value, 
             "groundWaterLevel":document.warehouseForRent.warehouseForRentGroundWaterLevel.value,
             "amenities":warehouseForRentAmenitiesList, 
             "aboutLocation":document.warehouseForRent.warehouseForRentAboutLocation.value,
             "aboutProperty":document.warehouseForRent.warehouseForRentAboutProperty.value, 
             "other":document.warehouseForRent.warehouseForRentOther.value,
             "rooms":document.warehouseForRent.warehouseForRentRooms.value, 
             "floorNumber":document.warehouseForRent.warehouseForRentFloorNumber.value, 
             "balconies":document.warehouseForRent.warehouseForRentBalconies.value,
             "facing":document.querySelector('input[name=warehouseForRentFacing]:checked').value,
             "otherTag":document.querySelector('input[name=warehouseForRentOtherTags]:checked').value,
             "persons":document.warehouseForRent.warehouseForRentPersons.value,           
             "minPrice":document.warehouseForRent.warehouseForRentMinPrice.value,
             "maxPrice":document.warehouseForRent.warehouseForRentMaxPrice.value,           
             "longDescription":document.warehouseForRent.warehouseForRentLongDescription.value,
             //"propertyVicinity":document.warehouseForRent.warehouseForRentPropertyVicinity.value,
             "coLiving":document.querySelector('input[name=warehouseForRentCoLiving]:checked').value,
             "agreementPeriod":document.querySelector('input[name=warehouseForRentAgreementPeriod]:checked').value, 
              "preference":document.querySelector('input[name=warehouseForRentPereference]:checked').value, 
              "tenentHabitPreference":document.querySelector('input[name=warehouseForRentTenentHabitsPreference]:checked').value,
              "maintainanceCharge":document.warehouseForRent.warehouseForRentMaintainenssCharges.value, 
              "securityDeposit":document.warehouseForRent.warehouseForRentSecurityDeposit.value,
             //"NoOfImages":document.warehouseForRent.warehouseForRentNoOfImages.value,                                             
           })
   }).then((response) => response.json())
           .then((responseJson) => {                  
                   console.log(responseJson);
                   if(responseJson.resultStatus == 2){
                     alert("");                    
                   }else if(responseJson.resultStatus == 1){                                                               
                       alert('');                    
                   }else{
                     alert("Error");
                   }
                   return responseJson.movies;
           })
           .catch((error) => {
                   console.error(error);
           });
       }
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Warehousing for Rent
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="warehouseForRentLongDescription"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Status</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="New"
                    name="warehouseForRentStatus"
                    value="New"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    name="warehouseForRentStatus"
                    value="Resale"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    name="warehouseForRentStatus"
                    value="UnderConstructions"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    name="warehouseForRentStatus"
                    value="ReadyToMoveIn"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    name="warehouseForRentStatus"
                    value="BuiltToSuit"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    name="warehouseForRentStatus"
                    value="BareShell"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Furnishing</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    name="warehouseForRentFurnishing"
                    value="Furnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    name="warehouseForRentFurnishing"
                    value="SemiFurnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    name="warehouseForRentFurnishing"
                    value="UnFurnished"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentRooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Floor Number</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentFloorNumber"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Balconies</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentBalconies"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Bath Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentBathrooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Carpet Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentCarpetArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Super built Up Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentSuperBuiltupArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Row>
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="1"
                    id="warehouseForRentLength"
                    required
                  />
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="1"
                    id="warehouseForRentBreadth"
                    required
                  />
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="warehouseForRentFacing"
                      value="North"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="warehouseForRentFacing"
                      value="South"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="warehouseForRentFacing"
                      value="East"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="warehouseForRentFacing"
                      value="West"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="warehouseForRentFacing"
                      value="NorthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="warehouseForRentFacing"
                      value="NorthWest"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="warehouseForRentFacing"
                      value="SouthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="warehouseForRentFacing"
                      value="SouthWest"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentRoadFacingWidth"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentGroundWaterLevel"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Amenities</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="A/c"
                      name="warehouseForRentAmenities"
                      value="AC"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="GYM"
                      name="warehouseForRentAmenities"
                      value="GYM"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Laundry"
                      name="warehouseForRentAmenities"
                      value="Laundry"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="warehouseForRentAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="WiFi"
                      name="warehouseForRentAmenities"
                      value="WiFi"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Security"
                      name="warehouseForRentAmenities"
                      value="Security"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Pool"
                      name="warehouseForRentAmenities"
                      value="Pool"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="warehouseForRentAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other Aminities</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentOtherAmenities"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other tags</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentOtherTags"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Agreement Period</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="3 months"
                      name="warehouseForRentAgreementPeriod"
                      value="3 months"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="6 months"
                      name="warehouseForRentAgreementPeriod"
                      value="6 months"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="12 months"
                      name="warehouseForRentAgreementPeriod"
                      value="12 months"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="2 years"
                      name="warehouseForRentAgreementPeriod"
                      value="2 years"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Pereference</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Veg"
                      name="warehouseForRentPereference"
                      value="Veg"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Non Veg"
                      name="warehouseForRentPereference"
                      value="NonVeg"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No perference"
                      name="warehouseForRentPereference"
                      value="NoPreference"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label=""
                      name="warehouseForRentPereference"
                      value=""
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Tenent habits preference</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Smoking"
                      name="warehouseForRentTenentHabitsPreference"
                      value="Smoking"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Drinking"
                      name="warehouseForRentTenentHabitsPreference"
                      value="NoDrinking"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Smoking"
                      name="warehouseForRentTenentHabitsPreference"
                      value="NoSmoking"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Preference"
                      name="warehouseForRentTenentHabitsPreference"
                      value="NoPreference"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label><br/>
                <Form.Label>Minimum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Min Price"
                  id="warehouseForRentMinPrice"
                  required
                /><br/>
                <Form.Label>Maximum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Max Price"
                  id="warehouseForRentMaxPrice"
                  required
                />
                    </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Maintainenss charges/month</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentMaintainenssCharges"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Security Deposit</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForRentSecurityDeposit"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  id="warehouseForRentAboutLocation"
                  required
                />
                <ButtonToolbar>
                  <ButtonGroup>
                    <Button>
                      <i className="fas fa-align-left" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-center" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-right" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-justify" />
                    </Button>
                  </ButtonGroup>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  id="warehouseForRentAboutProperty"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  id="warehouseForRentOther"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="11">
                <Button
                  className="button1 btn-danger btnFullwidth"
                  onClick={e => {e.preventDefault(); {warehouseForRentSubmit()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }