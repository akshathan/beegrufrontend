import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class ForgotPassword extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
      
   }
   
  
  
    render() {
      return (
        <Modal
          {...this.props}
          size="md"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Forgot Password</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12">
                <Form.Label>Enter Email id or Password </Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Enter Email id or Password"
                  required
                />
              </Form.Group>
              
                
               
                  <Button
                    className="btn btn-danger"
                    type="submit"
                    size="md"
                   
                  >
                    Reset
                  </Button>
                
             
            </Form>
            
           
  
          </Modal.Body>
        </Modal>
      );
    }
  }