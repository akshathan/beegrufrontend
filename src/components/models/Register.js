import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,

} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";
import OtpModal from "./OtpModal.js"
var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Register extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = { validated: false };
    this.state = { modalShow: false };
  }
 
  handleSubmit(event) {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }
    this.setState({ validated: true });
    
  }

  handleGameClik() {
    this.setState( {disabled: !this.state.disabled} )
  } 

  render() {
    const { validated } = this.state;
    let modalClose = () =>
      this.setState({
        modalShow1: false
      });

      let otpModal = () =>
      this.setState({
        modalShow1: true
      });

      let closeRegistrationModal = () =>
      this.setState({
        modalShow: false
      });

      function onClick() {
        if(document.registration.regPhoneNumber.value ==""){
          alert("Please enter your Mobile Number")
        }else if(document.registration.regPhoneNumber.value.length <= 9){
          alert("Please enter valid mobile number")
        }else{          
          sendOTP();
        }
      }


      function sendOTP(){
        // alert(document.registration.regPhoneNumber.value);
        fetch('https://aceimagingandvideo.com/beegru/website/sendOtp', {
          method: 'POST',
          async: false,
          headers: {
                  //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({                  
                  "mobileNumber": document.registration.regPhoneNumber.value                 
          })
      }).then((response) => response.json())
          .then((responseJson) => {
                  console.log(responseJson);
                  if(responseJson.resultStatus == 2){
                    alert("Mobile Number Already Registered");
                    // closeOtpModal();
                  }else if(responseJson.resultStatus == 1){
                    otpModal();
                  } else{
                    alert("Server Error");
                  }               
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
      }

      function verifyRegistration(){
        console.log(document.registration.regLocation.value);
        console.log(document.registration.regMessage.value);
        if(statusVariable == 1){
        fetch('https://aceimagingandvideo.com/beegru/website/signup', {
                            method: 'POST',
                            async: false,
                            headers: {
                                    //"Authorization": "Basic " + btoa("8970685540"+ ":" +"123456"),
                                    Accept: 'application/json',
                                    'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                    "name": document.registration.regName.value,
                                    "mobileNumber": document.registration.regPhoneNumber.value,
                                    "emailId": document.registration.regEmail.value,
                                    "location": document.registration.regLocation.value,
                                    "password": document.registration.regPassword.value,
                                    "address": document.registration.regAddress.value,
                                    "aboutme": document.registration.regAboutMe.value,
                                    "message": document.registration.regMessage.value
                            })
                    }).then((response) => response.json())
                            .then((responseJson) => {
                                    console.log(responseJson);
                                    console.log("Response",responseJson.resultStatus);
                                    if(responseJson.resultStatus == 2){
                                      alert("Email ID already Registered");
                                      //document.getElementById("registrationForm").reset();
                                    }else if(responseJson.resultStatus == 1){
                                        alert("Registration Successfull");
                                        document.getElementById("registrationForm").reset();
                                        window.location.reload();
                                        closeRegistrationModal();
                                    }else{
                                      alert("Error");
                                    }
                                    return responseJson.movies;
                            })
                            .catch((error) => {
                                    console.error(error);
                            });
                          }else{
                          alert("Please verify your mobile number");
                          }
      }

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            noValidate
            validated={validated}
            onSubmit={e => this.handleSubmit(e)}
            name="registration"
            id="registrationForm"
          >
            <Form.Row>
              <Form.Group as={Col} md="6" sm="6">
                <Button type="submit" className="loginfacebook" size="md" block>
                  <i className="fab fa-facebook-f"></i> Facebook
                </Button>
              </Form.Group>
              <Form.Group as={Col} md="6" sm="6">
                <Button
                  variant="white gray border"
                  type="submit"
                  size="md"
                  block
                >
                  <i className="fab fa-google"></i> Google
                </Button>
              </Form.Group>
            </Form.Row>
            <hr className="hr-text" data-content=" or "></hr>
            <Form.Group as={Col} md="12" controlId="validationCustom01">
              <Form.Label>NAME * </Form.Label>
              <Form.Control
                className="controll"
                required
                type="text"
                placeholder="Name"
                id="regName"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
           
            <Form.Group as={Col} md="12" controlId="validationCustom03">
              <Form.Label>PHONE *</Form.Label>
              <InputGroup className="mb-3">
                <Form.Control
                  className="controll"                  
                  type="text"
                  placeholder="Phone"
                  id="regPhoneNumber"
                  disabled = {(this.state.disabled)? "disabled" : ""}                  
                />
                <Form.Control.Feedback type="invalid">
                  must be 10 numbers. Please provide a valid Phone number.
                </Form.Control.Feedback>
                <InputGroup.Append>
                  <Button variant="outline-secondary" className="btn btn-danger"
                  onClick={onClick}>Sent OTP</Button>
                </InputGroup.Append>
              </InputGroup>
              <OtpModal show={this.state.modalShow1} onHide={modalClose} />
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom04">
              <Form.Label>EMAIL *</Form.Label>
              <Form.Control
                className="controll"
                type="email"
                placeholder="Email"
                id="regEmail"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Email id.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>PASSWORD *</Form.Label>
              <Form.Control
                className="controll"
                type="password"
                placeholder="Password"
                id="regPassword"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
              <Form.Text className="red">
                Must be more than 8 Characters.
              </Form.Text>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom05">
              <Form.Label>LOCATION *</Form.Label>

              <Form.Control
                className="controll"
                type="Location"
                as="select"
                placeholder="location"
                id="regLocation"
                required
              >
                <option value="Bangalore">Bangalore</option>
                <option value="Mysore">Mysore</option>
                <option value="Chitradurga">Chitradurga</option>
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please provide a valid password.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom06">
              <Form.Label>ADDRESS *</Form.Label>
              <Form.Control
                className="controll"
                type="text"
                placeholder="Address"
                id="regAddress"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid Address.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom07">
              <Form.Label>About me *</Form.Label>
              <Form.Control
                className="controll"
                type="textarea"
                as="textarea"
                placeholder="About me "
                id="regAboutMe"
                required
              />
              <Form.Control.Feedback type="invalid">
                write About me.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} md="12" controlId="validationCustom08">
              <Form.Label>
                How can Beegru help with respect to your property needs?
              </Form.Label>
              <Form.Control
                className="controll"
                type="textarea"
                as="textarea"
                placeholder="Write a message "
                id="regMessage"
                required
              />
              <Form.Control.Feedback type="invalid">
                Please provide a valid message, atleast 10 words.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group>
              <Form.Check
              className="bordernone"
                required
                label="I have read and agree to the Privacy Policy and 
                 Terms of use*"
                feedback="You must agree before submitting."
              />
            </Form.Group>

            <Form.Group>
              <Button type="submit"  onClick={verifyRegistration} className="btn btn-danger" size="md" block>
                Register
              </Button>
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}