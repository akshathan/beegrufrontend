import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup,
 
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Warehousingforsale extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
    render() {
      function warehouseForSaleSubmit(){              
        var items=document.getElementsByName('warehouseForSaleAmenities');
        var warehouseForSaleAmenitiesList="";
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox' && items[i].checked==true)
          warehouseForSaleAmenitiesList+=items[i].value+",";
        }
        console.log("Selected Amenities : "+warehouseForSaleAmenitiesList);     
        fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
           method: 'POST',
           async: false,
           headers: {                               
                   Accept: 'application/json',
                   'Content-Type': 'application/json',
           },
           body: JSON.stringify({
             "propertyName":document.uploadProperty.propertyName.value, 
             "propertyLocation":document.uploadProperty.propertyLocation.value,
             "propertyLandmark":document.uploadProperty.propertyLandmark.value,
             "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
             "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
             "shortDescription":document.uploadProperty.propertyDescription.value,
             "status":document.querySelector('input[name=warehouseForSaleStatus]:checked').value,
             "furnishing":document.querySelector('input[name=warehouseForSaleFurnishing]:checked').value, 
             "bathrooms":document.warehouseForSale.warehouseForSaleBathrooms.value,
             "carpetArea":document.warehouseForSale.warehouseForSaleCarpetArea.value,
             "superBuiltupArea":document.warehouseForSale.warehouseForSaleSuperBuiltupArea.value,
             "length":document.warehouseForSale.warehouseForSaleLength.value,
             "breadth":document.warehouseForSale.warehouseForSaleBreadth.value,
             "roadFacingWidth":document.warehouseForSale.warehouseForSaleRoadFacingWidth.value,
             "waterSupply":document.querySelector('input[name=warehouseForSaleWaterSupply]:checked').value, 
             "groundWaterLevel":document.warehouseForSale.warehouseForSaleGroundWaterLevel.value,
             "amenities":warehouseForSaleAmenitiesList, 
             "aboutLocation":document.warehouseForSale.warehouseForSaleAboutLocation.value,
             "aboutProperty":document.warehouseForSale.warehouseForSaleAboutProperty.value, 
             "other":document.warehouseForSale.warehouseForSaleOther.value,
             "rooms":document.warehouseForSale.warehouseForSaleRooms.value, 
             "floorNumber":document.warehouseForSale.warehouseForSaleFloorNumber.value, 
             "balconies":document.warehouseForSale.warehouseForSaleBalconies.value,
             "facing":document.querySelector('input[name=warehouseForSaleFacing]:checked').value,
             "otherTag":document.warehouseForSale.warehouseForSaleOtherTags.value,
             "persons":document.warehouseForSale.warehouseForSalePersons.value,           
             "minPrice":document.warehouseForSale.warehouseForSaleMinPrice.value,
             "maxPrice":document.warehouseForSale.warehouseForSaleMaxPrice.value,           
             "longDescription":document.warehouseForSale.warehouseForSaleLongDescription.value,
            // "propertyVicinity":document.warehouseForSale.warehouseForSalePropertyVicinity.value,
             "coLiving":document.querySelector('input[name=warehouseForSaleCoLiving]:checked').value,
             "maintainanceCharge":document.warehouseForSale.warehouseForSaleMaintainenssCharges.value, 
             "securityDeposit":document.warehouseForSale.warehouseForSaleSecurityDeposit.value, 
             //"NoOfImages":document.warehouseForSale.warehouseForSaleNoOfImages.value,                                             
           })
   }).then((response) => response.json())
           .then((responseJson) => {                  
                   console.log(responseJson);
                   if(responseJson.resultStatus == 2){
                     alert("");                    
                   }else if(responseJson.resultStatus == 1){                                                               
                       alert('');                    
                   }else{
                     alert("Error");
                   }
                   return responseJson.movies;
           })
           .catch((error) => {
                   console.error(error);
           });
       }
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Warehousing for Sale
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="warehouseForSaleLongDescription"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Status</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="New"
                    name="warehouseForSaleStatus"
                    value="New"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    name="warehouseForSaleStatus"
                    value="Resale"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    name="warehouseForSaleStatus"
                    value="UnderConstructions"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    name="warehouseForSaleStatus"
                    value="ReadyToMoveIn"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    name="warehouseForSaleStatus"
                    value="BuiltToSuit"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    name="warehouseForSaleStatus"
                    value="BareShell"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Furnishing</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    name="warehouseForSaleFurnishing"
                    value="Furnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    name="warehouseForSaleFurnishing"
                    value="SemiFurnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    name="warehouseForSaleFurnishing"
                    value="UnFurnished"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleRooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Floor Number</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleFloorNumber"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Balconies</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleBalconies"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Bath Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleBathrooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Carpet Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleCarpetArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Super built Up Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleSuperBuiltupArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Row>
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="1"
                    id="warehouseForSaleLength"
                    required
                  />
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="1"
                    id="warehouseForSaleBreadth"
                    required
                  />
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="warehouseForSaleFacing"
                      value="North"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="warehouseForSaleFacing"
                      value="South"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="warehouseForSaleFacing"
                      value="East"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="warehouseForSaleFacing"
                      value="West"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="warehouseForSaleFacing"
                      value="NorthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="warehouseForSaleFacing"
                      value="NorthWest"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="warehouseForSaleFacing"
                      value="SouthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="warehouseForSaleFacing"
                      value="SouthWest"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleRoadFacingWidth"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleGroundWaterLevel"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Amenities</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="A/c"
                      name="warehouseForSaleAmenities"
                      value="AC"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="GYM"
                      name="warehouseForSaleAmenities"
                      value="GYM"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Laundry"
                      name="warehouseForSaleAmenities"
                      value="Laundry"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="warehouseForSaleAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="WiFi"
                      name="warehouseForSaleAmenities"
                      value="WiFi"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Security"
                      name="warehouseForSaleAmenities"
                      value="Security"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Pool"
                      name="warehouseForSaleAmenities"
                      value="Pool"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="warehouseForSaleAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other Aminities</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleOtherAmenities"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other tags</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleOtherTags"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label><br/>
                <Form.Label>Minimum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Min Price"
                  id="warehouseForSaleMinPrice"
                  required
                /><br/>
                <Form.Label>Maximum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Max Price"
                  id="warehouseForSaleMaxPrice"
                  required
                />
                    </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Maintainenss charges/month</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleMaintainenssCharges"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Security Deposit</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="warehouseForSaleSecurityDeposit"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  id="warehouseForSaleAboutLocation"
                  required
                />
                <ButtonToolbar>
                  <ButtonGroup>
                    <Button>
                      <i className="fas fa-align-left" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-center" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-right" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-justify" />
                    </Button>
                  </ButtonGroup>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  id="warehouseForSaleAboutProperty"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  id="warehouseForSaleOther"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="11">
                <Button
                  className="button1 btn-danger btnFullwidth"
                  onClick={e => {e.preventDefault(); {warehouseForSaleSubmit()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }