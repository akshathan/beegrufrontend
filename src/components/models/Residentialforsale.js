import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Residentialforsale extends React.Component {
    render() {
      function residentialForSaleSubmit(){              
        var items=document.getElementsByName('residentialForSaleAmenities');
        var residentialForSaleAmenitiesList="";
        for(var i=0; i<items.length; i++){
          if(items[i].type=='checkbox' && items[i].checked==true)
          residentialForSaleAmenitiesList+=items[i].value+",";
        }
        console.log("Selected Amenities : "+residentialForSaleAmenitiesList);     
        fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
           method: 'POST',
           async: false,
           headers: {                               
                   Accept: 'application/json',
                   'Content-Type': 'application/json',
           },
           body: JSON.stringify({
             "propertyName":document.uploadProperty.propertyName.value, 
             "propertyLocation":document.uploadProperty.propertyLocation.value,
             "propertyLandmark":document.uploadProperty.propertyLandmark.value,
             "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
             "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
             "shortDescription":document.uploadProperty.propertyDescription.value,
             "status":document.querySelector('input[name=residentialForSaleStatus]:checked').value,
             "furnishing":document.querySelector('input[name=residentialForSaleFurnishing]:checked').value, 
             "bathrooms":document.residentialForSale.residentialForSaleBathrooms.value,
             "carpetArea":document.residentialForSale.residentialForSaleCarpetArea.value,
             "superBuiltupArea":document.residentialForSale.residentialForSaleSuperBuiltupArea.value,
             "length":document.residentialForSale.residentialForSaleLength.value,
             "breadth":document.residentialForSale.residentialForSaleBreadth.value,
             "roadFacingWidth":document.residentialForSale.residentialForSaleRoadFacingWidth.value,
             "waterSupply":document.querySelector('input[name=residentialForSaleWaterSupply]:checked').value, 
             "groundWaterLevel":document.residentialForSale.residentialForSaleGroundWaterLevel.value,
             "amenities":residentialForSaleAmenitiesList, 
             "aboutLocation":document.residentialForSale.residentialForSaleAboutLocation.value,
             "aboutProperty":document.residentialForSale.residentialForSaleAboutProperty.value, 
             "other":document.residentialForSale.residentialForSaleOther.value,
             "rooms":document.residentialForSale.residentialForSaleRooms.value, 
             "floorNumber":document.residentialForSale.residentialForSaleFloorNumber.value, 
             "balconies":document.residentialForSale.residentialForSaleBalconies.value,
             "facing":document.querySelector('input[name=residentialForSaleFacing]:checked').value,
             "otherTag":document.querySelector('input[name=residentialForSaleOtherTags]:checked').value,
             "persons":document.residentialForSale.residentialForSalePersons.value,           
             "minPrice":document.residentialForSale.residentialForSaleMinPrice.value,
             "maxPrice":document.residentialForSale.residentialForSaleMaxPrice.value,           
             "longDescription":document.residentialForSale.residentialForSaleLongDescription.value,
             "propertyVicinity":document.residentialForSale.residentialForSalePropertyVicinity.value,
             "coLiving":document.querySelector('input[name=residentialForSaleCoLiving]:checked').value,
             "NoOfImages":document.residentialForSale.residentialForSaleNoOfImages.value,                                             
           })
   }).then((response) => response.json())
           .then((responseJson) => {                  
                   console.log(responseJson);
                   if(responseJson.resultStatus == 2){
                     alert("");                    
                   }else if(responseJson.resultStatus == 1){                                                               
                       alert('');                    
                   }else{
                     alert("Error");
                   }
                   return responseJson.movies;
           })
           .catch((error) => {
                   console.error(error);
           });
       }
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Residential for sale
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form name="residentialForSale" id="residentialForSaleForm">
              <Form.Group as={Col} md="12">
                <Form.Label>Type</Form.Label>
  
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Apartment"
                      name="residentialForSaleType"  
                      value="Apartment"                  
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Land"
                      name="residentialForSaleType"
                      value="Land"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Independent House"
                      name="residentialForSaleType"
                      value="IndependentHouse"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Plots"
                      name="residentialForSaleType"
                      value="Plots"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Farm House"
                      name="residentialForSaleType"
                      value="FarmHouse"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Co-Living"
                      name="residentialForSaleType"
                      value="CoLiving"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="other"
                      name="residentialForSaleType"
                      value="other"
                    />
                  </Form.Group>
                  <Form.Group as={Col}></Form.Group>
                </Form.Row>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="other"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="residentialForSaleLongDescription"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Status</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="New"
                      name="residentialForSaleStatus"
                      value="New"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Resale"
                      name="residentialForSaleStatus"
                      value="Resale"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Under Constructions"
                      name="residentialForSaleStatus"
                      value="UnderConstructions"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Ready to move in"
                      name="residentialForSaleStatus"
                      value="ReadyToMoveIn"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Built to Suit"
                      name="residentialForSaleStatus"
                      value="BuilttoSuit"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Bare Shell"
                      name="residentialForSaleStatus"
                      value="BareShell"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Furnishing</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    name="residentialForSaleFurnishing"
                    value="Furnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    name="residentialForSaleFurnishing"
                    value="SemiFurnished"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    name="residentialForSaleFurnishing"
                    value="UnFurnished"
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleRooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>if Co-Living Person</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSalePersons"
                  required
                />
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Private Cabin"
                      name="residentialForSaleCoLiving"
                      value="PrivateCabin"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Shared Work Space"
                      name="residentialForSaleCoLiving"
                      value="SharedWorkSpace"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Floor Number</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleFloorNumber"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Balconies</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleBalconies"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Bath Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleBathrooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Carpet Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleCarpetArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Super built Up Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleSuperBuiltupArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Control
                      type="text"
                      label="Length"
                      className="controll"
                      placeholder="Length"
                      id="residentialForSaleLength"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Control
                      type="text"
                      label="Breadth"
                      className="controll"
                      placeholder="Breadth"
                      id="residentialForSaleBreadth"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="residentialForSaleFacing"
                      value="North"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="residentialForSaleFacing"
                      value="South"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="residentialForSaleFacing"
                      value="East"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="residentialForSaleFacing"
                      value="West"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="residentialForSaleFacing"
                      value="NorthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="residentialForSaleFacing"
                      value="NorthWest"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="residentialForSaleFacing"
                      value="SouthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="residentialForSaleFacing"
                      value="SouthWest"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleRoadFacingWidth"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleGroundWaterLevel"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Amenities</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="A/c"
                      name="residentialForSaleAmenities"
                      value="AC"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="GYM"
                      name="residentialForSaleAmenities"
                      value="GYM"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Laundry"
                      name="residentialForSaleAmenities"
                      value="Laundry"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="residentialForSaleAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="WiFi"
                      name="residentialForSaleAmenities"
                      value="WiFi"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Security"
                      name="residentialForSaleAmenities"
                      value="Security"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="Pool"
                      name="residentialForSaleAmenities"
                      value="Pool"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"
                      label="CCTV"
                      name="residentialForSaleAmenities"
                      value="CCTV"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other Aminities</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForSaleOtherAmenities"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Vicinity</Form.Label>
                <Form.Label>Type</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="type"
                  required
                />
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="residentialForSalePropertyVicinity"
                  required
                ></Form.Control>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other tags</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NRI"
                      name="residentialForSaleOtherTags"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Mansion"
                      name="residentialForSaleOtherTags"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>                                             
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  id="residentialForSaleAboutLocation"
                  required
                />             
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  id="residentialForSaleAboutProperty"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  id="residentialForSaleOther"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label><br/>
                <Form.Label>Minimum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Min Price"
                  id="residentialForSaleMinPrice"
                  required
                /><br/>
                <Form.Label>Maximum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Max Price"
                  id="residentialForSaleMaxPrice"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="11">
                <Button
                  className="button1 btn-danger btnFullwidth"
                  onClick={e => {e.preventDefault(); {residentialForSaleSubmit()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }
  