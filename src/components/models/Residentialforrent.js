import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Residentialforrent extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
    render() {
      function residentialForRentSubmit(){  
       // alert(document.querySelector('input[name=residentialForRentStatus]:checked').value);      
       var items=document.getElementsByName('residentialForRentAmenities');
       var residentialForRentAmenitiesList="";
       for(var i=0; i<items.length; i++){
         if(items[i].type=='checkbox' && items[i].checked==true)
         residentialForRentAmenitiesList+=items[i].value+",";
       }
       console.log("Selected Amenities : "+residentialForRentAmenitiesList);     
       fetch('https://aceimagingandvideo.com/beegru/login/saveProperties', {
          method: 'POST',
          async: false,
          headers: {                               
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "propertyName":document.uploadProperty.propertyName.value, 
            "propertyLocation":document.uploadProperty.propertyLocation.value,
            "propertyLandmark":document.uploadProperty.propertyLandmark.value,
            "propertyType":document.querySelector('input[name=propertyType]:checked').value,  
            "propertyStatus":document.querySelector('input[name=propertyStatus]:checked').value, 
            "shortDescription":document.uploadProperty.propertyDescription.value,
            "status":document.querySelector('input[name=residentialForRentStatus]:checked').value,
            "furnishing":document.querySelector('input[name=residentialForRentFurnishing]:checked').value, 
            "bathrooms":document.residentialForRent.residentialForRentBathrooms.value,
            "carpetArea":document.residentialForRent.residentialForRentCarpetArea.value,
            "superBuiltupArea":document.residentialForRent.residentialForRentSuperBuiltupArea.value,
            "length":document.residentialForRent.residentialForRentLength.value,
            "breadth":document.residentialForRent.residentialForRentBreadth.value,
            "roadFacingWidth":document.residentialForRent.residentialForRentRoadFacingWidth.value,
            "waterSupply":document.querySelector('input[name=residentialForRentWaterSupply]:checked').value, 
            "groundWaterLevel":document.residentialForRent.residentialForRentGroundWaterLevel.value,
            "amenities":residentialForRentAmenitiesList, 
            "aboutLocation":document.residentialForRent.residentialForRentAboutLocation.value,
            "aboutProperty":document.residentialForRent.residentialForRentAboutProperty.value, 
            "other":document.residentialForRent.residentialForRentOther.value,
            "rooms":document.residentialForRent.residentialForRentRooms.value, 
            "floorNumber":document.residentialForRent.residentialForRentFloorNumber.value, 
            "balconies":document.residentialForRent.residentialForRentBalconies.value,
            "facing":document.querySelector('input[name=residentialForRentFacing]:checked').value,
            "otherTag":document.querySelector('input[name=residentialForRentOtherTags]:checked').value,
            "persons":document.residentialForRent.residentialForRentPersons.value,
            "tenentType":document.querySelector('input[name=residentialForRentTenentType]:checked').value,
            "petFriendly":document.querySelector('input[name=residentialForRentPetFriendly]:checked').value,
            "agreementPeriod":document.querySelector('input[name=residentialForRentAgreementPeriod]:checked').value, 
            "preference":document.querySelector('input[name=residentialForRentPreference]:checked').value,
            "tenentHabitPreference":document.querySelector('input[name=residentialForRentTenentHabitPreference]:checked').value,
            "minPrice":document.residentialForRent.residentialForRentMinPrice.value,
            "maxPrice":document.residentialForRent.residentialForRentMaxPrice.value,
            "coLiving":document.querySelector('input[name=residentialForRentCoLiving]:checked').value,
            "longDescription":document.residentialForRent.residentialForRentLongDescription.value,
            "propertyVicinity":document.residentialForRent.residentialForRentPropertyVicinity.value,
            "NoOfImages":document.residentialForRent.residentialForRentNoOfImages.value,                                             
          })
  }).then((response) => response.json())
          .then((responseJson) => {                  
                  console.log(responseJson);
                  if(responseJson.resultStatus == 2){
                    alert("");                    
                  }else if(responseJson.resultStatus == 1){                                                               
                      alert('');                    
                  }else{
                    alert("Error");
                  }
                  return responseJson.movies;
          })
          .catch((error) => {
                  console.error(error);
          });
      }
  
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Residential for rent
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form name="residentialForRent" id="residentialForRentForm">
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  id="residentialForRentLongDescription"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12" id="">
                <Form.Label>Status</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="New"
                    value="New"
                    name="residentialForRentStatus"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Resale"
                    value="Resale"
                    name="residentialForRentStatus"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Under Constructions"
                    value="UnderConstructions"
                    name="residentialForRentStatus"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Ready to move in"
                    value="ReadyToMoveIn"
                    name="residentialForRentStatus"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Built to Suit"
                    value="BuiltToSuit"
                    name="residentialForRentStatus"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Bare Shell"
                    value="BareShell"
                    name="residentialForRentStatus"                  
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Furnishing</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="furnished"
                    value="Furnished"
                    name="residentialForRentFurnishing"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="semi-Furnished"
                    value="SemiFurnished"
                    name="residentialForRentFurnishing"                  
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Un-Furnished"
                    value="UnFurnished"
                    name="residentialForRentFurnishing"                  
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentRooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>if Co-Living Person</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentPersons"
                  required
                />
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Private Cabin"
                      value="PrivateCabin"
                      name="residentialForRentCoLiving"                    
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Shared Work Space"
                      value="SharedWorkSpace"
                      name="residentialForRentCoLiving"                    
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Floor Number</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentFloorNumber"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Balconies</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentBalconies"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Bath Rooms</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentBathrooms"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Carpet Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentCarpetArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Super built Up Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentSuperBuiltupArea"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Row>
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="Length"
                    required
                  />
                  <Form.Control
                    as={Col}
                    md="6"
                    className="controll"
                    type="text"
                    placeholder="Breadth"
                    required
                  />
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="residentialForRentFacing"
                      value="North"                    
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="residentialForRentFacing"
                      value="South"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="residentialForRentFacing"
                      value="East"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="residentialForRentFacing"
                      value="West"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="residentialForRentFacing"
                      value="NorthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="residentialForRentFacing"
                      value="NorthWest"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="residentialForRentFacing"
                      value="SouthEast"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="residentialForRentFacing"
                      value="SouthWest"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentRoadFacingWidth"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentGroundWaterLevel"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Amenities</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check                                        
                      type="checkbox"                   
                      label="A/c"
                      name="residentialForRentAmenities"
                      value="AC"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check                    
                      type="checkbox"                    
                      label="GYM"
                      name="residentialForRentAmenities"
                      value="Gym"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check   
                      type="checkbox"                 
                      label="Laundry"
                      name="residentialForRentAmenities" 
                      value="Laundry"                 
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check 
                      type="checkbox"                   
                      label="CCTV"
                      name="residentialForRentAmenities"
                      value="CCTV"                    
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check 
                      type="checkbox"                   
                      label="WiFi"
                      name="residentialForRentAmenities"
                      value="WiFi"                    
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="checkbox"                    
                      label="Security"
                      name="residentialForRentAmenities"
                      value="Security"                    
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check  
                      type="checkbox"                  
                      label="Pool"
                      name="residentialForRentAmenities"  
                      value="Pool"                  
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check    
                      type="checkbox"                
                      label="CCTV"
                      name="residentialForRentAmenities" 
                      value="CCTV"                   
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other Amenities</Form.Label>
               
              
                {this.state.questions.map((question, index) => (
              <span key={index}>
                <Form.Row>
                <Form.Group as={Col} md={"9"}>
                <Form.Control
                  className="controll"
                  type="text"
                  onChange={this.handleText(index)}
                  value={question}
                  
                  required
                />
                </Form.Group>
                <Form.Group as={Col} md={"3"}>
                <Button className="button1 btn-danger" onClick={this.handleDelete(index)}>Delete</Button>
                </Form.Group>
                </Form.Row>
                </span>
              ))}
               <Form.Group as={Col}>
                  <Button className="button1 btn-danger" onClick={this.addQuestion}>+ Add</Button>
               </Form.Group>
  
              </Form.Group>
             
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Other tags</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NRI"
                      name="residentialForRentOtherTags"
                      value="NRI"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Mansion"
                      name="residentialForRentOtherTags"
                      value="Mansion"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Tenant Type</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Company"
                      name="residentialForRentTenantType"
                      value="Company"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Family"
                      name="residentialForRentTenantType"
                      value="Family"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Single"
                      name="residentialForRentTenantType"
                      value="Single"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Preference"
                      name="residentialForRentTenantType"
                      value="NoPreference"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Pet Friendly</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Yes"
                      name="residentialForRentPetFriendly"
                      value="Yes"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No"
                      name="residentialForRentPetFriendly"
                      value="No"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Agreement Period</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="3 months"
                      name="residentialForRentAgreementPeriod"
                      value="ThreeMonths"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="6 months"
                      name="residentialForRentAgreementPeriod"
                      value="SixMonths"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="12 months"
                      name="residentialForRentAgreementPeriod"
                      value="TwelveMonths"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="2 years"
                      name="residentialForRentAgreementPeriod"
                      value="TwoYears"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Pereference</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Veg"
                      name="residentialForRentPreference"
                      value="Veg"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Non Veg"
                      name="residentialForRentPreference"
                      value="NonVeg"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No perference"
                      name="residentialForRentPreference"
                      value="NoPreference"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label=""
                      name="residentialForRentPreference"
                      value=""
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Tenent habits preference</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Smoking"
                      name="residentialForRentTenentHabitsPreference"
                      value="Smoking"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Drinking"
                      name="residentialForRentTenentHabitsPreference"
                      value="NoDrinking"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Smoking"
                      name="residentialForRentTenentHabitsPreference"
                      value="NoSmoking"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="No Preference"
                      name="residentialForRentTenentHabitsPreference"
                      value="NoPreference"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label><br/>
                <Form.Label>Minimum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Min Price"
                  id="residentialForRentMinPrice"
                  required
                /><br/>
                <Form.Label>Maximum</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Max Price"
                  id="residentialForRentMaxPrice"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Maintainenss charges/month</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentMaintainenssCharges"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Security Deposit</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  id="residentialForRentSecurityDeposit"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  id="residentialForRentAboutLocation"
                  required
                />
                <ButtonToolbar>
                  <ButtonGroup>
                    <Button>
                      <i className="fas fa-align-left" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-center" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-right" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-justify" />
                    </Button>
                  </ButtonGroup>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  id="residentialForRentAboutProperty"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  id="residentialForRentOthers"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="11">
                <Button                
                  onClick={e => {e.preventDefault(); {residentialForRentSubmit()}}}
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }