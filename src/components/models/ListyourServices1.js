import React, { Component } from "react";
import {
  Form,
  
  Col,
  Modal,
  Button,

} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class ListyourServices1 extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
    }
  
    render() {
      let modalClose = () =>
        this.setState({
          modalShow7: false,
          modalShow8: false,
          modalShow9: false,
          modalShow10: false,
          modalShow11: false,
          modalShow12: false,
          modalShow13: false,
          modalShow14: false,
          modalShow15: false,
          modalShow16: false,
          modalShow17: false
        });
      return (
        <Modal
          {...this.props}
          size="md"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              List Your Services
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12" controlId="formBasic">
                {/* <Form.Label>I want to</Form.Label> */}
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow7: true })}
                  block
                >
                  Residential for Sale
                </Button>
                <Residentialforsale
                  show={this.state.modalShow7}
                  onHide={modalClose}
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicEmail">
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow8: true })}
                  block
                >
                  Residential for Rent
                </Button>
                <Residentialforrent
                  show={this.state.modalShow8}
                  onHide={modalClose}
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasic">
                {/* <Form.Label>I want to</Form.Label> */}
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow9: true })}
                  block
                >
                  Commercial for Sale
                </Button>
                <Commercialforsale
                  show={this.state.modalShow9}
                  onHide={modalClose}
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicEmail">
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow10: true })}
                  block
                >
                  Commercial for Rent
                </Button>
                <Commercialforrent
                  show={this.state.modalShow10}
                  onHide={modalClose}
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasic">
                {/* <Form.Label>I want to</Form.Label> */}
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow11: true })}
                  block
                >
                  Industrial for Sale
                </Button>
                <Industrialforsale
                  show={this.state.modalShow11}
                  onHide={modalClose}
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicEmail">
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow12: true })}
                  block
                >
                  Industrial for Rent
                </Button>
                <Industrialforrent
                  show={this.state.modalShow12}
                  onHide={modalClose}
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasic">
                {/* <Form.Label>I want to</Form.Label> */}
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow13: true })}
                  block
                >
                  WareHousing for Sale
                </Button>
                <Warehousingforsale
                  show={this.state.modalShow13}
                  onHide={modalClose}
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicEmail">
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow14: true })}
                  block
                >
                  WareHousing for Rent
                </Button>
                <Warehousingforrent
                  show={this.state.modalShow14}
                  onHide={modalClose}
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasic">
                {/* <Form.Label>I want to</Form.Label> */}
                <Button
                  variant="white gray border"
                  size="md"
                  onClick={() => this.setState({ modalShow15: true })}
                  block
                >
                  Agricultural for Sale
                </Button>
                <Agriculturalforsale
                  show={this.state.modalShow15}
                  onHide={modalClose}
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicEmail">
                <Button
                  variant="white gray border"
                  size="md"
                 // onClick={() => this.setState({ modalShow16: true })}
                  block
                >
                  Agricultural for Rent
                </Button>
                <Agriculturalforrent
                  show={this.state.modalShow16}
                  onHide={modalClose}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }
