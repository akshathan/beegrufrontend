import React, { Component } from "react";
import {
  Form,
  Col,
  Button,
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class addOtherAminities extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
   
  
  
    render() {
      
      return (
        <div>
        {this.state.questions.map((question, index) => (
          <span  key={index}>
            <Form.Row>
            <Form.Control
              className="controll"
              type="text"
              onChange={this.handleText(index)}
              value={question}
              placeholder="1"
              required
              as={Col} md="8"
            />
            <Button as={Col} md="4" onClick={this.handleDelete(index)}>Delete</Button>
            </Form.Row>
            </span>
          ))}
           <Button onClick={this.addQuestion}>Add</Button>
  
          </div>
      );
    }
  }
  