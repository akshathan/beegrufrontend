import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";
import UploadImage from "./UploadImage.js"
import Residentialforsale from "./Residentialforsale.js"
import Residentialforrent from "./Residentialforrent.js"
import Commercialforrent from "./Commercialforrent.js"
import Commercialforsale from "./Commercialforsale.js"
import Industrialforrent from "./Industrialforrent.js"
import Industrialforsale from "./Industrialforsale.js"
import Warehousingforrent from "./Warehousingforrent.js"
import Warehousingforsale from "./Warehousingforsale.js"
import Agriculturalforrent from "./Agriculturalforrent.js"
import Agriculturalforsale from "./Agriculturalforsale.js"

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class ListyourProperty extends React.Component { 
  
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
      this.handleOpenModal = this.handleOpenModal.bind(this);
      this.handleCloseModal = this.handleCloseModal.bind(this);
    }
    
    handleOpenModal () {
      this.setState({ showModal: true });
    }
    
    handleCloseModal () {
      this.setState({ modalShow5: false });
    }
  
    getFiles(files){
      this.setState({ files: files })
    }
  
    render() {     
        let modalClose = () =>
        this.setState({
          modalShow7: false,
          modalShow8: false,
          modalShow9: false,
          modalShow10: false,
          modalShow11: false,
          modalShow12: false,
          modalShow13: false,
          modalShow14: false,
          modalShow15: false,
          modalShow16: false,
          modalShow17: false
        });
  
        let closeListYourPropertyModal = () =>
        this.setState({
          modalShow5: false
      });
  
      let residentialForSaleModal = () =>
        this.setState({
          modalShow7: true
      });
  
      let residentialForRentModal = () =>
        this.setState({
          modalShow8: true
      });
  
      let commercialForSaleModal = () =>
        this.setState({
          modalShow9: true
      });
  
      let commercialForRentModal = () =>
        this.setState({
          modalShow10: true
      });
  
      let industrialForSaleModal = () =>
        this.setState({
          modalShow11: true
      });
  
      let industrialForRentModal = () =>
        this.setState({
          modalShow12: true
      });
  
      let warehousingForSaleModal = () =>
        this.setState({
          modalShow13: true
      });
  
      let warehousingForRentModal = () =>
        this.setState({
          modalShow14: true
      });
  
      let agriculturalForSaleModal = () =>
        this.setState({
          modalShow15: true
      });
  
      let agriculturalForRentModal = () =>
        this.setState({
          modalShow16: true
      });
  
      
      function checkPropertyType(){
        var data = new FormData();
        var imagedata = document.querySelector('input[type="file"]').value;
        data.append("data", imagedata); 
  
        //alert("Property Type");
        if((document.querySelector('input[name=propertyType]:checked').value == "Residential") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
          closeListYourPropertyModal();
          residentialForSaleModal();        
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Residential") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
          alert(imagedata);
          residentialForRentModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Commercial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
          commercialForSaleModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Commercial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
          commercialForRentModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Industrial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
          industrialForSaleModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Industrial") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
          industrialForRentModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Warehousing") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
          warehousingForSaleModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Warehousing") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
          warehousingForRentModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Agricultural") && (document.querySelector('input[name=propertyStatus]:checked').value == "Sale")){
          agriculturalForSaleModal();
        }else if((document.querySelector('input[name=propertyType]:checked').value == "Agricultural") && (document.querySelector('input[name=propertyStatus]:checked').value == "Rent")){
          agriculturalForRentModal();
        }else{
          alert("Please Select Property Type and Property Status")
        }
      }     
      return (     
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              List your Property
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form id="addProperty" name="uploadProperty">
              <Form.Group as={Col} md="12" controlId="formBasicName">
                <Form.Label>NAME OF THE PROPERTY *</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Name of the property"
                  id="propertyName"                
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                 <Form.Label>I WANT TO *</Form.Label>
                <Form.Row>
                  <Form.Group as={Col} md="6">
                    <Form.Check
                      type="radio"
                      label="Sale"
                      name="propertyStatus"
                      value="Sale"
                    />
                  </Form.Group>
                  <Form.Group as={Col} md="6">
                    <Form.Check
                      type="radio"
                      label="Rent"
                      name="propertyStatus"
                      value="Rent"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col} md="6">
                    <Form.Check
                      type="radio"
                      label="Find Joint Venture Partner-Investor"
                      name="propertyStatus"
                      value="Venture"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>TYPE OF PROPERTY *</Form.Label>
                <Form.Row>
                  <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Residential"
                    name="propertyType"
                    value="Residential"
                  />
                </Form.Group>
                  <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Commercial"
                    name="propertyType"
                    value="Commercial"
                  />
                </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Warehousing"
                    name="propertyType"
                    value="Warehousing"
                  />
                </Form.Group>
                  <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Industrial"
                    name="propertyType"
                    value="Industrial"
                  />
                </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col} md="6">
                  <Form.Check
                    type="radio"
                    label="Agricultural"
                    name="propertyType"
                    value="Agricultural"
                  />
                </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>LOCATION ADDRESS *</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Location"
                  id="propertyLocation"
                  
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>LANDMARK *</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Landmark"
                  id="propertyLandmark"
                  
                />
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Landmark description"
                  id="propertyDescription"
                  
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>UPLOAD IMAGE *</Form.Label>
                <InputGroup>
                  <UploadImage />
                  {/* <FileBase64
                      multiple={ true }
                      onDone={ this.getFiles.bind(this) } /> */}
                </InputGroup>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>VIDEO URL *</Form.Label>
                <InputGroup>
                  <Form.Control
                    placeholder="https://www.youtube.com/watch?v=sHXBWHON"
                    type="text"                  
                  />
                </InputGroup>
              </Form.Group>
  
              <Form.Group as={Col} md="11">
                <Button      
                 className="button1 btn-danger btnFullwidth"          
                  type="submit"
                  onClick={e => {e.preventDefault(); {checkPropertyType()}}}      
                  size="lg"                          
                  block
                >
                  Next
                </Button>
  
                <Residentialforsale
                  show={this.state.modalShow7}
                  onHide={modalClose}
                />
  
                <Residentialforrent
                                show={this.state.modalShow8}
                                onHide={modalClose}
                />
  
                <Commercialforsale
                                show={this.state.modalShow9}
                                onHide={modalClose}
                />
  
                <Commercialforrent
                                show={this.state.modalShow10}
                                onHide={modalClose}
                />
  
                <Industrialforsale
                                show={this.state.modalShow11}
                                onHide={modalClose}
                />
  
                <Industrialforrent
                                show={this.state.modalShow12}
                                onHide={modalClose}
                />
  
                <Warehousingforsale
                                show={this.state.modalShow13}
                                onHide={modalClose}
                />
  
                <Warehousingforrent
                                show={this.state.modalShow14}
                                onHide={modalClose}
                />
  
                <Agriculturalforsale
                                show={this.state.modalShow15}
                                onHide={modalClose}
                />
  
                <Agriculturalforrent
                                show={this.state.modalShow16}
                                onHide={modalClose}
                />
                <ListyourProperty show={this.state.modalShow5} onHide={modalClose} />
  
              </Form.Group>
            </Form>
          </Modal.Body>        
        </Modal>
        
      );
      // }else{      
      //   return null;
      // }
    }
  }
  