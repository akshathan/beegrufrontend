import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";
import OtpModal from "./OtpModal.js"

import "../../css/rigester.css";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class LoginwithOTP extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
      
   }
   
  
  
    render() {
      let modalClose = () =>
        this.setState({
          modalShow: false
        });
  
      return (
        <Modal
          {...this.props}
          size="sm"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Login with OTP</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12">
                <Form.Label>Enter Mobile number or Email</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Mobile number or Email"
                  required
                />
              </Form.Group>
             
               
                <Button
                      variant="outline-secondary"
                      className="btn btn-danger"
                      onClick={() => this.setState({ modalShow: true })}
                    >
                      Sent OTP
                    </Button>
                 
                <OtpModal show={this.state.modalShow} onHide={modalClose} />
               
                
             
             
            </Form>
            
           
  
          </Modal.Body>
        </Modal>
      );
    }
  }