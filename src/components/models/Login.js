import React, { Component } from "react";
import {
  Form,
  Row,
  Col,
  Modal,
  Button,
  Container,
  Image
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";
import ForgotPassword from "./ForgotPassword.js"
import Register from "../models/Register.js"
import { Link } from "react-router-dom";
import "../../css/rigester.css";
import icon from "../../images/logoIcon.png";
import HomeIcon from "../../images/HomeIcon.png";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class  Login extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { validated: false };
      this.state = { modalShow: false };
    }
  
     handleSubmit(event) {
      const form = event.currentTarget;
       if (form.checkValidity() === false) {
         event.preventDefault();       
          event.stopPropagation();
       }
       this.setState({ validated: true });
       
     }
  
    render() {
      const { validated } = this.state;
      let modalClose = () =>
        this.setState({
          modalShow2: false
        });
  
        let loginmodalClose = () =>
        this.setState({
          modalShow1: false
        });
  
        let registrationModalOpen = () =>
        this.setState({
          modalShow2: true
        });
  
        
        function registrationModal(){
          loginmodalClose();
          registrationModalOpen();
        }
  
        function verifyLogin(){        
          fetch('https://aceimagingandvideo.com/beegru/website/login', {
            method: 'POST',
            async: false,
            headers: {                               
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                    "mobileNumber": document.login.loginEmailId.value,
                    "password": document.login.loginPassword.value                                
            })
    }).then((response) => response.json())
            .then((responseJson) => {                  
                    console.log(responseJson);
                    if(responseJson.resultStatus == 2){
                      alert("Invalid Credentials");                    
                    }else if(responseJson.resultStatus == 1){                                           
                        localStorage.setItem('loginStatus', 'loginStatus');
                        localStorage.setItem('mobileNumber', document.login.loginPassword.value);
                        alert('Login Successful');
                        document.getElementById("loginForm").reset();
                        window.location.reload();
                        //onHide={modalClose}                 
                    }else if(responseJson.resultStatus == 3){
                      alert("Not approved by admin");                    
                   }else{
                      alert("Error");
                    }
                    return responseJson.movies;
            })
            .catch((error) => {
                    console.error(error);
            });
        }
  
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">Login</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col className="Loginmodaldiv1">
                  <Image src={icon} fluid />
                  <Image src={HomeIcon} fluid />
  
                  <p>
                    Find the best matches for you Make the most of high seller
                    scores Experience a joyful journey
                  </p>
                </Col>
                <Col className="Loginmodaldiv2">
                  <Form
                    noValidate
                    validated={validated}
                    onSubmit={e => this.handleSubmit(e)}
                    name="login"
                    id="loginForm"
                  >
                    <Form.Group as={Col} md="12" controlId="validationCustom11">
                      <Form.Label>EMAIL or MOBILE </Form.Label>
                      <Form.Control
                        className="controll"
                        type="email"
                        placeholder="Email or Mobile"
                        id="loginEmailId"
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        must be 8 characters strong. Please provide a valid email.
                      </Form.Control.Feedback>
                    </Form.Group>
  
                    <Form.Group as={Col} md="12" controlId="validationCustom12">
                      <Form.Label>PASSWORD</Form.Label>
                      <Form.Control
                        className="controll"
                        type="password"
                        placeholder="Password"
                        id="loginPassword"
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        must be 8 characters strong. Please provide a valid
                        Password.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>
                        <Link href="#">Login with OTP</Link>
                      </Form.Label>
                    </Form.Group>
                    <Form.Group as={Col} md="11">
                      <Button
                        type="submit"                      
                        onClick={e => {e.preventDefault();this.setState({modalShow5: false}); {verifyLogin()}}}
                        className="btnFullwidth loginButton"
                        size="md"
                        block
                      >
                        Login
                      </Button>
  
                      <hr className="hr-text" data-content="or login via"></hr>
  
                      <Button
                        className="btnFullwidth loginfacebook"
                        size="md"
                        block
                      >
                        <i className="fab fa-facebook-f"></i> Facebook
                      </Button>
  
                      <Button
                        variant="white gray border"
                        className="btnFullwidth"
                        size="md"
                        block
                      >
                        <i className="fab fa-google"></i> Google
                      </Button>
                    </Form.Group>
                    <Form.Group className="login">
                      <p>
                        {" "}
                        Don't have an account? Please
                        {/* <Link href="#"> Register</Link> */}
                        <Link onClick={registrationModal}>
                          Register
                        </Link>
                        <Register
                          show={this.state.modalShow2}
                          onHide={modalClose}
                        />
                      </p>
  
                      <p>
                       
                        <Link onClick={() => this.setState({ modalShow19: true })}>
                        Forgot your password?
                        </Link>
                        <ForgotPassword
                          show={this.state.modalShow19}
                          onHide={modalClose}
                        />
                      </p>
  
                    </Form.Group>
                  </Form>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
      );
    }
  }
  