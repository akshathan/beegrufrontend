import React, { Component } from "react";
import {
  Form,
  Col,
  Modal,
  Button,
 
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";

import UploadImage from "./UploadImage.js"
import "../../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class ListyourServices extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { validated: false };
    }
  
    handleSubmit(event) {
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      this.setState({ validated: true });
    }
  
    render() {
      const { validated } = this.state;
  
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              List Your Services
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form
              noValidate
              validated={validated}
              onSubmit={e => this.handleSubmit(e)}
            >
              <Form.Group as={Col} md="12" controlId="validationCustom23">
                <Form.Label>SERVICE TYPE *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="speciality"
                  as="select"
                  placeholder="Speciality"
                  required
                >
                  <option>ARCHITECTURE</option>
                  <option>Lawyer</option>
                  <option>Interior</option>
                  <option>Banker</option>
                  <option>Other</option>
                </Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="validationCustom21">
                <Form.Label>SERVICE NAME *</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Name"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>UPLOAD IMAGE *</Form.Label>
                <InputGroup>
                  <UploadImage />
                </InputGroup>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>ABOUT</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About your Services"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="validationCustom21">
                <Form.Label>Video URL</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="paste your video url"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description about your Services"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="validationCustom21">
                <Form.Label>Rating For Your Services</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="paste your video url"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="11">
                <Button
                  type="submit"
                  className="btn btn-danger btnFullwidth"
                  size="lg"
                  block
                >
                  Post Your Services
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }