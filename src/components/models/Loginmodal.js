import React, { Component } from "react";
import {
  
  Row,
  Col,
  Modal,
  Button,
  Container,
  Image,
  Card,
  CardDeck
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import icon from "./../images/logoIcon.png";
import HomeIcon from "./../images/HomeIcon.png";
import avtar1 from "./../images/img_avatar.png";
import avtar2 from "./../images/img_avatar2.png";
import avtar3 from "./../images/img_avatar3.png";

var statusVariable= 0;
var status = localStorage.getItem('loginStatus');

export default class Loginmodal extends React.Component {
    constructor(...args) {
      super(...args);
  
      this.state = { modalShow: false };
    }
  
    render() {
      let modalClose = () =>
        this.setState({
          modalShow1: false
        });
  
        
  
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter"></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Container>
              <Row>
                <Col className="Loginmodaldiv1">
                  <Image src={icon} fluid />
                  <Image src={HomeIcon} fluid />
  
                  <p>
                    Find the best matches for you Make the most of high seller
                    scores Experience a joyful journey
                  </p>
                </Col>
                <Col className="Loginmodaldiv2">
                  <p>You are moments away from selling with Beegru</p>
                  <InputGroup className="mb-3 m1">
                    <InputGroup.Prepend>
                      <Button variant="outline">
                        <i className="fa fa-search"></i>
                      </Button>
                    </InputGroup.Prepend>
                    <FormControl aria-describedby="basic-addon1" />
                  </InputGroup>
                  <p>Who are you?</p>
                  <CardDeck>
                    <Card onClick={() => this.setState({ modalShow1: true })}>
                      <Card.Img variant="top" src={avtar1} thumbnail />
                      <Card.Body>
                        <Card.Text>Owner</Card.Text>
                      </Card.Body>
                    </Card>
                    <Card onClick={() => this.setState({ modalShow1: true })}>
                      <Card.Img variant="top" src={avtar2} thumbnail />
                      <Card.Body>
                        <Card.Text>Seller</Card.Text>
                      </Card.Body>
                    </Card>
                    <Card onClick={() => this.setState({ modalShow1: true })}>
                      <Card.Img variant="top" src={avtar3} thumbnail />
                      <Card.Body>
                        <Card.Text>Agent</Card.Text>
                      </Card.Body>
                    </Card>
                  </CardDeck>
                  <Login show={this.state.modalShow1} onHide={modalClose} />
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
      );
    }
  }