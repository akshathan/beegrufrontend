import React from "react";
import {
  Form,
  Col,
  Modal,
  Button,
  ButtonToolbar,
  ButtonGroup
} from "react-bootstrap";
// import FileBase64 from 'react-file-base64';
// import $ from "jquery";


import "../../css/rigester.css";

export default class Agriculturalforrent extends React.Component {
    state = {
      questions: ['Amenities']
    }
  
    handleText = i => e => {
      let questions = [...this.state.questions]
      questions[i] = e.target.value
      this.setState({
        questions
      })
    }
  
    handleDelete = i => e => {
      e.preventDefault()
      let questions = [
        ...this.state.questions.slice(0, i),
        ...this.state.questions.slice(i + 1)
      ]
      this.setState({
        questions
      })
    }
  
    addQuestion = e => {
      e.preventDefault()
      let questions = this.state.questions.concat([''])
      this.setState({
        questions
      })
    }
    render() {
      
      return (
        <Modal
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Agricultural for rent
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Col} md="12" controlId="formBasicspeciality">
                <Form.Label>DESCRIPTION *</Form.Label>
  
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Description"
                  required
                ></Form.Control>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Kharad Area</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Facing</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="N"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="S"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="E"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="W"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NE"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="NW"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SE"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="SW"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12">
                <Form.Label>Road Facing width</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Water supply</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Canal"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Muncipality"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Borewell"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Ground Water level</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Soil type</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Red"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Black"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>
                  Structure <span>super built up area</span>
                </Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Land type</Form.Label>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Wet"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Dry"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Check
                    type="radio"
                    label="Highlands"
                    name="formHorizontalRadios"
                    id=""
                  />
                </Form.Group>
              </Form.Group>
              <Form.Group as={Col} md="12">
                <Form.Label>Irrigation type</Form.Label>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Drip"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check
                      type="radio"
                      label="Sprinkler"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group as={Col}>
                    <Form.Check                   
                      label="Canal"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios1"
                    />
                  </Form.Group>
                  <Form.Group as={Col}>
                    <Form.Check                   
                      label="Pipe"
                      name="formHorizontalRadios"
                      id="formHorizontalRadios2"
                    />
                  </Form.Group>
                </Form.Row>
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Plantation</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="Banana"
                  required
                />
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Plantation description"
                  required
                />
              </Form.Group>
  
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Location</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Location"
                  required
                />
                <ButtonToolbar>
                  <ButtonGroup>
                    <Button>
                      <i className="fas fa-align-left" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-center" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-right" />
                    </Button>
                    <Button>
                      <i className="fas fa-align-justify" />
                    </Button>
                  </ButtonGroup>
                </ButtonToolbar>
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>About Property</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="About Property"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLandmark">
                <Form.Label>Others</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  as="textarea"
                  placeholder="Others"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Pricing</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Maintainenss charges/month</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="formBasicLocation">
                <Form.Label>Security Deposit</Form.Label>
                <Form.Control
                  className="controll"
                  type="text"
                  placeholder="1"
                  required
                />
              </Form.Group>
              <Form.Group as={Col} md="11">
                <Button
                  className="button1 btn-danger btnFullwidth"
                  type="submit"
                  size="lg"
                  block
                >
                  Post my listing
                </Button>
              </Form.Group>
            </Form>
          </Modal.Body>
        </Modal>
      );
    }
  }