	import React from 'react';
import { Redirect } from 'react-router-dom';
import { Route } from 'react-router-dom';
import * as FeatherIcon from 'react-feather';

import { isUserAuthenticated, getLoggedInUser } from '../helpers/authUtils';

// auth
const Login = React.lazy(() => import('../pages/auth/Login'));
const Logout = React.lazy(() => import('../pages/auth/Logout'));
const Register = React.lazy(() => import('../pages/auth/Register'));
const ForgetPassword = React.lazy(() => import('../pages/auth/ForgetPassword'));
const Confirm = React.lazy(() => import('../pages/auth/Confirm'));
// dashboard
const Dashboard = React.lazy(() => import('../pages/dashboard'));
// apps
const Favourite = React.lazy(() => import('../pages/other/Pricing'));
const AssignedStaff = React.lazy(() => import('../pages/tables/Advanced'));
const UploadProperties = React.lazy(() => import('../pages/forms/Upload'));
const UploadedProperties = React.lazy(() => import('../pages/tables/Uploaded'));
const HotProperties = React.lazy(() => import('../pages/tables/HotProperties'));
const UploadDownload = React.lazy(() => import('../pages/forms/Download'));
const ReportIssue = React.lazy(() => import('../pages/forms/Basic'));
const Profile = React.lazy(() => import('../pages/other/Profile/'));



// handle auth and authorization
const PrivateRoute = ({ component: Component, roles, ...rest }) => (
    <Route
        {...rest}
        render={props => {
            if (!isUserAuthenticated()) {
                // not logged in so redirect to login page with the return url
                return <Redirect to={{ pathname: '/account/login', state: { from: props.location } }} />;
            }

            const loggedInUser = getLoggedInUser();
            // check if route is restricted by role
            if (roles && roles.indexOf(loggedInUser.role) === -1) {
                // role not authorised so redirect to home page
                return <Redirect to={{ pathname: '/' }} />;
            }

            // authorised so return component
            return <Component {...props} />;
        }}
    />
);

// root routes
const rootRoute = {
    path: '/',
    exact: true,
    component: () => <Redirect to="/dashboard" />,
    route: PrivateRoute,
};

// dashboards
const dashboardRoutes = {
    path: '/dashboard',
    name: 'Dashboard',
    icon: FeatherIcon.Tv,       
    component: Dashboard,
    roles: ['Admin'],
    route: PrivateRoute
};

// apps
const homeRoutes = {
    path: '/home',
    name: 'Home Page',    
    icon: FeatherIcon.Home,
    component :() => { 
        window.location.href = 'http://localhost:3000'; 
        return null;
    },
    route: PrivateRoute,
    roles: ['Admin'],
};

const favouriteRoutes = {
    path: '/other/pricing',
    name: 'Favourites',    
    icon: FeatherIcon.Heart,
    component: Favourite,
    route: PrivateRoute,
    roles: ['Admin'],
};

const staffRoutes = {
    path: '/tables/advanced',
    name: 'Assigned Staff',
    icon: FeatherIcon.User,
    component: AssignedStaff,
    route: PrivateRoute,
    roles: ['Admin'],
};

// const uploadPropertiesRoutes = {
//     path: '/forms/upload',
//     name: 'Upload Properties',
//     icon: FeatherIcon.Upload,
//     component: UploadProperties,
//     route: PrivateRoute,
//     roles: ['Admin'],
// };

const uploadedPropertiesRoutes = {
    path: '/tables/uploaded',
    name: 'Uploaded Properties',
    icon: FeatherIcon.CheckSquare,
    component: UploadedProperties,
    route: PrivateRoute,
    roles: ['Admin'],
};

// pages
const hotPropertiesRoutes = {
    path: '/tables/hotProperties',
    name: 'Hot Properties',    
    icon: FeatherIcon.Zap,
    component: HotProperties,
    route: PrivateRoute,
    roles: ['Admin'],
};


// components
const uploadDownloadRoutes = {
    path: '/forms/download',
    name: 'Upload / Download',   
    icon: FeatherIcon.Download,
    component: UploadDownload,
    route: PrivateRoute,
    roles: ['Admin'],
};

// charts
const reportIssueRoutes = {
    path: '/forms/basic',
    name: 'Report Issue',
    component: ReportIssue,
    icon: FeatherIcon.AlertTriangle,
	route: PrivateRoute,
    roles: ['Admin'],   
}

const profileRoutes = {
    path: '/pages/profile',
    name: '',
    component: Profile,    
	route: PrivateRoute,
    roles: ['Admin'],   
}

const appRoutes = [homeRoutes, favouriteRoutes, staffRoutes, uploadedPropertiesRoutes, hotPropertiesRoutes, uploadDownloadRoutes, reportIssueRoutes,profileRoutes];
// auth
const authRoutes = {
    path: '/account',
    name: 'Auth',
    children: [
        {
            path: '/account/login',
            name: 'Login',
            component: Login,
            route: Route,
        },
        {
            path: '/account/logout',
            name: 'Logout',
            component :() => { 
                localStorage.setItem('loginStatus', '');
                window.location.href = 'http://localhost:3000'; 
                return null;
            },
            route: Route,
        },
        {
            path: '/account/register',
            name: 'Register',
            component: Register,
            route: Route,
        },
        {
            path: '/account/confirm',
            name: 'Confirm',
            component: Confirm,
            route: Route,
        },
        {
            path: '/account/forget-password',
            name: 'Forget Password',
            component: ForgetPassword,
            route: Route,
        },
    ],
};

// flatten the list of all nested routes
const flattenRoutes = routes => {
    let flatRoutes = [];

    routes = routes || [];
    routes.forEach(item => {
        flatRoutes.push(item);

        if (typeof item.children !== 'undefined') {
            flatRoutes = [...flatRoutes, ...flattenRoutes(item.children)];
        }
    });
    return flatRoutes;
};

// All routes
const allRoutes = [
    rootRoute,
    dashboardRoutes,
    ...appRoutes,
    authRoutes,
];

const authProtectedRoutes = [dashboardRoutes, ...appRoutes];
const allFlattenRoutes = flattenRoutes(allRoutes);
export { allRoutes, authProtectedRoutes, allFlattenRoutes };
