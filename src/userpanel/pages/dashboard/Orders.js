import React from 'react';
import { Card, CardBody, Table, Button } from 'reactstrap';

const Orders = () => {
    return (
        <Card>
            <CardBody className="pb-0">
                <Button className="float-right" size={'sm'} color="primary">
                    <i className='uil uil-export ml-1'></i> Export
                </Button>

                <h5 className="card-title mt-0 mb-0 header-title">Recent Sales</h5>

                <Table hover responsive className="mt-4">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Property</th>
                            <th scope="col">Customer</th>
                            <th scope="col">Price</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>#98754</td>
                            <td>SS SAPPHIRE</td>
                            <td>Otto B</td>
                            <td>₹10.5Cr</td>
                            <td><span className="badge badge-soft-success py-1">Success</span></td>
                        </tr>
                        <tr>
                            <td>#98753</td>
                            <td>AKRUTHI GREEN WOODS</td>
                            <td>Mark P</td>
                            <td>₹12.8Cr</td>
                            <td><span className="badge badge-soft-success py-1">Success</span>
                            </td>
                        </tr>
                        <tr>
                            <td>#98752</td>
                            <td>VAISHNAVI TERRACES</td>
                            <td>Dave B</td>
                            <td>₹20.5Cr</td>
                            <td><span className="badge badge-soft-success py-1">Success</span>
                            </td>
                        </tr>
                        <tr>
                            <td>#98751</td>
                            <td>RELIAABLE GARDENIAA</td>
                            <td>Shreyu N</td>
                            <td>₹8.5Cr</td>
                            <td><span className="badge badge-soft-success py-1">Success</span>
                            </td>
                        </tr>
                        <tr>
                            <td>#98750</td>
                            <td>Aquaviva Hurghada</td>
                            <td>Rik N</td>
                            <td>₹15.2Cr</td>
                            <td><span className="badge badge-soft-success py-1">Success</span>
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </CardBody>
        </Card>
    );
};

export default Orders;
