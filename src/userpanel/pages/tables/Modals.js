import React, { Component } from 'react';
import { Row, Col, Card, CardBody, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import img1 from '../../assets/images/small/vaishnavi.jpg';
import img2 from '../../assets/images/small/saphire.jpg';



class Modals extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modal: false,
        };

        this.toggle = this.toggle.bind(this);
        this.openModalWithSize = this.openModalWithSize.bind(this);
        this.openModalWithClass = this.openModalWithClass.bind(this);
    }

    /**
     * Show/hide the modal
     */
    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    };

    /**
     * Opens large modal
     */
    openModalWithSize = size => {
        this.setState({ size: size, className: null });
        this.toggle();
    };

    /**
     * Opens modal with custom class
     */
    openModalWithClass = className => {
        this.setState({ className: className, size: null });
        this.toggle();
    };

    render() {
        return (
            <Row>
                <Col>
                    <Card>
                        <CardBody>
                             <Button color="success" onClick={() => this.openModalWithSize('lg')}>View</Button>
                            <Modal
                                isOpen={this.state.modal}
                                toggle={this.toggle}
                                className={this.state.className}
                                size={this.state.size}>
                                <ModalHeader toggle={this.toggle}>Photos</ModalHeader>
                                <ModalBody>                                                           
                                    <img className="card-img-top img-fluid" src={img1} alt="" /><br/><br/>
									<img className="card-img-top img-fluid" src={img2} alt="" />
                                   
                                </ModalBody>
                                <ModalFooter>                                    
                                    <Button color="secondary" className="ml-1" onClick={this.toggle}>Cancel</Button>
                                </ModalFooter>
                            </Modal>


                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default Modals;
