import React from 'react';
import { Row, Col, Card, CardBody, Input, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import Modals from './Modals';
import Modals1 from './DescriptionModal';
import Modals3 from './FeaturesModal';
import ToolkitProvider, { Search, CSVExport } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';



const records = [
    {
        id: 1,
        name: 'CAMELLIA PRIDE',
        address: 'Tippasandra Cross, Halasalli Road, Varthur',
        price: '₹ 65L',
		discount: <span className="badge badge-soft-success py-1">10%</span>,
        city: 'Bengaluru',
		pincode: '560025',
		state: 'Karnataka',
		description: <Modals1 />,
		photos:<Modals />,
		features: <Modals3 />
    },
    {
        id: 2,
        name: 'RELIAABLE GARDENIAA',
        address: 'Doddanagmangala, Hosa Road',
        price: '₹ 55L',
		discount: <span className="badge badge-soft-success py-1">20%</span>,
        city: 'Bengaluru',
		pincode: '560075',
		state: 'Karnataka',
		description: <Modals1 />,
		photos:<Modals />,
		features: <Modals3 />
    },
    {
        id: 3,
        name: 'SS SAPPHIRE',
        address: 'Kasavanahalli Main Road, Sarjapur',
        price: '₹ 45L',
		discount: <span className="badge badge-soft-success py-1">15%</span>,
        city: 'Bengaluru',
		pincode: '560035',
		state: 'Karnataka',
		description: <Modals1 />,
		photos:<Modals />,
		features: <Modals3 />
    },
   
];

const columns = [
    {
        dataField: 'id',
        text: 'Sl No.',
        sort: true,
    },
    {
        dataField: 'name',
        text: 'Property Name',
        sort: true,
    },
    {
        dataField: 'address',
        text: 'Address',
        sort: true,
    },    
    {
        dataField: 'price',
        text: 'Price',
        sort: true,
    },
	{
        dataField: 'discount',
        text: 'Discount',
        sort: true,
    },
	{
        dataField: 'city',
        text: 'City',
        sort: true,
    },
	{
        dataField: 'pincode',
        text: 'Pincode',
        sort: true,
    },
	{
        dataField: 'state',
        text: 'State',
        sort: true,
    },
	{
        dataField: 'description',
        text: 'Description',
        sort: true,
    },
	{
        dataField: 'features',
        text: 'Features',
        sort: true,
    },
	{
        dataField: 'photos',
        text: 'Photos',
        sort: true,
    },
];

const defaultSorted = [
    {
        dataField: 'id',
        order: 'asc',
    },
];

const sizePerPageRenderer = ({ options, currSizePerPage, onSizePerPageChange }) => (
    <React.Fragment>
        <label className="d-inline mr-1">Show</label>
        <Input type="select" name="select" id="no-entries" className="custom-select custom-select-sm d-inline col-1"
            defaultValue={currSizePerPage}
            onChange={(e) => onSizePerPageChange(e.target.value)}>
            {options.map((option, idx) => {
                return <option key={idx}>{option.text}</option>
            })}
        </Input>
        <label className="d-inline ml-1">entries</label>
    </React.Fragment>
);



const TableWithSearch = () => {
    const { SearchBar } = Search;
    const { ExportCSVButton } = CSVExport;

    return (
        <Card>
            <CardBody>
                

                <ToolkitProvider
                    bootstrap4
                    keyField="id"
                    data={records}
                    columns={columns}
                    search
                    exportCSV={{ onlyExportFiltered: true, exportAll: false }}>
                    {props => (
                        <React.Fragment>
                            <Row>
                                <Col>
                                    <SearchBar {...props.searchProps} />
                                </Col>
                                <Col className="text-right">
                                    <ExportCSVButton {...props.csvProps} className="btn btn-primary">
                                        Export CSV
                                    </ExportCSVButton>
                                </Col>
                            </Row>

                            <BootstrapTable
                                {...props.baseProps}
                                bordered={false}
                                defaultSorted={defaultSorted}
                                pagination={paginationFactory({ sizePerPage: 5, sizePerPageRenderer: sizePerPageRenderer, sizePerPageList: [{ text: '5', value: 5, }, { text: '10', value: 10 }, { text: '25', value: 25 }, { text: 'All', value: records.length }] })}
                                wrapperClasses="table-responsive"
                            />
                        </React.Fragment>
                    )}
                </ToolkitProvider>
            </CardBody>
        </Card>
    );
};


const Tables = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    <h3>Hot Properties</h3>
                </Col>
            </Row>

            

            <Row>
                <Col>
                    <TableWithSearch />
                </Col>
            </Row>

           
        </React.Fragment>
    );
};

export default Tables;
