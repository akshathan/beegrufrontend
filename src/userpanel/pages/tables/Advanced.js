import React from 'react';
import { Row, Col, Card, CardBody, Input, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search, CSVExport } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';



const records = [
    {
        id: 1025,
        date: '02-01-2020',
        name: 'Adarsh',
		email: 'adarsh23@gmail.com',
		propertyName:'SS SAPPHIRE APARTMENTS' ,
		location: 'Kasavanahalli, Near Wipro Gate',
        status: <span className="badge badge-soft-success py-1">Completed</span>,
        phone: '9632587456',
    },
    {
        id: 2025,
        date: '20-02-2020',
        name: 'Akash',
		email: 'akash1996@gmail.com',
		propertyName:'AKRUTHI GREEN WOODS' ,
		location: 'Jigani, Anekal taluk',
        status: <span className="badge badge-soft-warning py-1">Pending</span>,
        phone: '8963251236',
    },
    {
        id: 1233,
        date: '15-01-2020',
        name: 'Akshay',
		email: 'aksh123@gmail.com',
		propertyName:'VAISHNAVI TERRACES' ,
		location: 'Dollars Colony, Phase 4, J P Nagar',
        status: <span className="badge badge-soft-danger py-1">Failed</span>,
        phone: '8523698745',
    },
   
];

const columns = [
    {
        dataField: 'id',
        text: 'Staff ID',
        sort: true,
    },
    {
        dataField: 'name',
        text: 'Name',
        sort: true,
    },
    {
        dataField: 'phone',
        text: 'Mobile Number',
        sort: true,
    },
	{
        dataField: 'email',
        text: 'Email ID',
        sort: true,
    },
	{
        dataField: 'propertyName',
        text: 'Property Name',
        sort: true,
    },
	{
        dataField: 'location',
        text: 'Location',
        sort: true,
    },
    {
        dataField: 'date',
        text: 'Assigned Date',
        sort: true,
    },
    {
        dataField: 'status',
        text: 'Status',
        sort: true,
    },
];

const defaultSorted = [
    {
        dataField: 'id',
        order: 'asc',
    },
];

const sizePerPageRenderer = ({ options, currSizePerPage, onSizePerPageChange }) => (
    <React.Fragment>
        <label className="d-inline mr-1">Show</label>
        <Input type="select" name="select" id="no-entries" className="custom-select custom-select-sm d-inline col-1"
            defaultValue={currSizePerPage}
            onChange={(e) => onSizePerPageChange(e.target.value)}>
            {options.map((option, idx) => {
                return <option key={idx}>{option.text}</option>
            })}
        </Input>
        <label className="d-inline ml-1">entries</label>
    </React.Fragment>
);



const TableWithSearch = () => {
    const { SearchBar } = Search;
    const { ExportCSVButton } = CSVExport;

    return (
        <Card>
            <CardBody>
                

                <ToolkitProvider
                    bootstrap4
                    keyField="id"
                    data={records}
                    columns={columns}
                    search
                    exportCSV={{ onlyExportFiltered: true, exportAll: false }}>
                    {props => (
                        <React.Fragment>
                            <Row>
                                <Col>
                                    <SearchBar {...props.searchProps} />
                                </Col>
                                <Col className="text-right">
                                    <ExportCSVButton {...props.csvProps} className="btn btn-primary">
                                        Export CSV
                                    </ExportCSVButton>
                                </Col>
                            </Row>

                            <BootstrapTable
                                {...props.baseProps}
                                bordered={false}
                                defaultSorted={defaultSorted}
                                pagination={paginationFactory({ sizePerPage: 5, sizePerPageRenderer: sizePerPageRenderer, sizePerPageList: [{ text: '5', value: 5, }, { text: '10', value: 10 }, { text: '25', value: 25 }, { text: 'All', value: records.length }] })}
                                wrapperClasses="table-responsive"
                            />
                        </React.Fragment>
                    )}
                </ToolkitProvider>
            </CardBody>
        </Card>
    );
};


const Tables = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    <h3>Assigned Staff</h3>
                </Col>
            </Row>

            

            <Row>
                <Col>
                    <TableWithSearch />
                </Col>
            </Row>

           
        </React.Fragment>
    );
};

export default Tables;
