import React, { Component } from 'react';
import { Row, Col, Card, CardBody, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';




class Modals extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modal: false,
        };

        this.toggle = this.toggle.bind(this);
        this.openModalWithSize = this.openModalWithSize.bind(this);
        this.openModalWithClass = this.openModalWithClass.bind(this);
    }

    /**
     * Show/hide the modal
     */
    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    };

    /**
     * Opens large modal
     */
    openModalWithSize = size => {
        this.setState({ size: size, className: null });
        this.toggle();
    };

    /**
     * Opens modal with custom class
     */
    openModalWithClass = className => {
        this.setState({ className: className, size: null });
        this.toggle();
    };

    render() {
        return (
            <Row>
                <Col>
                    <Card>
                        <CardBody>
                             <Button color="primary" onClick={() => this.openModalWithSize('lg')}>View</Button>
                            <Modal
                                isOpen={this.state.modal}
                                toggle={this.toggle}
                                className={this.state.className}
                                size={this.state.size}>
                                <ModalHeader toggle={this.toggle}>Description</ModalHeader>
                                <ModalBody>                                                           
                                    <p>
										Akruthi Green Woods BMRDA Sites for Sale<br/>
										
										Starting at 25L<br/>
										
										Plot sizes starting at Sqft 1200<br/>
										
										Akruthi Green Woods offers sites/plots located at Jigani near Surya City, The largest housing board layout off Hosur Road<br/>
										
										The community is near to SVYASA Yoga University, Electronics City and Jigani Industrial Area<br/>
									</p>                                   
                                </ModalBody>
                                <ModalFooter>                                    
                                    <Button color="secondary" className="ml-1" onClick={this.toggle}>Cancel</Button>
                                </ModalFooter>
                            </Modal>


                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default Modals;
