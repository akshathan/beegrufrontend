import React from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    CustomInput,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Button,
} from 'reactstrap';



const BasicInputElements = () => {
    return (
        <Card>
            <CardBody>               
                <Row>
                    <Col lg={6}>
                        <Form>
                            <FormGroup>
                                <Label for="text"> File Name</Label>
                                <Input type="text" name="text" id="name" value="Aggrement" readonly/>
								<br/><button class="btn btn-success">Download</button>
                            </FormGroup>                            										
                        </Form>
                    </Col>
					<Col lg={6}>
                        <Form>
                            <FormGroup>
                                <Label for="text"> File to be  Upload</Label>
								<Input type="text" name="text" id="name" value="License" readonly/>
                                <br/><Input type="file" name="file" id="file" />
								<br/><button class="btn btn-primary">Upload</button>
                            </FormGroup>                            										
                        </Form>
                    </Col>	
                </Row>
            </CardBody>
        </Card>
    );
};


const BasicForms = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    <h3>Upload / Download Document</h3>
                </Col></Row>

            <Row>
                <Col>
                    <BasicInputElements />
                </Col>
            </Row>
           
        </React.Fragment>
    );
};
export default BasicForms;
