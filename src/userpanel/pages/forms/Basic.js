import React from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    CustomInput,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Button,
} from 'reactstrap';



const BasicInputElements = () => {
    return (
        <Card>
            <CardBody>               
                <Row>
                    <Col lg={6}>
                        <Form>
                            <FormGroup>
                                <Label for="text">Name</Label>
                                <Input type="text" name="text" id="name" placeholder="Name" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">Email</Label>
                                <Input type="email" name="email" id="exampleEmail" placeholder="Email" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">Mobile Number</Label>
                                <Input type="email" name="email" id="exampleEmail" placeholder="Without +91" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleText">Message</Label>
                                <Input type="textarea" name="text" id="exampleText" rows="3" />
                            </FormGroup>

                            <button class="btn btn-success">Submit</button>												
                        </Form>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
};


const BasicForms = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    <h3>Report Issue</h3>
                </Col></Row>

            <Row>
                <Col>
                    <BasicInputElements />
                </Col>
            </Row>
           
        </React.Fragment>
    );
};
export default BasicForms;
