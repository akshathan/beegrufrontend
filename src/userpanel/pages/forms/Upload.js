import React from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    CustomInput,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Button,
} from 'reactstrap';



const BasicInputElements = () => {
    return (
        <Card>
            <CardBody>               
                <Row>
                    <Col lg={6}>
                        <Form>
                            <FormGroup>
                                <Label for="text">Name</Label>
                                <Input type="text" name="text" id="name" placeholder="Name" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">Address</Label>
                                <Input type="text" name="address" id="address" placeholder="Address" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">City</Label>
                                <Input type="text" name="city" id="city" placeholder="City" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleText">Price</Label>
                                <Input type="text" name="price" id="price" placeholder="₹" />
                            </FormGroup>
							
							<FormGroup>
                                <Label for="exampleText">Upload Photos</Label>
                                <Input type="file" name="file" id="file" multiple/>
                            </FormGroup>												
                        </Form>
                    </Col>
					<Col lg={6}>
                        <Form>
                            <FormGroup>
                                <Label for="exampleSelect">Pincode</Label>
                                <Input type="text" name="pincode" id="pincode" placeholder="Pincode"/>                                    
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelectMulti">State</Label>
                                <Input type="text" name="state" id="state" placeholder="State"/>                                    
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleFile">Country</Label>
                                <Input type="text" name="country" id="country" placeholder="Country"/>
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleDate">Features</Label>
                                <Input type="textarea" name="features" id="features" placeholder="Features"/>
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleMonth">Description</Label>
                                <Input type="textarea" name="description" id="description" placeholder="Description"/>
                            </FormGroup>
                        </Form>
                    </Col>
					<button class="btn btn-success">Submit</button>
                </Row>
            </CardBody>
        </Card>
    );
};


const BasicForms = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    <h3>Upload Property</h3>
                </Col></Row>

            <Row>
                <Col>
                    <BasicInputElements />
                </Col>
            </Row>
           
        </React.Fragment>
    );
};
export default BasicForms;
