import React from 'react';
import { Card, CardBody, Row, Col, Progress } from 'reactstrap';

import profileImg from '../../../assets/images/users/avatar-7.jpg';

const UserBox = () => {
    return (
        <Card className="">
            <CardBody className="profile-user-box">
                <Row>
                    <Col>
                        <div className="text-center mt-3">
                            <img src={profileImg} alt=""
                                className="avatar-lg rounded-circle" />
                            <h5 className="mt-2 mb-0">Ashray</h5>
                            <h6 className="text-muted font-weight-normal mt-2 mb-0">User</h6>                            
                        </div>
						<div className="mt-5 pt-2 border-top">
                            <h4 className="mb-3 font-size-15">Welcome</h4>
                            <p className="text-muted mb-4">Hi Ashray welcome to Beegru.</p>
                        </div>
                        <div className="mt-3 pt-2 border-top">
                            <h4 className="mb-3 font-size-15">Contact Information</h4>
                            <div className="table-responsive">
                                <table className="table table-borderless mb-0 text-muted">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Email</th>
                                            <td>ashray@atechtron.com</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Phone</th>
                                            <td>9449891295</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>                        
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
};

export default UserBox;
