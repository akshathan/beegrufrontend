import React from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    CustomInput,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Button,
} from 'reactstrap';



const BasicInputElements = () => {
    return (
		<React.Fragment>		
        <Card>			
            <CardBody>               
                <Row>
                    <Col lg={12}>
                        <Form>
                            <FormGroup>
                                <Label for="text">Name</Label>
                                <Input type="text" name="text" id="name" placeholder="Ashray" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">Email ID</Label>
                                <Input type="text" name="address" id="address" placeholder="ashray@atechtron.com" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">Mobile Number</Label>
                                <Input type="text" name="mobile" id="mobile" placeholder="9449891295" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleText">No. of Properties Uploaded</Label>
                                <Input type="text" name="price" id="price" value="3" readonly/>
                            </FormGroup>
							
							<button class="btn btn-success">Update</button>												
                        </Form>
                    </Col>										
                </Row>
            </CardBody>
        </Card>
		</React.Fragment>
    );
};


const Details = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    
                </Col></Row>

            <Row>
                <Col>
                    <BasicInputElements />
                </Col>
            </Row>
           
        </React.Fragment>
    );
};
export default Details;
