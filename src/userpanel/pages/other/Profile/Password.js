import React from 'react';
import {
    Row,
    Col,
    Card,
    CardBody,
    CustomInput,
    Form,
    FormGroup,
    Label,
    Input,
    FormText,
    Button,
} from 'reactstrap';



const BasicInputElements = () => {
    return (
        <Card>
            <CardBody>               
                <Row>
                    <Col lg={12}>
                        <Form>
                            <FormGroup>
                                <Label for="text">Current Password</Label>
                                <Input type="password" id="oldPassword" placeholder="******" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">New Password</Label>
                                <Input type="password" id="newPassword" placeholder="******" />
                            </FormGroup>

                            <FormGroup>
                                <Label for="exampleEmail">Confirm New Password</Label>
                                <Input type="password" id="confirmNewPassword" placeholder="*****" />
                            </FormGroup>

							<button class="btn btn-success">Reset</button>												
                        </Form>
                    </Col>										
                </Row>
            </CardBody>
        </Card>
    );
};


const Details = () => {
    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={12}>
                    
                </Col></Row>

            <Row>
                <Col>
                    <BasicInputElements />
                </Col>
            </Row>
           
        </React.Fragment>
    );
};
export default Details;
