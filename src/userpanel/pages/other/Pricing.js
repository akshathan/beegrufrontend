import React from 'react';
import { Row, Col, Card, CardBody, Media } from 'reactstrap';
import classNames from 'classnames';
import { Users, Briefcase, Heart, Archive } from 'react-feather';




const Pricing = () => {
    const plans = [
        {
            id: 1,
            name: 'VAISHNAVI RELIAABLE GARDENIAA',
            icon: Heart,			
            price: '₹5.3Cr',
            features: ['Anekal Town, Bangalore, Karnataka','2400-3200 Sqft', ' 2-5 BHK', '★Top 5','24x7 Support'],
            isRecommended: true,
        },
        {
            id: 2,
            name: 'SS SAPPHIRE APARTMENTS',
            icon: Heart,			
            price: '₹8.8Cr',
			features: ['Singasandra Bangalore Karnataka','2400-3200 Sqft', ' 2-5 BHK', '★Top 5','24x7 Support'],
            isRecommended: true,
        },
        {
            id: 3,
            name: 'AKRUTHI GREEN WOODS',
            icon: Heart,			
            price: '₹3.5Cr',
            features: ['Jigani, Konasandra, Karnataka','2400-3200 Sqft', ' 2-5 BHK', '★Top 5','24x7 Support'],
            isRecommended: true,
        },
    ];

    return (
        <React.Fragment>

            <Row className="justify-content-center">
                <Col lg={10}>
                    <div className="text-center mt-4 mb-5">
                        <h3>Favourites</h3>                        
                    </div>

                    <Row>
                        {plans.map((plan, idx) => {
                            const Icon = plan.icon;
                            return (
                                <Col lg={4} key={idx}>
                                    <Card className={classNames('card-pricing')}>
                                        <CardBody className="p-4">
                                            <Media>
                                                <Media body>
                                                    <h5 className="mt-0 mb-2 font-size-16">{plan.name}</h5>
                                                    <h2 className="mt-0 mb-2">{plan.price}</h2>
                                                </Media>
                                                <div className="align-self-center">
                                                    <Icon className="icon-dual icon-lg"></Icon>
                                                </div>
                                            </Media>
                                            <ul className="card-pricing-features text-muted border-top pt-2 mt-2 pl-0 list-unstyled">
                                                {plan.features.map((feature, idx1) => {
                                                    return <li key={idx1}>
                                                        <i className='uil uil-check text-success font-size-15 mr-1'></i>{feature}</li>;
                                                })}
                                            </ul>
											<button class="btn btn-danger">Remove</button>
                                            <div className="mt-5 text-center">
                                                <button className={classNames("btn", { "btn-soft-primary": !plan.isRecommended, 'btn-primary': plan.isRecommended }, "px-sm-4")}><i className='uil uil-arrow-right mr-1'></i>Buy Now for {plan.price}</button>
                                            </div>
                                        </CardBody>
                                    </Card>
                                </Col>
                            );
                        })}
                    </Row>
                </Col>
            </Row>

        </React.Fragment>
    );
};

export default Pricing;
